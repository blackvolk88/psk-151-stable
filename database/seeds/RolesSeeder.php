<?php

use Illuminate\Database\Seeder;

use App\Models\Common\Role;

class RolesSeeder extends Seeder
{
    private $roles = [
        'admin' => 'Backend role super user',
        'designer' => 'Frontend role for designers client'
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->roles as $role => $description) {
            Role::create([
                'name' => $role,
                'description' => $description
            ]);
        }
    }
}
