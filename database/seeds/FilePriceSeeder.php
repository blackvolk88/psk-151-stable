<?php

use Illuminate\Database\Seeder;

class FilePriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('file_prices')->insert(['key' => 'design', 'price' => '1.0']);
    }
}
