<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FileStatusSeeder::class);
        $this->call(FilePriceSeeder::class);
        $this->call(DesignerLevel::class);
        $this->call(RolesSeeder::class);
        $this->call(PermissionsSeeder::class);
        $this->call(ReleasesPermissionsSeeder::class);
    }
}
