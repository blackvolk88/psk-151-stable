<?php

use Illuminate\Database\Seeder;

use App\Models\Common\Permission;

class PermissionsSeeder extends Seeder
{
    private $permissions = [
        'dashboard' => [
            'display_name' => 'Dashboard',
            'description' => 'Dashboard section'
        ],
        'list_batches' => [
            'display_name' => 'List of batches',
            'description' => 'View list of batches section'
        ],
        'moderating_batches' => [
            'display_name' => 'Batches moderating',
            'description' => 'View batches moderating section'
        ],
        'list_photos' => [
            'display_name' => 'List of photos',
            'description' => 'View list of photos section'
        ],
        'moderating_photos' => [
            'display_name' => 'Photos moderating',
            'description' => 'View photos moderating section'
        ],
        'references' => [
            'display_name' => 'References',
            'description' => 'View references section'
        ],
        'settings' => [
            'display_name' => 'Settings',
            'description' => 'View settings section'
        ],
        'clients' => [
            'display_name' => 'Clients',
            'description' => 'View clients section'
        ],
        'members' => [
            'display_name' => 'Members',
            'description' => 'View members section'
        ],
        'roles' => [
            'display_name' => 'Roles',
            'description' => 'View roles section'
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->permissions as $permission => $details) {
            Permission::create([
                'name' => $permission,
                'display_name' => $details['display_name'],
                'description' => $details['description']
            ]);
        }
    }
}
