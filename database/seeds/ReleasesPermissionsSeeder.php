<?php

use Illuminate\Database\Seeder;

use App\Models\Common\Permission;

class ReleasesPermissionsSeeder extends Seeder
{
    private $permissions = [
        'list_releases' => [
            'display_name' => 'List of releases',
            'description' => 'View list of releases section'
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->permissions as $permission => $details) {
            Permission::create([
                'name' => $permission,
                'display_name' => $details['display_name'],
                'description' => $details['description']
            ]);
        }
    }
}
