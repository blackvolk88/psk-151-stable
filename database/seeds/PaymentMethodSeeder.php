<?php

use App\Models\Common\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethodSeeder extends Seeder
{
    private $payment_method = [
        'ePayments',
        'PrivatBank',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->payment_method as $name) {
            PaymentMethod::create([
                'status' => PaymentMethod::STATUS_INACTIVE,
                'name' => $name,
            ]);
        }
    }
}
