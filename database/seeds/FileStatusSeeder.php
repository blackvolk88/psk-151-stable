<?php

use Illuminate\Database\Seeder;

class FileStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('file_statuses')->insert(['name' => 'new']);
        DB::table('file_statuses')->insert(['name' => 'not processed', 'template' => 'process']);
        DB::table('file_statuses')->insert(['name' => 'waiting', 'template' => 'waiting']);
        DB::table('file_statuses')->insert(['name' => 'declined', 'template' => 'decline']);
        DB::table('file_statuses')->insert(['name' => 'approved', 'template' => 'approve']);
    }
}