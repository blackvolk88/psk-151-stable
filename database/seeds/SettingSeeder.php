<?php

use App\Models\Common\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{

    private $settings = [
        'min_cash_out' => 'The minimum allowed amount of money withdrawal for the designer',
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->settings as $key => $name) {
            if(Setting::where(compact('key'))->get()->count() === 0) {
                Setting::create([
                    'name' => $name,
                    'key' => $key,
                ]);
            }
        }
    }
}
