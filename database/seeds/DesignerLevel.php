<?php

use Illuminate\Database\Seeder;

class DesignerLevel extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('designer_levels')->insert(['level' => 'beginner']);
        DB::table('designer_levels')->insert(['level' => 'medium']);
        DB::table('designer_levels')->insert(['level' => 'high']);
    }
}
