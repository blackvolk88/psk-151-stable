<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('owner_id');
            $table->string('mime_type');
            $table->string('size');
            $table->integer('width');
            $table->integer('height');
            $table->string('make')->nullable();
            $table->string('model')->nullable();
            $table->float('aperture')->nullable();
            $table->string('exposure_time')->nullable();
            $table->float('focal_length')->nullable();
            $table->integer('iso')->nullable();
            $table->string('thumb_path')->comment('The thumb url where the file is stored');
            $table->string('path')->comment('The actual url where the file is stored');
            $table->string('original_name')->comment('The original uploaded file name');
            $table->string('storage_name')->comment('The stored name to uploaded file');
            $table->integer('status_id')->comment('status from image file');
            $table->timestamp('filmed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
