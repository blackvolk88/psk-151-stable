<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DesignerLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_levels', function(Blueprint $table) {
            $table->increments('id');
            $table->string('level');
            $table->float('coefficient')->nullable()->default(1);
            $table->integer('limit')->nullable()->default(1);
            $table->timestamps();
        });

        Schema::create('user_designer_level', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->integer('level_id')->unsigned();
            $table->foreign('level_id')
                ->references('id')->on('designer_levels')
                ->onDelete('cascade');

            $table->primary(['user_id', 'level_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_levels');
    }
}
