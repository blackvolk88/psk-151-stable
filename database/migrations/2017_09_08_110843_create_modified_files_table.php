<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModifiedFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modified_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->integer('batch_id');
            $table->string('thumb_path')->comment('The thumb url where the file is stored');
            $table->string('path')->comment('The actual url where the file is stored');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modified_files');
    }
}
