<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclineFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decline_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('file_id');
            $table->string('comment')->nullable();
            $table->timestamps();
        });

        Schema::create('decline_file_reasons', function (Blueprint $table) {
            $table->integer('ticket_id')->unsigned();
            $table->foreign('ticket_id')
                ->references('id')->on('decline_files')
                ->onDelete('cascade');

            $table->integer('reason_id')->unsigned();
            $table->foreign('reason_id')
                ->references('id')->on('reject_reasons')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('common_decline_files');
        Schema::dropIfExists('decline_file_reasons');
    }
}
