<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReleasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('releases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('user_id');
            $table->string('storage_name');
            $table->string('file_path');
            $table->timestamps();
        });

        Schema::create('release_file', function (Blueprint $table) {
            $table->unsignedInteger('release_id');
            $table->unsignedInteger('file_id');

            $table->foreign('release_id')->references('id')->on('releases')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('file_id')->references('id')->on('files')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['release_id', 'file_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('releases');
        Schema::dropIfExists('release_file');
    }
}
