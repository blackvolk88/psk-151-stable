@extends('backend.layouts.app')

@section('title')
    | Settings
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Photo Work Prices</h2>
                    </div>
                    <div class="body">
                        <table id="prices-table" class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($prices as $price)
                                <tr>
                                    <td class="col-xs-10">{{ $price->key }}</td>
                                    <td target-id="{{ $price->id }}">{{ $price->price }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Prices not found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2>Batch Rates Coefficients</h2>
                    </div>
                    <div class="body">
                        <table id="difficulties-table" class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Difficulty</th>
                                    <th>Coefficient</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($difficulties as $difficulty)
                                <tr>
                                    <td class="col-xs-10">{{ $difficulty->name }}</td>
                                    <td target-id="{{ $difficulty->id }}">{{ $difficulty->coefficient }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Difficulties not found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2>Designer Rates Coefficients</h2>
                    </div>
                    <div class="body">
                        <table id="designer-levels-table" class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Levels</th>
                                    <th>Coefficient</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($designerLevels as $designerLevel)
                                <tr>
                                    <td class="col-xs-10">{{ $designerLevel->level }}</td>
                                    <td target-id="{{ $designerLevel->id }}">{{ $designerLevel->coefficient }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Designer rates not found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2>Max number batches for designer</h2>
                    </div>
                    <div class="body">
                        <table id="designer-batch-limits-table" class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Levels</th>
                                    <th>Limit</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($designerLevels as $designerLevel)
                                <tr>
                                    <td class="col-xs-10">{{ $designerLevel->level }}</td>
                                    <td target-id="{{ $designerLevel->id }}">{{ $designerLevel->limit }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Max number batches not found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card">
                    <div class="header">
                        <h2>Global Setting</h2>
                    </div>
                    <div class="body">
                        <table id="global-setting-table" class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Value</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($globalSettings as $globalSetting)
                                <tr>
                                    <td class="col-xs-10">{{ $globalSetting->name }}</td>
                                    <td target-id="{{$globalSetting->id}}">{{ $globalSetting->value }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Global settings not found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('plugins/editable-table/mindmup-editabletable.js') }}"></script>
    <script src="{{ asset('js/admin/settings/main.js') }}"></script>
@endsection