@extends('backend.layouts.app')

@section('title')
| New batch
@endsection

@section('content')
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix batches-header">
                        <div class="col-xs-3">
                            <div>
                                @include('backend.pages.batches.partials.batch-list', [
                                'batches' => $batches,
                                'currentBatch' => $currentBatch
                                ])
                            </div>
                        </div>
                        <div class="col-xs-9">
                            <div class="row clearfix">
                                <div class="col-xs-7">
                                    <ul class="options batches-options single-list-options">
                                        <li>
                                            <div class="left-aligned">
                                                @foreach ($formats as $format)
                                                <strong>{{ FileHelper::mimeToHuman($format) }}</strong>&nbsp;&nbsp;
                                                @endforeach
                                            </div>
                                            <div class="right-aligned">{{ $batch->files->count() }}&nbsp;&nbsp;&nbsp;{{ FileHelper::bytesToHuman($batch->files->sum('size')) }}</div>
                                        </li>
                                        <li><div class="left-aligned">{{ $batch->status->name }}</div><div class="right-aligned">{{ $batch->type->name }}&nbsp;&nbsp;&nbsp;{{ $batch->difficulty->name }}</div></li>
                                    </ul>
                                </div>
                                <div class="col-xs-5">
                                    <div class="clearfix batch-group-buttons">
                                        <button id="ungroup-batch" class="btn btn-danger waves-effect f_right"
                                                target-id="{{ $batch->id }}">ungroup batch
                                        </button>
                                        <button id="ungroup-files-batch" class="btn btn-danger waves-effect f_right"
                                                target-id="{{ $batch->id }}" disabled="disabled">ungroup selected files
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-xs-12"><button id="unselect" class="btn btn-default" disabled>Unselect All</button></div>
                    </div>
                </div>

                <div class="body">
                    @include('backend.pages.batches.partials.batch-files-list', ['files' => $files])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="{{ asset('js/admin/batches/moderating.js') }}"></script>
@endsection
