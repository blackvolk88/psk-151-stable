@extends('backend.layouts.app')

@section('title')
| New batch
@endsection

@section('page-css')
<link href="{{ asset('plugins/light-gallery/css/lightgallery.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix batches-header">
                        <div class="col-xs-3">
                            <div class="m-b-15">
                                @include('backend.pages.batches.partials.batch-list', [
                                'batches' => $batches,
                                'currentBatch' => ''
                                ])
                            </div>
                            <div class="m-b-15">
                                <select class="form-control show-tick" id="files-format">
                                    <option value="">All formats</option>
                                    @foreach ($formats as $format)
                                    @if ($currentFormat == $format)
                                    <option selected="selected" value="{{ $format }}">{{
                                        FileHelper::mimeToHuman($format) }}
                                    </option>
                                    @else
                                    <option value="{{ $format }}">{{ FileHelper::mimeToHuman($format) }}</option>
                                    @endif
                                    @endforeach
                                </select>

                            </div>

                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="{{ $filmed }}" id="filmed-date" class="form-control"
                                           placeholder="Filmed">
                                </div>
                            </div>
                            <div class="form-group m-b-0">
                                <div class="form-line">
                                    <input type="text" value="{{ $uploaded }}" id="uploaded-date" class="form-control"
                                           placeholder="Uploaded">
                                </div>
                            </div>

                        </div>
                        <div class="col-xs-9">
                            <div class="row clearfix">
                                <div class="col-xs-7">
                                    <div class="row clearfix">
                                        <div class="col-xs-12">
                                            <ul class="options batches-options single-list-options m-b-15">
                                                <li>
                                                    <div class="left-aligned"><strong><span
                                                            id="info-selected-formats"></span></strong></div>
                                                    <div class="right-aligned"><span id="info-selected-count"></span>
                                                        &nbsp;&nbsp;&nbsp;<span id="info-selected-size"></span></div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-6">
                                            <select class="form-control show-tick" id="work-type">
                                                <option value="">Work type</option>
                                                @foreach ($work_types as $type)
                                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-6">
                                            <select class="form-control show-tick" id="difficulty">
                                                <option value="">Difficulty</option>
                                                @foreach ($difficulties as $difficulty)
                                                <option value="{{ $difficulty->id }}">{{ $difficulty->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-xs-12">
                                            <div class="form-group m-t-20">
                                                <div class="form-line">
                                                    <textarea rows="2" class="form-control no-resize" placeholder="Please specify comment" id="batch-comment"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-5 grouping-row">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input class="form-control" id="random-batching-count"
                                                   placeholder="Random batching" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control show-tick" id="designers">
                                            <option value="">designer to assign batch</option>
                                            @foreach ($designers as $designer)
                                                <option value="{{ $designer->id }}">{{ $designer->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button id="group-batch" class="btn bg-indigo waves-effect f_right"
                                            disabled="disabled">
                                        group batch
                                    </button>
                                </div>

                            </div>

                        </div>
                        <div class="col-xs-12"><button id="unselect" class="btn btn-default m-t-20" disabled>Unselect All</button></div>

                    </div>


                </div>

                <div class="body">
                    @include('backend.pages.batches.partials.files-list', ['files' => $files])
                </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection

@section('page-script')
<script src="{{ asset('js/admin/batches/moderating.js') }}"></script>
@endsection
