@extends('backend.layouts.app')

@section('title')
    | View batch files
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <form method="POST" action="{{ route('backend.batches.update.comment', $batch) }}" class="form-horizontal edit-user-form">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="row clearfix">
                                <div class="col-xs-12">
                                    <div class="form-group m-t-20">
                                        <div class="form-line">
                                            <textarea rows="2" class="form-control no-resize" placeholder="Please specify comment for administrator" name="comment">{{ $batch->admin_comment }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-offset-2 col-xs-10">
                                    <button class="btn btn-primary m-t-15 waves-effect f_right" type="submit">save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="body">
                        @include('backend.pages.batches.partials.batch-files-list', ['files' => $files])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection