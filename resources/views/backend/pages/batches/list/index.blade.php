@extends('backend.layouts.app')

@section('title')
| List of batches
@endsection

@section('content')
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12">

                            <div class="row clearfix">
                                <div class="col-xs-6">
                                    <h2 class="title-block">Filters</h2>
                                     <div class="row clearfix">
                                        <div class="col-xs-6">
                                            <div class="m-b-10">
                                                <select class="form-control show-tick filter-items" id="difficulty">
                                                    <option value="">Difficulty</option>
                                                    @foreach ($difficulties as $difficulty)
                                                    @if ($currentDifficulty == $difficulty->id)
                                                    <option selected="selected" value="{{ $difficulty->id }}">{{
                                                        $difficulty->name }}
                                                    </option>
                                                    @else
                                                    <option value="{{ $difficulty->id }}">{{ $difficulty->name }}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="">
                                                <select class="form-control show-tick filter-items" id="work-type">
                                                    <option value="">Work type</option>

                                                    @foreach ($work_types as $type)
                                                    @if ($currentType == $type->id)
                                                    <option selected="selected" value="{{ $type->id }}">{{ $type->name }}
                                                    </option>
                                                    @else
                                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-xs-6">
                                            <div class="m-b-10">
                                                <select class="form-control show-tick filter-items" id="batch-status">
                                                    <option value="">Status</option>
                                                    @foreach ($statuses as $status)
                                                    @if ($currentStatus == $status->id)
                                                    <option selected="selected" value="{{ $status->id }}">{{ $status->name }}</option>
                                                    @else
                                                    <option value="{{ $status->id }}">{{ $status->name }}</option>
                                                    @endif
                                                    @endforeach
                                                </select>

                                            </div>
                                            <div class="">
                                                <select class="form-control show-tick filter-items" data-live-search="true" id="executors-list">
                                                    <option value="">No executor</option>
                                                    @foreach ($executors as $executor)
                                                    @if ($currentExecutors == $executor->user->id)
                                                    <option selected="selected" value="{{ $executor->user->id }}" data-subtext="{{ $executor->user->id }}">{{ $executor->user->name }}</option>
                                                    @else
                                                    <option value="{{ $executor->user->id }}" data-subtext="{{ $executor->user->id }}">{{ $executor->user->name }}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-offset-2 col-xs-4">
                                    <h2 class="title-block">Batch statistics</h2>
                                    <ul class="options single-list-options">
                                        @foreach ($statusesInfo as $name => $batchesCount)
                                            <li>
                                                <div class="left-aligned">{{ $name }}:</div>
                                                <div class="right-aligned"><strong>{{ $batchesCount }}</strong></div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <table class="table table-responsive table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Status</th>
                                <th>Work type</th>
                                <th>Difficulty</th>
                                <th>Author</th>
                                <th>Executor</th>
                                <th>Pics</th>
                                <th>Size</th>
                                <th>Created</th>
                                <th>Started</th>
                                <th>Finished</th>
                                <th>Last download</th>
                                <th class="right-aligned">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse ($batches as $batch)
                            <tr>
                                <td> <a href="{{ route('backend.batches.moderating.view', $batch->id) }}">{{ $batch->id }}</a></td>
                                <td>{{ $batch->status->name }}</td>
                                <td>{{ $batch->type->name }}</td>
                                <td>{{ $batch->difficulty->name }}</td>
                                <td>{{ $batch->author->name }}</td>
                                @if (is_null($batch->executor()->first()))
                                    <td></td>
                                @else
                                    <td>{{ $batch->executor()->first()->name }}</td>
                                @endif
                                <td>{{ $batch->files->count() }}</td>
                                <td>{{ FileHelper::bytesToHuman($batch->files->sum('size')) }}</td>
                                <td>{{ $batch->created_at->diffForHumans() }}</td>
                                @if (is_null($batch->started_at))
                                    <td></td>
                                @else
                                    <td>{{ $batch->started_at->diffForHumans() }}</td>
                                @endif
                                @if (is_null($batch->finished_at))
                                    <td></td>
                                @else
                                    <td>{{ $batch->finished_at->diffForHumans() }}</td>
                                @endif
                                @if (is_null($batch->download_at))
                                    <td></td>
                                @else
                                    <td>{{ $batch->download_at->diffForHumans() }}</td>
                                @endif
                                <td class="right-aligned">
                                    @if ($batch->isCompleted())
                                        <a href="{{ route('backend.batches.list.download', $batch->id) }}" class="btn btn-success waves-effect btn-icon"><i class="material-icons" title="Download">file_download</i></a>
                                    @else
                                        <a href="#" class="btn btn-danger waves-effect ungroup-batch" target-id="{{ $batch->id }}" title="Ungroup"><img src="{{ asset('bsbmd/images/ungroup.png') }}" alt=""></a>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="11">Batches not found</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <div class="list-unstyled row clearfix paginate-wrapper-row">
                        <div class="col-xs-12">
                            {{ $batches->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="{{ asset('js/admin/batches/list.js') }}"></script>
@endsection