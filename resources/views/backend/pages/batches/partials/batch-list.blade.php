<select class="form-control show-tick" data-live-search="true" id="batch-list">
    <option value="">No batch</option>
    @foreach ($batches as $batch)
        @if ($currentBatch == $batch->id)
            <option selected="selected" value="{{ $batch->id }}" data-subtext="{{ $batch->status->name }}">{{ $batch->id }}</option>
        @else
            <option value="{{ $batch->id }}" data-subtext="{{ $batch->status->name }}">{{ $batch->id }}</option>
        @endif
    @endforeach
</select>