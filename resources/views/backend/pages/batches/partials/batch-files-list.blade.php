<div class="fixed-height">
    <div class="scrolling-batches">
        <div class="row clearfix">
            @forelse ($files as $file)
                <div class="preview-thumb batch-group-thumb">
                    <a>
                        <div class="img-overlay">
                            @if (!is_null($file->modified))
                                <img class="batch-file-image select-file main-preview" id="image_{{ $file->id }}"
                                     target-id="{{ $file->id }}" src="/storage/{{ $file->modified->thumb_path }}">
                            @else
                                <img class="batch-file-image select-file main-preview" id="image_{{ $file->id }}"
                                     target-id="{{ $file->id }}" src="/storage/{{ $file->thumb_path }}">
                            @endif
                            <div class="inner-overlay"></div>
                            <div class="check"><i class="fa fa-check" aria-hidden="true"></i></div>
                            @if (!is_null($file->modified))
                                <div class="small-thumb">
                                    <img src="/storage/{{ $file->thumb_path }}" alt="">
                                </div>
                            @endif
                            <div class="status {{$file->status->template}}">{{ $file->status->name }}</div>
                        </div>
                    </a>
                </div>
            @empty
                <div class="col-xs-12">
                    Images not found
                </div>
            @endforelse
        </div>
    </div>
</div>