@extends('backend.layouts.app')

@section('title')
    | List of releases
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12">
                                <div class="row clearfix">
                                    <div class="col-xs-4">
                                        <div class="clearfix search-row">
                                            <div class="">
                                                <a href="{{ route('backend.photos.release.new.form') }}" class="btn bg-indigo waves-effect"><i class="fa fa-plus" aria-hidden="true"></i> new release</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <table class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Author</th>
                                    <th>Files assigned</th>
                                    <th>Created</th>
                                    <th class="right-aligned">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($releases as $release)
                                    <tr>
                                        <td><a href="{{ route('backend.photos.release.view', $release->id) }}">{{ $release->name }}</a></td>
                                        <td>{{ $release->author->name }}</td>
                                        <td>{{ $release->files()->count() }}</td>
                                        <td>{{ $release->created_at->diffForHumans() }}</td>
                                        <td class="right-aligned">
                                            <a href="{{ route('backend.photos.release.download', $release->id) }}" class="btn btn-success waves-effect btn-icon"><i class="material-icons" title="Download release file">file_download</i></a>
                                            <a href="{{ route('backend.photos.release.prepare.assign', $release->id) }}" class="btn btn-primary waves-effect btn-icon"><i class="material-icons" title="Assign files">edit</i></a>
                                            <a href="#" target-id="{{ $release->id }}" class="btn btn-danger waves-effect delete-release btn-icon" title="Delete"><i class="material-icons" title="Delete release">delete_forever</i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="5">Releases not found</td>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('js/admin/photos/release.js') }}"></script>
@endsection