@extends('backend.layouts.app')

@section('title')
    | Prepare assign files for {{ $release->name }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix batches-header">
                            <div class="col-xs-3">
                                <div>
                                    <select class="form-control show-tick" data-live-search="true" id="package-list" target-id="{{ $release->id }}">
                                        <option value="">No package</option>
                                        @foreach ($packages as $package)
                                            @if ($currentPackage == $package->id)
                                                <option selected="selected" value="{{ $package->id }}">{{ $package->name }}</option>
                                            @else
                                                <option value="{{ $package->id }}">{{ $package->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-9">
                                <div class="row clearfix">
                                    <div class="col-xs-12">
                                        <button id="assign-files" target-id="{{ $release->id }}" class="btn btn-success waves-effect">assign files</button>

                                        <button id="unselect" class="btn btn-default m-l-10 m-r-5" disabled style="float:right;">Unselect All</button>
                                        <button id="select-all-files" class="btn btn-default" style="float:right;">Select All</button>


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        @include('backend.pages.batches.partials.files-list', ['files' => $files])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('js/admin/photos/release.js') }}"></script>
@endsection