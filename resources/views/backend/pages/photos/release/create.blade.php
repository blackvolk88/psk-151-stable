@extends('backend.layouts.app')

@section('title')
    | New release document
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="body">
                        <form method="POST" action="{{ route('backend.photos.release.new.form.submit') }}" class="form-horizontal edit-user-form" enctype='multipart/form-data'>
                            {{ csrf_field() }}

                            <div class="row clearfix">
                                <div class="col-xs-2 form-control-label">
                                    <label for="name">Name</label>
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" value="" class="form-control" placeholder="Specify release name">
                                        </div>
                                        @if ($errors->has('name'))
                                            <label id="name-error" class="error" for="name">{{ $errors->first('name') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-2 form-control-label">
                                    <label for="file">File</label>
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <input type="file" name="file" accept="image/*">
                                        @if ($errors->has('file'))
                                            <label class="error" for="name">{{ $errors->first('file') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-offset-2 col-xs-10">
                                    <button class="btn btn-primary m-t-15 waves-effect f_right" type="submit">save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection