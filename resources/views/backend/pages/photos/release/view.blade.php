@extends('backend.layouts.app')

@section('title')
    | View release files
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="fixed-height">
                            <div class="scrolling-batches">
                                <div class="row clearfix">
                                    @forelse ($files as $file)
                                        <div class="preview-thumb batch-group-thumb">
                                            <a>
                                                <div class="img-overlay">
                                                    <img class="batch-file-image select-file main-preview"
                                                         id="image_{{ $file->id }}" target-id="{{ $file->id }}"
                                                         src="/storage/{{ $file->thumb_path }}">
                                                    <div class="inner-overlay"></div>
                                                    <div class="check"><i class="fa fa-check" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                                <div class="count-release">{{$file->releases->count()}}</div>
                                                <div class="tooltip-info">
                                                    @forelse($file->releases as $release)
                                                        <span>{{$release->name}}</span>
                                                    @empty
                                                        <span>not a single release is not attached to the file</span>
                                                    @endforelse
                                                </div>
                                            </a>
                                        </div>
                                    @empty
                                        <div class="col-xs-12">
                                            Files not found
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
