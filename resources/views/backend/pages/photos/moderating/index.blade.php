@extends('backend.layouts.app')

@section('title')
    | Photos moderating
@endsection

@section('page-css')
    <link href="{{ asset('plugins/light-gallery/css/lightgallery.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">

                            <div class="col-xs-4">
                                <h2 class="title-block">Filters</h2>
                                <div class="m-b-10">
                                    <select class="form-control show-tick filter-items" id="difficulty">
                                    @foreach ($fileStatuses as $status)
                                        @if (isset($currentFileStatus) && $currentFileStatus == $status->id)
                                            <option selected="selected"
                                                    value="{{ $status->id }}">{{ $status->name }}</option>
                                        @else
                                            <option value="{{ $status->id }}">{{ $status->name }}</option>
                                        @endif
                                    @endforeach
                                    </select>
                                </div>
                                <div>
                                    <select id="change-sort" data-selected-value="{{$currentSort}}"
                                        data-selected-type="{{$typeSort}}" class="form-control show-tick filter-items">
                                        <option value="asc" data-type="storage_name">A - Z</option>
                                        <option value="desc" data-type="storage_name">Z - A</option>
                                        <option value="asc" data-type="created_at">New first</option>
                                        <option value="desc" data-type="created_at">Old first</option>
                                    </select>
                                </div>
                            </div>
                            @if (!is_null($file))
                                <div class="col-xs-8">
                                    <h2 class="title-block">Photo information</h2>
                                    <ul class="options">
                                        <li>
                                            <strong>{{ $file->storage_name }}</strong>&nbsp;&nbsp;<strong>{{ FileHelper::mimeToHuman($file->mime_type) }}</strong>
                                        </li>
                                        <li>{{ $file->width }} x {{ $file->height }}
                                            &nbsp;&nbsp;&nbsp;{{ FileHelper::bytesToHuman($file->size) }}</li>
                                    </ul>

                                    <ul class="options single-list-options">
                                        <li>
                                            <div class="left-aligned">Executor</div>
                                            <div class="right-aligned">{{ $file->getWhoWorked() }}</div>
                                        </li>
                                        <li>
                                            <div class="left-aligned">Batch</div>
                                            <div class="right-aligned">{{ $file->batch()->first()->id }}</div>
                                        </li>
                                        <li>
                                            <div class="left-aligned">Work type</div>
                                            <div class="right-aligned">{{ $file->batch()->first()->type->name }}</div>
                                        </li>
                                        <li>
                                            <div class="left-aligned">Difficulty</div>
                                            <div class="right-aligned">{{ $file->batch()->first()->difficulty->name }}</div>
                                        </li>
                                        @if ($file->batch()->first()->comment)
                                            <li>{{ $file->batch()->first()->comment }}</li>
                                        @endif

                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="body">
                        @if ($files->count())
                            <div class="row clearfix fixed-height">
                                <div class="col-xs-4">
                                    <div class="clearfix view-change-buttons">
                                        <a href="{{ route('backend.photos.moderating.multiple') }}"
                                           class="link-preview-action f_left">Multiple</a>

                                    </div>
                                    <div class="scrolling-previews">
                                        <div class="clearfix">
                                            @foreach ($files as $tmpFile)
                                                <div class="preview-thumb thumb-with-name list-unstyled clearfix">
                                                    <a href="{{ route('backend.photos.moderating.view', [$tmpFile, $typeSort, $currentSort]) }}"
                                                       title="{{ $tmpFile->storage_name }}">
                                                        <div class="status waiting">{{ $tmpFile->storage_name }}</div>
                                                        <div class="img-overlay">
                                                            <img class="batch-file-image select-file main-preview"
                                                                 src="/storage/{{ $tmpFile->modified->thumb_path }}">
                                                            <div class="small-thumb preview-image"
                                                                 target-id="{{ $tmpFile->id }}">
                                                                <img src="/storage/{{ $tmpFile->thumb_path }}" alt="">
                                                            </div>

                                                        </div>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-8">
                                    <div class="big-thumb">
                                        <img src="/storage/{{ $file->modified->thumb_path }}">
                                    </div>
                                    <div class="buttons-bottom clearfix">
                                        <div class="qty-selected f_left">1 photo selected</div>
                                        <a href="{{ route('backend.photos.moderating.download', $file->id) }}"
                                           class="btn btn-primary waves-effect f_left">download</a>
                                        <button id="approve-button" class="btn btn-success waves-effect f_right"
                                                target-id="{{ $file->id }}">approve
                                        </button>
                                        <button id="decline-button" class="btn btn-danger waves-effect f_right"
                                                target-id="{{ $file->id }}">decline
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-xs-12">
                                    Files not found
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('js/admin/photos/moderating.js') }}"></script>
@endsection
