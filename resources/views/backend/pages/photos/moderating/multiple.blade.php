@extends('backend.layouts.app')

@section('title')
| Photos moderating
@endsection

@section('page-css')
<link href="{{ asset('plugins/light-gallery/css/lightgallery.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">

                        <div class="col-xs-4">
                            <h2 class="title-block">Filters</h2>
                            <div class="m-b-10">
                                <select class="form-control show-tick filter-items" id="difficulty">
                                @foreach ($fileStatuses as $status)
                                @if (isset($currentFileStatus) && $currentFileStatus == $status->id)
                                    <option selected="selected" value="{{ $status->id }}">{{ $status->name }}</option>
                                @else
                                    <option value="{{ $status->id }}">{{ $status->name }}</option>
                                @endif
                                @endforeach
                                </select>
                            </div>
                            <div class="m-b-10">
                                <select id="change-sort-multiple" data-selected-value="{{$currentSort}}"
                                        data-selected-type="{{$typeSort}}" class="form-control show-tick filter-items">
                                    <option value="asc" data-type="storage_name">A - Z</option>
                                    <option value="desc" data-type="storage_name">Z - A</option>
                                    <option value="asc" data-type="created_at">New first</option>
                                    <option value="desc" data-type="created_at">Old first</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-8" id="multiple-info">

                        </div>
                    </div>
                </div>
                <div class="body">
                    @if($files->count())
                    <div class="row clearfix fixed-height">

                        <div class="col-xs-4">
                            <div class="clearfix view-change-buttons">
                                <a href="{{ route('backend.photos.moderating') }}" class="link-preview-action f_left">Single</a>

                                <button id="unselect" class="link-preview-action f_right m-l-5" disabled>Unselect All</button>
                                <button id="select-all-files" class="link-preview-action f_right">Select All</button>
                            </div>
                            <div class="scrolling-previews">
                                <div class="clearfix">
                                    @foreach ($files as $tmpFile)
                                    <div class="preview-thumb thumb-with-name multiple-thumb list-unstyled clearfix">
                                        <a title="{{ $tmpFile->storage_name }}">
                                            <div class="status waiting">{{ $tmpFile->storage_name }}</div>
                                            <div class="img-overlay">
                                                <img class="select-file main-preview" target-id="{{ $tmpFile->id }}" src="/storage/{{ $tmpFile->modified->thumb_path }}">
                                                <div class="inner-overlay"></div>
                                                <div class="check"><i class="fa fa-check" aria-hidden="true"></i></div>
                                                <div class="small-thumb preview-image" target-id="{{ $tmpFile->id }}">
                                                    <img src="/storage/{{ $tmpFile->thumb_path }}" alt="">
                                                </div>

                                            </div>
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-8" id="multiple-content"></div>

                    </div>
                    @else
                    <div class="row">
                        <div class="col-xs-12">Files not found</div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="{{ asset('js/admin/photos/moderating.js') }}"></script>
@endsection
