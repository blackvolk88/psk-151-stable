<form method="post" id="decline-form" action="{{ route('backend.photos.moderating.decline.submit', $file->id) }}">
    {{ csrf_field() }}
    @foreach ($reasons as $reason)
        <div class="form-group">
            <div class="form-line">
                <input id="reason_{{ $reason->id }}" name="reasons[]" value="{{ $reason->id }}" class="filled-in" type="checkbox">
                <label for="reason_{{ $reason->id }}">{{ $reason->name }}</label>
            </div>
        </div>
    @endforeach
    <div class="form-group m-t-20">
        <div class="form-line">
            <textarea rows="4" class="form-control no-resize" placeholder="Please specify comment" name="comment" required></textarea>
        </div>
    </div>
    <div class="text-center m-b-20">
        <button type="button" class="btn btn-danger waves-effect submit-decline-form">Decline</button>
    </div>
</form>

<script>
    $(document).ready(function() {
        $.adminPhotosModeratingModule.events.initSubmitDeclineForm();
    });
</script>
