<h2 class="title-block">Photos information</h2>
<ul class="options">
    <li>@foreach ($formats as $format)<strong>{{ FileHelper::mimeToHuman($format) }}</strong>&nbsp;&nbsp;@endforeach</li>
</ul>
<ul class="options">
    <li>{{ $files->count() }}pics&nbsp;&nbsp;&nbsp;&nbsp;{{ FileHelper::bytesToHuman($files->sum('size')) }}</li>
</ul>
