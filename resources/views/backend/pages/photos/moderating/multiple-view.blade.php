<div class="scroll-table">
    <table class="table table-responsive table-striped table-bordered">
        <tbody>
        @foreach ($files as $file)
            <tr>
                <td>
                    <img width="50" height="50" src="/storage/{{ $file->modified->thumb_path }}">
                </td>

                <td>{{$file->batch->first()->executor->first()->name}}</td>
                <td>{{ FileHelper::mimeToHuman($file->mime_type) }}</td>
                <td>{{ FileHelper::bytesToHuman($file->size) }}</td>
                <td>{{ $file->storage_name }}</td>
                <td>{{ $file->batch->first()->id }}</td>
                <td>{{ $file->batch->first()->difficulty->name }}</td>
                <td>{{ $file->batch->first()->started_at }}</td>
                <td>{{ $file->created_at }}</td>
                <td><span class="unselect-list" target-id="{{ $file->id }}"><i class="fa fa-trash-o"
                                                                                aria-hidden="true"></i></span></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div class="buttons-bottom clearfix">
    <div class="qty-selected f_left">{{ $files->count() }} photo selected</div>
    <button class="btn btn-primary waves-effect f_left download-selected">download</button>
    <button class="btn btn-success waves-effect f_right approve-selected">approve</button>
    <button class="btn btn-danger waves-effect f_right decline-selected">decline</button>
</div>


<script>
    $(document).ready(function () {
        $.adminPhotosModeratingModule.events.downloadSelected();
        $.adminPhotosModeratingModule.events.declineSelected();
        $.adminPhotosModeratingModule.events.approveSelected();
    });
</script>