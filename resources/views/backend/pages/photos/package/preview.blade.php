<div class="row clearfix text-center m-b-20">
    <img src="/storage/{{ $file->thumb_path }}">
</div>
<div class="row clearfix">
    <table class="table table-responsive table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th>Original Name</th>
                <th>Storage Name</th>
                <th>Format</th>
                <th>Size</th>
                <th>Width</th>
                <th>Height</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $file->original_name }}</td>
                <td>{{ $file->storage_name }}</td>
                <td>{{ FileHelper::mimeToHuman($file->mime_type) }}</td>
                <td>{{ FileHelper::bytesToHuman($file->size) }}</td>
                <td>{{ $file->width }}</td>
                <td>{{ $file->height }}</td>
            </tr>
        </tbody>
    </table>
    <table class="table table-responsive table-striped table-hover table-bordered">
        <thead>
        <tr>
            <th>Camera Brand</th>
            <th>Camera Model</th>
            <th>Date Taken</th>
            <th>Exposure Time</th>
            <th>Aperture Value</th>
            <th>ISO Speed Rating</th>
            <th>Focal Length</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $file->make }}</td>
            <td>{{ $file->model }}</td>
            <td>{{ $file->filmed_at }}</td>
            <td>{{ $file->exposure_time }}</td>
            <td>{{ $file->aperture }}</td>
            <td>{{ $file->iso }}</td>
            <td>{{ $file->focal_length }}</td>
        </tr>
        </tbody>
    </table>
</div>