<div class="row clearfix text-center">
    <a href="{{ route('backend.photos.package.download.original', $package->id) }}" class="btn btn-primary waves-effect download-package-files">original files</a>
    <a href="{{ route('backend.photos.package.download.completed', $package->id) }}" class="btn btn-success waves-effect download-package-files">completed files</a>
</div>

<script>
    $(document).ready(function() {
        $.adminPhotosPackageModule.events.initCloseDownloadPackage();
    });
</script>