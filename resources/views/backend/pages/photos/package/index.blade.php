@extends('backend.layouts.app')

@section('title')
    | List of packages
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12">
                                <div class="row clearfix">
                                    <div class="col-xs-4">
                                        <div class="clearfix search-row">
                                            <button id="search-package-button" class="btn bg-indigo waves-effect f_right">search</button>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input value="{{ $currentSearch }}" class="form-control" id="search-package-name" placeholder="Please select package name" type="text">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="clearfix">
                                            <a href="{{ route('backend.photos.package.new.form') }}" class="btn bg-indigo waves-effect f_right"><i class="fa fa-plus" aria-hidden="true"></i> new package</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="body">
                        <table class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Author</th>
                                    <th>Files count / Completed</th>
                                    <th class="right-aligned">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($packages as $package)
                                    <tr>
                                        <td><a href="{{ route('backend.photos.package.view', $package->id) }}">{{ $package->name }}</a></td>
                                        <td>{{ $package->user->name }}</td>
                                        <td>{{ $package->files()->count() }} / {{ $package->completedFilesCount() }}</td>
                                        <td class="right-aligned">
                                            <a href="#" target-id="{{ $package->id }}" class="btn btn-success waves-effect btn-icon prepare-download-package"><i class="material-icons" title="Download">file_download</i></a>
                                            <a href="#" target-id="{{ $package->id }}" class="btn btn-danger waves-effect delete-package btn-icon" title="Delete"><i class="material-icons">delete_forever</i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="4">Packages not found</td>
                                @endforelse
                            </tbody>
                        </table>
                        <div class="list-unstyled row clearfix paginate-wrapper-row">
                            <div class="col-xs-12">
                                {{ $packages->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('js/admin/photos/package.js') }}"></script>
@endsection