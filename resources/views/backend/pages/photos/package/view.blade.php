@extends('backend.layouts.app')

@section('title')
| view package {{ $package->name }}
@endsection

@section('page-css')
<link href="{{ asset('plugins/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>{{ $package->name }}</h2>
                </div>
                <div class="body">
                    <form class="dropzone" method="POST" enctype="multipart/form-data" action="{{ route('backend.photos.package.upload.submit', $package->id) }}">
                        {{ csrf_field() }}

                        <div class="dz-message">
                            <div class="drag-icon-cph">
                                <i class="material-icons">touch_app</i>
                            </div>
                            <span>Drop files here or click to upload</span>
                        </div>
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12">
                            <div class="row clearfix">
                                <div class="col-xs-4">
                                    <div class="m-b-10">
                                        <select class="form-control show-tick filter-items" id="file-format">
                                            <option value="">All formats</option>
                                            @foreach ($formats as $format)
                                                @if ($currentFormat == $format)
                                                    <option selected="selected" value="{{ $format }}">{{ FileHelper::mimeToHuman($format) }}</option>
                                                @else
                                                    <option value="{{ $format }}">{{ FileHelper::mimeToHuman($format) }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="clearfix search-row">
                                        <button id="search-file-button" class="btn bg-indigo waves-effect f_right">search</button>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input value="{{ $currentSearch }}" class="form-control" id="search-file-name" placeholder="Please select file name" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="{{ $package->id }}" id="package-id">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <table class="table table-responsive table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Preview</th>
                            <th>File</th>
                            <th>Format</th>
                            <th>Batch</th>
                            <th>Uploaded</th>
                            <th>Size</th>
                            <th class="right-aligned">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($files as $file)
                            <tr>
                                <td>
                                    <div class="preview-thumb">
                                        <div class="img-overlay">
                                            <img class="main-preview" src="/storage/{{ $file->thumb_path }}">
                                            <div class="inner-overlay"></div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" target-id="{{ $file->id }}" class="preview-image">{{ $file->original_name }}</a>
                                </td>
                                <td>{{ FileHelper::mimeToHuman($file->mime_type) }}</td>
                                @if (!is_null($file->batch->first()))
                                    <td>{{ $file->batch->first()->id }}</td>
                                @else
                                    <td>Not assigned</td>
                                @endif
                                <td>{{ $file->created_at->diffForHumans() }}</td>
                                <td>{{ FileHelper::bytesToHuman($file->size) }}</td>
                                <td class="right-aligned">
                                    <a href="{{ route('backend.photos.package.file.download', $file->id) }}" class="btn btn-success waves-effect btn-icon"><i class="material-icons" title="Download">file_download</i></a>
                                    <a href="#" class="btn btn-danger waves-effect delete-file btn-icon" parent-id="{{ $package->id }}" target-id="{{ $file->id }}" title="Delete"><i class="material-icons">delete_forever</i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">Files not found</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <div class="list-unstyled row clearfix paginate-wrapper-row">
                        <div class="col-xs-12">
                            {{ $files->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="{{ asset('plugins/dropzone/dropzone.min.js') }}"></script>
<script src="{{ asset('js/admin/photos/package.js') }}"></script>
@endsection