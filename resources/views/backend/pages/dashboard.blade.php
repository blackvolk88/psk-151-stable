@extends('backend.layouts.app')

@section('title')
    | Dashboard
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="header">
                <h2>Statistics</h2>
            </div>
            <div class="body dashboard">
                <div class="row clearfix">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="header bg-cyan">
                                <h2>Photos</h2>
                            </div>
                            <div class="body">
                                <table class="table table-responsive table-striped">
                                    <tbody>
                                        <tr>
                                            <td><b>Total</b></td>
                                            <td>{{ $files['total'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>New</td>
                                            <td>{{ $files['new'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Need moderating</td>
                                            <td>{{ $files['designed'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Approved</td>
                                            <td>{{ $files['approved'] }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="header bg-cyan">
                                <h2>Users</h2>
                            </div>
                            <div class="body">
                                <table class="table table-responsive table-striped">
                                    <tbody>
                                        <tr>
                                            <td>Designers</td>
                                            <td>{{ $users['designers'] }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-t-20">
                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="header bg-cyan">
                                <h2>Batches</h2>
                            </div>
                            <div class="body">
                                <table class="table table-responsive table-striped">
                                    <tbody>
                                        <tr>
                                            <td><b>Total</b></td>
                                            <td>{{ $batches['total'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Processed</td>
                                            <td>{{ $batches['processed'] }}</td>
                                        </tr>
                                        <tr>
                                            <td>Completed</td>
                                            <td>{{ $batches['completed'] }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="header bg-cyan">
                                <h2>Payouts</h2>
                            </div>
                            <div class="body">
                                <table class="table table-responsive table-striped">
                                    <tbody>
                                    <tr>
                                        <td><b>Total</b></td>
                                        <td>{{ $payouts['accruals'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>For design</td>
                                        <td>{{ $payouts['accruals'] }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection