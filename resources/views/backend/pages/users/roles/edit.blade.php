@extends('backend.layouts.app')

@section('title')
    | Edit role {{ $role->name }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Permissions for {{ $role->name }} role</h2>
                    </div>
                    <div class="body">
                        <form method="POST" action="{{ route('backend.roles.edit.submit', $role->id) }}" class="form-horizontal edit-user-form">
                            {{ method_field('patch') }}
                            {{ csrf_field() }}

                            @foreach ($permissions as $permission)
                                    <div class="col-xs-6 p-t-5">
                                        @if ($role->hasPermission($permission->name))
                                            <input type="checkbox" value="{{ $permission->id }}" name="permissions[]" id="permission_{{ $permission->id }}" class="filled-in chk-col-indigo" checked>
                                        @else
                                            <input type="checkbox" value="{{ $permission->id }}" name="permissions[]" id="permission_{{ $permission->id }}" class="filled-in chk-col-indigo">
                                        @endif
                                            <label for="permission_{{ $permission->id }}">{{ $permission->description }}</label>
                                    </div>
                            @endforeach
                            @if ($errors->has('permissions'))
                                <div class="form-group">
                                    <label class="error">{{ $errors->first('permissions') }}</label>
                                </div>
                            @endif
                            <div class="row clearfix">
                                <div class="col-xs-offset-2 col-xs-10">
                                    <button class="btn btn-primary m-t-15 waves-effect f_right" type="submit">save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection