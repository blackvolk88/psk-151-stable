@extends('backend.layouts.app')

@section('title')
    | Roles
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <a href="{{ route('backend.roles.new') }}" class="btn bg-indigo waves-effect">new role</a>
                    </div>
                    <div class="body">
                        <table class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th class="right-aligned">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($roles as $role)
                                    <tr>
                                        <td>{{ $role->name }}</td>
                                        <td class="right-aligned">
                                            <a href="{{ route('backend.roles.edit', $role->id) }}" class="btn btn-primary waves-effect btn-icon"><i class="material-icons">edit</i></a>
                                            <a href="#" class="btn btn-danger waves-effect delete-role btn-icon" target-id="{{ $role->id }}" title="Delete"><i class="material-icons">delete_forever</i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('js/admin/users/roles.js') }}"></script>
@endsection