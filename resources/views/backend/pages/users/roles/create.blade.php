@extends('backend.layouts.app')

@section('title')
    | New role
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="body">
                        <form method="POST" action="{{ route('backend.roles.new.submit') }}" class="form-horizontal edit-user-form">
                            {{ csrf_field() }}
                            <div class="card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-xs-2 form-control-label">
                                            <label for="name">Name</label>
                                        </div>
                                        <div class="col-xs-10">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" name="name" value="" class="form-control" placeholder="Enter role name">
                                                </div>
                                                @if ($errors->has('name'))
                                                    <label id="name-error" class="error" for="name">{{ $errors->first('name') }}</label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="header">
                                    <h2>Permissions</h2>
                                </div>
                                <div class="body">
                                    <div class="row">
                                        <div class="switch">
                                            @foreach ($permissions as $permission)
                                                <div class="col-xs-6 p-t-5">
                                                    <input type="checkbox" value="{{ $permission->id }}" name="permissions[]" id="permission_{{ $permission->id }}" class="filled-in chk-col-indigo">
                                                    <label for="permission_{{ $permission->id }}">{{ $permission->description }}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @if ($errors->has('permissions'))
                                        <div class="form-group">
                                            <label class="error">{{ $errors->first('permissions') }}</label>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-offset-2 col-xs-10">
                                    <button class="btn btn-primary m-t-15 waves-effect f_right" type="submit">save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection