@extends('backend.layouts.app')

@section('title')
    | Clients
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12">
                                <div class="row clearfix">
                                    <div class="col-xs-4">
                                        <div class="m-b-20">
                                            <select class="form-control show-tick filter-items" id="file-format">
                                                <option value="">All roles</option>
                                                @foreach($roles as $role)
                                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="clearfix search-row">
                                            <button id="search-button" class="btn bg-indigo waves-effect f_right">search</button>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input value="{{ $currentSearch }}" class="form-control" id="search-client-name" placeholder="Please specify client name" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <table class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Moderated</th>
                                    <th>Documents</th>
                                    <th>Last login</th>
                                    <th class="right-aligned">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($clients as $client)
                                    <tr>
                                        <td>{{ $client->name }}</td>
                                        <td>{{ $client->email }}</td>
                                        <td>{{ $client->roles()->first()->name }}</td>
                                        <td>
                                            @if ($client->active)
                                                true
                                            @else
                                                false
                                            @endif
                                        </td>
                                        <td>
                                            @if ($client->passport_url)
                                                true
                                            @else
                                                false
                                            @endif
                                        </td>
                                        @if (is_null($client->last_login))
                                            <td></td>
                                        @else
                                            <td>{{ $client->last_login->diffForHumans() }}</td>
                                        @endif
                                        <td class="right-aligned">
                                            <a href="{{ route('backend.users.clients.view', $client->id) }}" class="btn btn-primary waves-effect"><i class="material-icons">pageview</i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7">Users not found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <div class="list-unstyled row clearfix paginate-wrapper-row">
                            <div class="col-xs-12">
                                {{ $clients->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('js/admin/users/clients.js') }}"></script>
@endsection