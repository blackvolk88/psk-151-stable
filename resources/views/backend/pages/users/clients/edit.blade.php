@extends('backend.layouts.app')

@section('title')
    | Edit client {{ $user->name }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="body">
                        <form method="POST" action="{{ route('backend.users.clients.edit.submit', $user->id) }}" class="form-horizontal edit-user-form" enctype='multipart/form-data'>
                            {{ method_field('patch') }}
                            {{ csrf_field() }}

                            <div class="row clearfix">
                                <div class="col-xs-2 form-control-label">
                                    <label for="name">Name</label>
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" value="{{ $user->name }}" class="form-control" placeholder="Enter client name">
                                        </div>
                                        @if ($errors->has('name'))
                                            <label class="error" for="name">{{ $errors->first('name') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-2 form-control-label">
                                    <label for="email">Email</label>
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="email" value="{{ $user->email }}" class="form-control" placeholder="Enter client email">
                                        </div>
                                        @if ($errors->has('email'))
                                            <label class="error" for="name">{{ $errors->first('email') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-2 form-control-label">
                                    <label for="phone">Phone</label>
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="phone" value="{{ $user->phone }}" class="form-control" placeholder="Enter client phone">
                                        </div>
                                        @if ($errors->has('phone'))
                                            <label class="error" for="name">{{ $errors->first('phone') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-2 form-control-label">
                                    <label for="skype">Skype</label>
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="skype" value="{{ $user->skype }}" class="form-control" placeholder="Enter client skype">
                                        </div>
                                        @if ($errors->has('skype'))
                                            <label class="error" for="name">{{ $errors->first('skype') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-2 form-control-label">
                                    <label for="passport">Passport</label>
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <input type="file" name="passport" accept="image/*">
                                        @if ($errors->has('passport'))
                                            <label class="error" for="name">{{ $errors->first('passport') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-2 form-control-label">
                                    <label for="passport">Level</label>
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <select name="level">
                                            <option>level</option>
                                            @foreach ($designerLevels as $level)
                                                @if ($currentLevel == $level->id)
                                                    <option selected="selected" value="{{ $level->id }}">{{ $level->level }}</option>
                                                @else
                                                    <option value="{{ $level->id }}">{{ $level->level }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('level'))
                                            <label class="error" for="level">{{ $errors->first('level') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-offset-2 col-xs-10">
                                    <button class="btn btn-primary m-t-15 waves-effect f_right" type="submit">save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection