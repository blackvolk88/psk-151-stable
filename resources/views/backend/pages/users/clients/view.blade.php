@extends('backend.layouts.app')

@section('title')
    | View client {{ $user->name }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header text-center">
                        @if (is_null($user->photo_url))
                            <img class="ava" src="/images/frontend/smith-person-icon.png" alt="" width="100" height="100">
                        @else
                            <img class="ava" src="/storage/{{ $user->photo_url }}" alt="" width="100" height="100">
                        @endif
                        <h3>{{ $user->name }}</h3>
                    </div>
                    <div class="body">
                        <table class="table table-responsive table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <td>Role</td>
                                    <td>{{ $user->roles()->first()->name }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{{ $user->email }}</td>
                                </tr>
                                <tr>
                                    <td>Phone number</td>
                                    <td>{{ $user->phone }}</td>
                                </tr>
                                <tr>
                                    <td>Skype</td>
                                    <td>{{ $user->skype }}</td>
                                </tr>
                                <tr>
                                    <td>Earned</td>
                                    <td>{{ $user->balance->balance }}</td>
                                </tr>
                                <tr>
                                    <td>Registration</td>
                                    <td>{{ $user->created_at->diffForHumans() }}</td>
                                </tr>
                                <tr>
                                    <td>Last login</td>
                                    @if (is_null($user->last_login))
                                        <td></td>
                                    @else
                                        <td>{{ $user->last_login->diffForHumans() }}</td>
                                    @endif
                                </tr>
                                @if ($user->passport_url)
                                    <tr>
                                        <td>Passport</td>
                                        <td>
                                            <a href="/storage/{{ $user->passport_url }}" target="_blank">View file</a>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>
                                        <div class="switch">
                                            @if ($user->active)
                                                <label>block<input type="checkbox" id="user-status" target-id="{{ $user->id }}" checked><span class="lever switch-col-cyan"></span>active</label>
                                            @else
                                                <label>block<input type="checkbox" id="user-status" target-id="{{ $user->id }}"><span class="lever switch-col-cyan"></span>active</label>
                                            @endif
                                        </div>
                                    </td>
                                    <td class="right-aligned">
                                        <a href="{{ route('backend.users.clients.edit', $user->id) }}" class="btn btn-primary waves-effect btn-icon"><i class="material-icons">edit</i></a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('js/admin/users/clients.js') }}"></script>
@endsection