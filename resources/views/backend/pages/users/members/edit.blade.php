@extends('backend.layouts.app')

@section('title')
    | Edit member {{ $user->name }}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="body">
                        <form method="POST" action="{{ route('backend.users.members.edit.submit', $user->id) }}" class="form-horizontal edit-user-form">
                            {{ method_field('patch') }}
                            {{ csrf_field() }}

                            <div class="row clearfix">
                                <div class="col-xs-2 form-control-label">
                                    <label for="name">Name</label>
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" value="{{ $user->name }}" class="form-control" placeholder="Enter client name">
                                        </div>
                                        @if ($errors->has('name'))
                                            <label id="name-error" class="error" for="name">{{ $errors->first('name') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-2 form-control-label">
                                    <label for="email">Email</label>
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="email" value="{{ $user->email }}" class="form-control" placeholder="Enter client email">
                                        </div>
                                        @if ($errors->has('email'))
                                            <label id="name-error" class="error" for="name">{{ $errors->first('email') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-2 form-control-label">
                                    <label for="role">Role</label>
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <select name="role">
                                            <option>Role</option>
                                            @foreach ($roles as $role)
                                                @if ($user->roles()->first()->id == $role->id)
                                                    <option value="{{ $role->id }}" selected="selected">{{ $role->name }}</option>
                                                @else
                                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('role'))
                                            <label class="error" for="role">{{ $errors->first('role') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-offset-2 col-xs-10">
                                    <button class="btn btn-primary m-t-15 waves-effect f_right" type="submit">save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection