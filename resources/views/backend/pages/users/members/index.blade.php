@extends('backend.layouts.app')

@section('title')
    | Members
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12">
                                <div class="row clearfix">
                                    <div class="col-xs-4">
                                        <div class="m-b-20">
                                            <select class="form-control show-tick filter-items" id="file-format">
                                                <option value="">All roles</option>
                                                @foreach ($roles as $role)
                                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="clearfix search-row">
                                            <button id="search-button" class="btn bg-indigo waves-effect f_right">search</button>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input value="{{ $currentSearch }}" class="form-control" id="search-member-name" placeholder="Please specify member name" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-b-20">
                                            <a href="{{ route('backend.users.members.new.form') }}" id="new-member-button" class="btn bg-indigo waves-effect">new member</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <table class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Active</th>
                                <th>Last login</th>
                                <th class="right-aligned">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($members as $member)
                                <tr>
                                    <td>{{ $member->name }}</td>
                                    <td>{{ $member->email }}</td>
                                    <td>{{ $member->roles()->first()->name }}</td>
                                    <td>
                                        @if ($member->active)
                                            true
                                        @else
                                            false
                                        @endif
                                    </td>
                                    @if (is_null($member->last_login))
                                        <td></td>
                                    @else
                                        <td>{{ $member->last_login->diffForHumans() }}</td>
                                    @endif
                                    <td class="right-aligned">
                                        <a href="{{ route('backend.users.members.view', $member->id) }}" class="btn btn-primary waves-effect"><i class="material-icons">pageview</i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">Users not found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="list-unstyled row clearfix paginate-wrapper-row">
                            <div class="col-xs-12">
                                {{ $members->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
        <script src="{{ asset('js/admin/users/members.js') }}"></script>
@endsection