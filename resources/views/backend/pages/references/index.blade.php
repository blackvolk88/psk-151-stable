@extends('backend.layouts.app')

@section('title')
    | References
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Batch statuses</h2>
                    </div>
                    <div class="body">
                        <table id="statuses-table" class="table table-striped table-responsive table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($statuses as $status)
                                <tr>
                                    <td status-id="{{ $status->id }}">{{ $status->name }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2>Work types</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{ route('backend.references.work-type.create') }}">Create</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <table class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Work type</th>
                                <th class="right-aligned">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($types as $type)
                                <tr>
                                    <td class="col-xs-10">{{ $type->name }}</td>
                                    <td class="right-aligned">
                                        <a href="{{ route('backend.references.work-type.edit', $type->id) }}" class="btn btn-primary waves-effect btn-icon"><i class="material-icons">edit</i></a>
                                        <a href="#" target-id="{{ $type->id }}" class="btn btn-danger waves-effect remove-type-item btn-icon"><i class="material-icons">delete_forever</i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No work types found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2>Difficulties</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{ route('backend.references.difficulty.create') }}">Create</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <table class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Difficulty</th>
                                <th class="right-aligned">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($difficulties as $difficulty)
                                <tr>
                                    <td class="col-xs-10">{{ $difficulty->name }}</td>
                                    <td class="right-aligned">
                                        <a href="{{ route('backend.references.difficulty.edit', $difficulty->id) }}" class="btn btn-primary waves-effect btn-icon"><i class="material-icons">edit</i></a>
                                        <a href="#" target-id="{{ $difficulty->id }}" class="btn btn-danger waves-effect remove-difficulty-item btn-icon"><i class="material-icons">delete_forever</i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">No difficulties found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2>Reject reasons</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="{{ route('backend.references.reject.create') }}">Create</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <table class="table table-responsive table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Reason</th>
                                <th class="right-aligned">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($reasons as $reason)
                                <tr>
                                    <td class="col-xs-10">{{ $reason->name }}</td>
                                    <td class="right-aligned">
                                        <a href="{{ route('backend.references.reject.edit', $reason->id) }}" class="btn btn-primary waves-effect btn-icon"><i class="material-icons">edit</i></a>
                                        <a href="#" target-id="{{ $reason->id }}" class="btn btn-danger waves-effect delete-reject-item btn-icon"><i class="material-icons">delete_forever</i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Reject reasons not found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('plugins/editable-table/mindmup-editabletable.js') }}"></script>
    <script src="{{ asset('js/admin/references/main.js') }}"></script>
@endsection