@extends('backend.layouts.app')

@section('title')
    | Create new difficulty
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="body">
                        <form method="POST"  class="clearfix" action="{{ route('backend.references.difficulty.create.submit') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" name="name" required="required" class="form-control" placeholder="Enter your difficulty">
                                </div>
                                @if ($errors->has('name'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('name') }}</label>
                                @endif
                            </div>
                            <button type="submit" class="btn bg-indigo waves-effect f_right">CREATE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection