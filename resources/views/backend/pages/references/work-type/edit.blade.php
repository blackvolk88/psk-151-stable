@extends('backend.layouts.app')

@section('title')
    | Edit work type
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="body">
                        <form method="POST" class="clearfix" action="{{ route('backend.references.work-type.edit.submit', $type->id) }}">
                            {{ method_field('patch') }}
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" name="name" required="required" value="{{ $type->name }}" class="form-control" placeholder="Enter your work type name">
                                </div>
                                @if ($errors->has('name'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('name') }}</label>
                                @endif
                            </div>
                            <button type="submit" class="btn bg-indigo waves-effect f_right">UPDATE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection