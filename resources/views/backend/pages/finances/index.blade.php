@extends('backend.layouts.app')

@section('title')
    | Finances
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Finances list</h2>
                    </div>
                    <div class="body">
                        <table id="finance-table"
                               class="table table-striped table-responsive table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>User</th>
                                <th>Date</th>
                                <th>Sum</th>
                                <th>Payment Method</th>
                                <th>Number Card</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($withdraws as $withdraw)
                                <tr>
                                    <td>{{$withdraw->user->name}}</td>
                                    <td>{{ $withdraw->created_at->format('Y-m-d H:m') }}</td>
                                    <td>{{ $withdraw->amount }}</td>
                                    <td>{{ $withdraw->paymentMethod->name }}</td>
                                    <td>{{  $withdraw->number_card }}</td>
                                    <td>{{ $withdraw->getNameStatus() }}</td>

                                    @if($withdraw->status === $status_new)
                                        <td class="right-aligned">
                                            <a href="{{route('backend.finance.edit', [$withdraw->id])}}"
                                               target-action="approved"
                                               class="edit-withdraw btn btn-primary waves-effect btn-icon"><i
                                                        class="material-icons">check</i></a>
                                            <a href="{{route('backend.finance.edit', [$withdraw->id])}}"
                                               target-action="decline"
                                               class="edit-withdraw btn btn-danger waves-effect remove-type-item btn-icon"><i
                                                        class="material-icons">close</i></a>
                                        </td>
                                    @else
                                        <td></td>
                                    @endif
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7">Applications for withdrawal of money not found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('js/admin/finances/main.js') }}"></script>
@endsection