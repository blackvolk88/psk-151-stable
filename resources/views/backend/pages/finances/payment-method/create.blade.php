@extends('backend.layouts.app')

@section('title')
    | New Payment Method
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="body">
                        <form method="POST" action="{{ route('backend.payment.method.new') }}" class="form-horizontal edit-user-form">
                            {{ csrf_field() }}

                            <div class="row clearfix">
                                <div class="col-xs-2 form-control-label">
                                    <label for="name">Name</label>
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" value="" class="form-control" placeholder="Payment method name">
                                        </div>
                                        @if ($errors->has('name'))
                                            <label id="name-error" class="error" for="name">{{ $errors->first('name') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-offset-2 col-xs-10">
                                    <button class="btn btn-primary m-t-15 waves-effect f_right" type="submit">save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection