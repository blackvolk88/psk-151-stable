@extends('backend.layouts.app')

@section('title')
    | Finances
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xs-12">
                <div class="card">
                    <div class="header">
                        <div class="row clearfix">
                            <div class="col-xs-12">
                                <div class="row clearfix">
                                    <div class="col-xs-4">
                                        <div class="clearfix search-row">
                                            <div class="m-b-20">
                                                <a href="{{ route('backend.payment.method.new.form') }}"
                                                   class="btn bg-indigo waves-effect">new payment method</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">

                        <table id="payment-method-table"
                               class="table table-striped table-responsive table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($paymentMethods as $paymentMethod)
                                <tr>
                                    <td class="edit-name" target-id="{{$paymentMethod->id}}">{{$paymentMethod->name}}</td>
                                    <td>
                                        @if($paymentMethod->status == $activeStatus)
                                            <a href="{{route('backend.payment.method.edit', [$paymentMethod->id])}}"
                                               target-status="{{$inActiveStatus}}" class="toogle-edit"><i
                                                        class="material-icons">visibility</i></a>
                                        @else
                                            <a href="{{route('backend.payment.method.edit', [$paymentMethod->id])}}"
                                               target-status="{{$activeStatus}}" class="toogle-edit"><i
                                                        class="material-icons">visibility_off</i></a>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Payment method not found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('plugins/editable-table/mindmup-editabletable.js') }}"></script>
    <script src="{{ asset('js/admin/finances/payment-method.js') }}"></script>
@endsection