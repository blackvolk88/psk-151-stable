<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true"
                     aria-expanded="false">{{ Auth::guard('backend')->user()->name }}</div>
                <div class="email">{{ Auth::guard('backend')->user()->email }}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                        <li role="seperator" class="divider"></li>
                        <li>
                            <a href="{{ route('backend.logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="material-icons">input</i>Sign Out
                            </a>
                        </li>

                        <form id="logout-form" action="{{ route('backend.logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">NAVIGATION</li>
                @if (auth()->user()->can('dashboard'))
                    <li {{ Route::is('/') ? 'class=active' : '' }}>
                        <a href="{{ route('backend.dashboard') }}">
                            <i class="material-icons">home</i>
                            <span>Dasboard</span>
                        </a>
                    </li>
                @endif

                @if (auth()->user()->can(['list_batches', 'moderating_batches']))
                    <li>
                        <a href="javascript:void(0);"
                           class="menu-toggle {{ request()->is('admin/batches*') ? 'toggled' : '' }}">
                            <i class="material-icons">camera_enhance</i>
                            <span>Batches</span>
                        </a>
                        <ul class="ml-menu">
                            @if (auth()->user()->can('list_batches'))
                                <li {{ request()->is('admin/batches/list*') ? 'class=active' : '' }}>
                                    <a href="{{ route('backend.batches.list') }}">
                                        <span>List of batches</span>
                                    </a>
                                </li>
                            @endif
                            @if (auth()->user()->can('moderating_batches'))
                                <li {{ request()->is('admin/batches/moderating*') ? 'class=active' : '' }}>
                                    <a href="{{ route('backend.batches.moderating') }}">
                                        <span>Batch moderating</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if (auth()->user()->can(['list_photos', 'moderating_photos']))
                    <li>
                        <a href="javascript:void(0);"
                           class="menu-toggle {{ request()->is('admin/photos*') ? 'toggled' : '' }}">
                            <i class="material-icons">camera</i>
                            <span>Photos</span>
                        </a>
                        <ul class="ml-menu">
                            @if (auth()->user()->can('list_photos'))
                                <li {{ request()->is('admin/photos/packages/*') ? 'class=active' : '' }}>
                                    <a href="{{ route('backend.photos.list') }}">
                                        <span>List of packages</span>
                                    </a>
                                </li>
                            @endif
                            @if (auth()->user()->can('moderating_photos'))
                                <li {{ request()->is('admin/photos/moderating*') ? 'class=active' : '' }}>
                                    <a href="{{ route('backend.photos.moderating') }}">
                                        <span>Photos moderating</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
                @if (auth()->user()->can('list_releases'))
                    <li>
                        <a href="javascript:void(0);"
                           class="menu-toggle {{ request()->is('admin/releases*') ? 'toggled' : '' }}">
                            <i class="material-icons">attachment</i>
                            <span>Releases</span>
                        </a>
                        <ul class="ml-menu">
                            <li {{ request()->is('admin/releases*') ? 'class=active' : '' }}>
                                <a href="{{ route('backend.releases.list') }}">
                                    <span>List of releases</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if (auth()->user()->can('references'))
                    <li {{ Route::is('admin/references*') ? 'class=active' : '' }}>
                        <a href="{{ route('backend.references.list') }}">
                            <i class="material-icons">verified_user</i>
                            <span>References</span>
                        </a>
                    </li>
                @endif

                @if (auth()->user()->can('list_finances', ''))
                    <li>
                        <a href="javascript:void(0);"
                           class="menu-toggle {{ (request()->is('admin/finances*') || request()->is('admin/payment-method*')) ? 'toggled' : '' }}">
                            <i class="material-icons">payment</i>
                            <span>Finance</span>
                        </a>
                        <ul class="ml-menu">
                            @if (auth()->user()->can('list_finances'))
                                <li {{ request()->is('admin/finances*') ? 'class=active' : '' }}>
                                    <a href="{{ route('backend.finance.list') }}">
                                        <span>List of finance</span>
                                    </a>
                                </li>
                            @endif

                            @if (auth()->user()->can('show_payment_method'))
                                <li {{ request()->is('admin/payment-method*') ? 'class=active' : '' }}>
                                    <a href="{{ route('backend.payment.method.list') }}">
                                        <span>Payment Method</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if (auth()->user()->can('settings'))
                    <li {{ Route::is('admin/settings') ? 'class=active' : '' }}>
                        <a href="{{ route('backend.settings.list') }}">
                            <i class="material-icons">settings</i>
                            <span>Settings</span>
                        </a>
                    </li>
                @endif

                @if (auth()->user()->can(['clients', 'members', 'roles']))
                    <li>
                        <a href="javascript:void(0);"
                           class="menu-toggle {{ request()->is('admin/users*') ? 'toggled' : '' }}">
                            <i class="material-icons">account_box</i>
                            <span>Users</span>
                        </a>
                        <ul class="ml-menu">
                            @if (auth()->user()->can('clients'))
                                <li {{ request()->is('admin/users/clients*') ? 'class=active' : '' }}>
                                    <a href="{{ route('backend.users.clients.list') }}">
                                        <span>Clients</span>
                                    </a>
                                </li>
                            @endif
                            @if (auth()->user()->can('members'))
                                <li {{ request()->is('admin/users/members*') ? 'class=active' : '' }}>
                                    <a href="{{ route('backend.users.members.list') }}">
                                        <span>Members</span>
                                    </a>
                                </li>
                            @endif
                            @if (auth()->user()->can('roles'))
                                <li {{ request()->is('admin/users/roles*') ? 'class=active' : '' }}>
                                    <a href="{{ route('backend.roles.list') }}">
                                        <span>Roles</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2017 <a href="javascript:void(0);">PSGo Admin Panel</a>.
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>

{{--FeedBack Jira--}}

<script type="text/javascript" src="https://projektcs.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-z4cx5m/b/1/7ebd7d8b8f8cafb14c7b0966803e5701/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=dd4bea11"></script>