<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1200">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} @yield('title')</title>

    <!-- Bootstrap Core Css -->
    @section('css')
    <link href="{{ asset('bsbmd/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet">
    <link href="{{ asset('bsbmd/plugins/node-waves/waves.css') }}" rel="stylesheet">
    <link href="{{ asset('bsbmd/plugins/animate-css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('bsbmd/plugins/morrisjs/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('bsbmd/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('bsbmd/css/themes/all-themes.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/jBox/Source/jBox.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}"
          rel="stylesheet">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    @show

    @yield('page-css')
</head>
<body class="theme-indigo">
@include('backend.layouts.partials.loader')
<div class="overlay"></div>
@include('backend.layouts.partials.menu')

<section class="content">
    @yield('content')
</section>

@section('script')
<script src="{{ asset('bsbmd/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('bsbmd/plugins/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('bsbmd/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('bsbmd/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('bsbmd/plugins/node-waves/waves.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('plugins/jBox/Source/jBox.min.js') }}"></script>
<script src="{{ asset('plugins/momentjs/moment.js') }}"></script>
<script src="{{ asset('plugins/jquery/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
@show
@yield('page-script')

@section('script-bottom')
<script src="{{ asset('bsbmd/js/admin.js') }}"></script>
<script src="{{ asset('js/admin/helper.js') }}"></script>
@show

@include('backend.partials.message')

</body>
</html>