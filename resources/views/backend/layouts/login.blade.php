<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
    @section('css')
    <link href="{{ asset('bsbmd/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('bsbmd/plugins/node-waves/waves.css') }}" rel="stylesheet">
    <link href="{{ asset('bsbmd/plugins/animate-css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('bsbmd/plugins/morrisjs/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('bsbmd/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('bsbmd/css/themes/all-themes.csss') }}" rel="stylesheet">

    <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    @show
</head>

<body class="login-page">
<div class="login-box">
    <div class="logo">
        <small>PSGo Admin Panel</small>
    </div>
    <div class="card">
        <div class="body">
            @yield('content')
        </div>
    </div>
</div>

@section('script')
    <script src="{{ asset('bsbmd/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('bsbmd/plugins/bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('bsbmd/plugins/node-waves/waves.js') }}"></script>
    <script src="{{ asset('bsbmd/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('bsbmd/js/admin.js') }}"></script>
    <script src="{{ asset('bsbmd/js/pages/examples/sign-in.js') }}"></script>
@show

</body>

</html>