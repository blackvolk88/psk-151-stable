@if (Session::has('modal-message'))
    <script type="text/javascript">
        $(document).ready(function() {
            $.adminHelper.notification(
                "{{ Session::get('modal-message')['type'] }}",
                "{{ Session::get('modal-message')['message'] }}"
            );
        });
    </script>
@endif