<div>New user registration on system</div>
<table>
    <thead>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Skype</th>
    </thead>
    <tbody>
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->phone }}</td>
            <td>{{ $user->skype }}</td>
        </tr>
    </tbody>
</table>