@extends('frontend.layouts.auth')

@section('content')

<body class="signup-page web-signup web-login-signup">
    <div class="signup-box">
        <div class="card">
            <div class="body">
                <form method="POST" action="{{ route('registration.submit') }}">
                    {{ csrf_field() }}

                    <div class="msg">Register a new user</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line {{ $errors->has('name') ? 'error' : '' }}">
                            <input type="text" class="form-control" name="name" placeholder="Name Surname" required autofocus>
                            @if ($errors->has('name'))
                                <label class="error" for="name">{{ $errors->first('name') }}</label>
                            @endif
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line {{ $errors->has('email') ? 'error' : '' }}">
                            <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                            @if ($errors->has('email'))
                                <label class="error" for="email">{{ $errors->first('email') }}</label>
                            @endif
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">info</i>
                        </span>
                        <div class="form-line {{ $errors->has('skype') ? 'error' : '' }}">
                            <input type="text" class="form-control" name="skype" placeholder="Skype" required>
                            @if ($errors->has('skype'))
                                <label class="error" for="skype">{{ $errors->first('skype') }}</label>
                            @endif
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">phone</i>
                        </span>
                        <div class="form-line {{ $errors->has('phone') ? 'error' : '' }}">
                            <input type="tel" class="form-control mobile-phone-number" name="phone" placeholder="Phone" required>
                            @if ($errors->has('phone'))
                                <label class="error" for="phone">{{ $errors->first('phone') }}</label>
                            @endif
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line {{ $errors->has('password') ? 'error' : '' }}">
                            <input type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>
                            @if ($errors->has('password'))
                                <label class="error" for="password">{{ $errors->first('password') }}</label>
                            @endif
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password_confirmation" minlength="6" placeholder="Confirm Password" required>
                        </div>
                    </div>
                    <button class="btn btn-block btn-lg bg-indigo waves-effect" type="submit">SIGN UP</button>

                    <div class="m-t-25 m-b--5 align-center">
                        <a href="{{ route('login') }}">You already have a membership?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
@endsection

@section('page-script')
    <script src="{{ asset('plugins/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('js/frontend/signup/main.js') }}"></script>
@endsection