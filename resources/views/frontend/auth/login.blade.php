@extends('frontend.layouts.auth')

@section('content')
    <body class="login-page web-login web-login-signup">
        <div class="login-box">
            <div class="card">
                <div class="body">
                    <form action="{{ route('login.submit') }}" method="POST">
                        {{ csrf_field() }}

                        <div class="msg">Sign in to start your session</div>
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                            <div class="form-line {{ $errors->has('email') ? 'error' : '' }}">
                                <input type="text" class="form-control" name="email" placeholder="Email" required autofocus>
                                @if ($errors->has('email'))
                                    <label class="error" for="email">{{ $errors->first('email') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 p-t-10">
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} class="filled-in chk-col-indigo">
                                <label for="remember">Remember Me</label>
                            </div>
                            <div class="col-xs-4">
                                <button class="btn btn-block bg-indigo waves-effect" type="submit">SIGN IN</button>
                            </div>
                        </div>
                        <div class="row p-t-5 m-b-0">
                            <div class="col-xs-6 m-b-0">
                                <a href="{{ route('registration') }}">Register Now!</a>
                            </div>
                            <div class="col-xs-6 align-right m-b-0">
                                <a href="#">Forgot Password?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
@endsection