@if (Session::has('modal-message'))
    <a href="#flash-message" id="call-message" class="fancy" style="display:none;"></a>
    <div id="flash-message" class="modal_window cash_window">
        <div class="title">System notification</div>
        <p>{{ Session::get('modal-message') }}</p>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $.main.displayFlashMessage();
        });
    </script>
@endif

@if (Session::has('modal-message-array'))
    <a href="#flash-message" id="call-message" class="fancy" style="display:none;"></a>
    <div id="flash-message" class="modal_window">
        <div class="title">System notification</div>
        <table class="modal-table">
            <tbody>
                @foreach (Session::get('modal-message-array') as $key => $message)
                    <tr>
                        <td>{{ $message['key'] }}</td>
                        <td class="text-{{ $message['message-type'] }}">{{ $message['message'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $.main.displayFlashMessage();
        });
    </script>
@endif