<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=1200">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext"
          rel="stylesheet">

    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.fancybox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.structure.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.theme.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/panels-style.css') }}" rel="stylesheet">

    @yield('page-css')

    <script src="{{ asset('plugins/jquery/jquery-1.11.1.min.js') }}"></script>

    <!--[if lt IE 9]>
    <script>
        document.createElement('header');
        document.createElement('figure');
        document.createElement('section');
        document.createElement('main');
        document.createElement('aside');
        document.createElement('footer');
    </script>
    <![endif]-->
</head>
<body>
@if (Session::has('menu-close-mode'))
    <div class="page close-panel">
        @else
            <div class="page">
                @endif
                <aside>
                    <div class="fixed-height">
                        <header>
                            <a href="">
                                <img class="logo-icon" src="{{ asset('images/frontend/logo-icon.png') }}" alt="">
                                <img class="logo-text" src="{{ asset('images/frontend/logo-text.png') }}" alt="">
                            </a>
                        </header>
                        <div class="user-info">
                            <div class="user-icon">
                                @if (is_null(auth()->user()->photo_url ))
                                    <img class="ava" src="/images/frontend/smith-person-icon.png" alt="">
                                @else
                                    <img class="ava" src="/storage/{{ auth()->user()->photo_url }}" alt="">
                                @endif
                            </div>
                            <div class="user-name"><span>{{ auth()->user()->name }}</span></div>
                        </div>
                        <div class="panel-actions">
                            <a href="#" id="logout-button"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            <a class="toggle-aside"><i class="fa fa-bars" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <ul class="user-actions">
                        <li>
                            <a href="{{ route('workdesk') }}" class="{{ request()->is('workdesk*') ? 'active' : '' }}">
                                <i class="fa fa-desktop" aria-hidden="true"></i>
                                <span>Working Desk</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('profile') }}" class="{{ request()->is('profile*') ? 'active' : '' }}">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span>Personal Info</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('financial') }}"
                               class="{{ request()->is('financial*') ? 'active' : '' }}">
                                <i class="fa fa-credit-card" aria-hidden="true"></i>
                                <span>Financial Data</span>
                            </a>
                        </li>
                        <li title="Support skype">
                            <a href="skype:PsgoAdmin">
                                <i class="fa fa-skype" aria-hidden="true"></i>
                                <span>Support skype</span>
                            </a>
                        </li>
                        <li class="skype-name"><a><span class="our-skype">Our Skype: PsgoAdmin</span></a></li>
                    </ul>
                    <footer>
                        2017 © PsGo
                    </footer>
                </aside>
                <div class="work-window">
                    @yield('content')
                </div>
            </div>
            <div class="image-tooltip">
                <div class="photo-item">
                    <label>
                        <div class="img-overlay">
                            <img class="main-thumb" src="" alt="">
                        </div>
                    </label>
                </div>
            </div>
            <script src="{{ asset('plugins/device.min.js') }}"></script>
            <script src="{{ asset('plugins/jquery/jquery-ui.min.js') }}"></script>
            <script src="{{ asset('plugins/jquery/jquery.fancybox.min.js') }}"></script>
            <script src="{{ asset('plugins/jquery/jquery.touchSwipe.min.js') }}"></script>
            <script src="{{ asset('plugins/jquery/jquery.formstyler.min.js') }}"></script>
            <script src="{{ asset('plugins/easing.js') }}"></script>
            <script src="{{ asset('plugins/jquery/jquery.mousewheel.js') }}"></script>
            <script src="{{ asset('plugins/jquery/jquery.mCustomScrollbar.concat.min.js') }}"></script>
            <script src="{{ asset('plugins/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
            <script src="{{ asset('js/frontend/main.js') }}"></script>
            <script src="{{ asset('js/frontend/helper.js') }}"></script>
            <script src="{{ asset('js/frontend/designer/filters.js') }}"></script>
            @yield('page-script')
            @include('frontend.partials.message')
            <a href="#flash-notice" id="notice-message" class="fancy" style="display:none;"></a>
            <div id="flash-notice" class="modal_window">
                <div class="title">Notice</div>
                <div id="notice-content"></div>
            </div>

            {{--FeedBack Jira--}}

            <script type="text/javascript" src="https://projektcs.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-z4cx5m/b/1/7ebd7d8b8f8cafb14c7b0966803e5701/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=dd4bea11"></script>
</body>
</html>