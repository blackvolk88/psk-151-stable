@extends('frontend.layouts.auth')

@section('content')
<body class="login-page web-login web-login-signup">
    <div class="login-box">
        <div class="card">
            <div class="body">
                <div class="msg">Registration Confirmed</div>
                <div class="login-message-text">
                    Your Email is successfully verified. Click here to <br><br><a href="{{ route('login') }}" class="bg-indigo waves-effect">Login</a>
                </div>
            </div>
        </div>
    </div>
</body>
@endsection