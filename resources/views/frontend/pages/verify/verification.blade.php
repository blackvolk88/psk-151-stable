@extends('frontend.layouts.auth')

@section("content")
<body class="login-page web-login web-login-signup">
    <div class="login-box">
        <div class="card">
            <div class="body">
                <div class="msg">Registration Confirmation</div>
                <div class="login-message-text">
                    You have successfully registered. An email is sent to you for verification.
                </div>
            </div>
        </div>
    </div>
</body>
@endsection