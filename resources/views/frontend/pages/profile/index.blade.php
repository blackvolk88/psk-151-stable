@extends('frontend.layouts.designer')

@section('content')
    <div class="work-space">
        <div class="">
            <h1>Personal Info</h1>
            <div class="user-data-wrap">
                <div class="user-photo">
                    @if (is_null(auth()->user()->photo_url))
                        <img class="ava" src="/images/frontend/smith-person-icon.png" alt="">
                    @else
                        <img class="ava" src="/storage/{{ auth()->user()->photo_url }}" alt="">
                    @endif
                    <form id="upload-photo-form" method="post" action="{{ route('profile.photo.upload') }}"
                          enctype='multipart/form-data'>
                        {{ csrf_field() }}
                        <input id="upload-photo" type="file" accept="image/*" name="photo" placeholder="Update photo">
                    </form>
                </div>
                <div class="user-data static-data">
                    <table>
                        <tr>
                            <td><label>Name</label></td>
                            <td>
                                <div class="edit-form"><input class="edit-field" type="text" id="profile-name"
                                                              value="{{ auth()->user()->name }}"></div>
                                <div class="form-text"><span>{{ auth()->user()->name }}</span></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Email</label></td>
                            <td>
                                <div class="edit-form"><input class="edit-field" type="email" id="profile-email"
                                                              value="{{ auth()->user()->email }}"></div>
                                <div class="form-text"><span>{{ auth()->user()->email }}</span></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Phone number</label></td>
                            <td>
                                <div class="edit-form"><input class="edit-field mobile-phone-number" type="tel"
                                                              placeholder="Phone"
                                                              id="profile-phone" value="{{ auth()->user()->phone }}">
                                </div>
                                <div class="form-text"><span>{{ auth()->user()->phone }}</span></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Skype</label></td>
                            <td>
                                <div class="edit-form"><input class="edit-field" type="tel" id="profile-skype"
                                                              value="{{ auth()->user()->skype }}"></div>
                                <div class="form-text"><span>{{ auth()->user()->skype }}</span></div>
                            </td>
                        </tr>
                        <tr class="empty">
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><label>Earned</label></td>
                            <td>
                                <div class="static-text"><span>{{ auth()->user()->balance->balance }}</span></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Photos processed</label></td>
                            <td>
                                <div class="static-text"><span>{{ $processed_files }}</span></div>
                            </td>
                        </tr>
                        <tr class="empty">
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><label>Registration</label></td>
                            <td>
                                <div class="static-text"><span>{{ auth()->user()->created_at->diffForHumans() }}</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Last activity</label></td>
                            <td>
                                <div class="static-text"><span>{{ auth()->user()->last_login->diffForHumans() }}</span>
                                </div>
                            </td>
                        </tr>
                        <tr class="empty">
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><label>Passport</label></td>
                            <td>
                                <form id="upload-passport-file-form" class="edit-form" method="post"
                                      action="{{ route('profile.passport.upload') }}" enctype='multipart/form-data'>
                                    {{ csrf_field() }}
                                    <input id="upload-passport-file" class="edit-field" type="file" name="file"
                                           accept="image/*" placeholder="Change file">
                                </form>
                                <div class="form-text">
                                    @if (auth()->user()->passport_url)
                                        <a href="/storage/{{ auth()->user()->passport_url }}" target="_blank">View
                                            file</a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr class="empty">
                            <td></td>
                            <td></td>
                        </tr>

                        @forelse($payment_methods as $payment_method)
                            <tr class="payment-data-row {{ empty($payment_method->userPaymentMethod->number_card) ? 'empty-value' : '' }}">
                                <td><label for="{{$payment_method->id}}">{{$payment_method->name}}</label></td>
                                <td>
                                    <div class="edit-form"><input class="edit-field payment-methods" type="text"
                                                                  name="payment-methods[{{$payment_method->id}}]"
                                                                  id="{{$payment_method->id}}"
                                                                  value="{{ !empty($payment_method->userPaymentMethod) ? $payment_method->userPaymentMethod->number_card : ''  }}">
                                        <label><input type="radio" name="default-payment-method"
                                                      class="default-payment-method"
                                                      value="1"
                                                    {{ (!empty($payment_method->userPaymentMethod) && $payment_method->userPaymentMethod->default == 1) ? 'checked' : ''  }}
                                                    {{ empty($payment_method->userPaymentMethod->number_card) ? 'disabled' : '' }}
                                            ><i></i></label>
                                    </div>
                                    <div class="form-text">
                                        <span>{{ !empty($payment_method->userPaymentMethod) ? $payment_method->userPaymentMethod->number_card : '' }}</span>
                                       @if(!empty($payment_method->userPaymentMethod) && $payment_method->userPaymentMethod->default == 1)
                                        <i title="default payment method" class="fa fa-check"
                                                aria-hidden="true"></i>
                                           @endif
                                    </div>

                                </td>
                            </tr>
                            @endforeach
                            <tr class="empty">
                                <td></td>
                                <td></td>
                            </tr>
                    </table>
                    <button class="action-button blue-button edit-action">Edit</button>
                    <input type="submit" class="action-button green-button" value="Save">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('js/frontend/profile/main.js') }}"></script>
@endsection