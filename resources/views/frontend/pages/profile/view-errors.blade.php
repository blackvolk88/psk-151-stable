<table class="text-table">
    <tbody>
        @foreach ($messages as $message)
            @foreach ($message as $error)
                <tr>
                    <td>{{ $error }}</td>
                </tr>
            @endforeach
        @endforeach
    </tbody>
</table>