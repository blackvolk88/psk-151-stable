@extends('frontend.layouts.designer')

@section('content')
    <div class="work-space">
        <div class="">
            <h1>Financial Data</h1>
            <ul class="options">
                <li>
                    Your rating:
                    <strong>{{ auth()->user()->designerLevel->first()->level }}</strong>
                </li>
                <li>
                    You have:
                    <strong>{{ Helper::formatPrice(auth()->user()->balance->balance) }}</strong>
                </li>
            </ul>
            <div class="buttons-row">
                <div class="buttons-td-left">
                    <a class="picker-wrap"><input type="text" placeholder="Select a date" class="datepicker"
                                                  id="datepicker"></a>
                </div>

                <div class="buttons-td-right">
                    @if(auth()->user()->canCacheOut())
                        <a href="#cash" class="action-button blue-button fancy">
                            {{ Html::image('images/frontend/cash-icon.png') }}Cash Out</a>
                    @else
                        <a href="#" class="action-button blue-button disable">
                            {{ Html::image('images/frontend/cash-icon.png') }}Cash Out</a>
                        <span class="notice">There is not enough funds to withdraw on your account.</span>
                    @endif
                </div>


            </div>
        </div>

        <div class="tabs_wrap">
            <input id="tab1" class="tabsstyle" type="radio" name="tab" checked="checked">
            <input id="tab2" class="tabsstyle" type="radio" name="tab">
            <div class="tabs-buttons clearfix">
                <label for="tab1" id="tab_l1">Transfers</label>
                <label for="tab2" id="tab_l2">Payments</label>
            </div>
            <div class="tabs_cont">
                <div class="tab" id="tab11">
                    <table class="text-table">
                        <thead>
                        <tr>
                            <td>Date</td>
                            <td>Sum</td>
                            <td>For</td>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($accruals as $accrual)
                            <tr>
                                <td>{{ $accrual->created_at->format('Y-m-d H:m') }}</td>
                                <td>{{ $accrual->accrual }}</td>
                                <td style="text-align: left;"><a class="tooltip"
                                                                 data-src="/storage/{{ $accrual->file->thumb_path }}">{{ $accrual->file->storage_name }}</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">Accruals not found</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="tab" id="tab22">

                    <table class="text-table">
                        <thead>
                        <tr>
                            <td>Date</td>
                            <td>Sum</td>
                            <td>Type</td>
                            <td>Number Card</td>
                            <td>Status</td>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($withdraws as $withdraw)
                            <tr>
                                <td>{{ $withdraw->created_at->format('Y-m-d H:m') }}</td>
                                <td>{{ $withdraw->amount }}</td>
                                <td>{{ $withdraw->paymentMethod->name }}</td>
                                <td>{{  substr_replace($withdraw->number_card, '******', -10, 6) }}</td>
                                <td>{{ $withdraw->getNameStatus() }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">Applications for withdrawal of money not found</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div id="cash" class="modal_window cash_window">
        <div class="title">Cash Out</div>

        @if($payment_method->count() > 0 && auth()->user()->existsPaymentMethod())
            {{ Form::open(array('route' => 'finance.withdraw.new.submit','id'=>'create-withdraw')) }}

            {{ Form::label('amount', 'Sum')}}
            {{ Form::text('amount', null, array('placeholder' => Helper::formatPrice()))}}

            {{ Form::label('type', 'Pay System')}}
            {{ Form::select('type', $payment_method, $defaultSelectPayment)}}

            {{ Form::submit('Confirm', array('class'=> 'action-button blue-button'))}}
            {{ Form::close() }}
        @elseif(!auth()->user()->existsPaymentMethod())
            <p>You did not enter card information</p>
            <input class="action-button blue-button" type="submit" value="Confirm">
        @else
            <p>Service is temporarily unavailable</p>
            <input class="action-button blue-button" type="submit" value="Confirm">
        @endif
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('js/frontend/financial/main.js') }}"></script>
@endsection