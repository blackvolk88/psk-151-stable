<div class="title">{{ $filename }}</div>
<ol>
    @foreach ($reasons as $reason)
        <li>{{ $reason->name }}</li>
    @endforeach
</ol>
<p>{{ $comment }}</p>