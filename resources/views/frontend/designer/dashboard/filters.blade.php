<div class="fixed-height">
    <div class="panel-item filters-block">
        @include('frontend.designer.dashboard.partials.filters')
    </div>
    @include('frontend.designer.dashboard.partials.process-batches')
</div>
<div class="scroll-wrap">
    @include('frontend.designer.dashboard.partials.batches-list')
</div>