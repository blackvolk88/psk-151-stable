<strong>Filters</strong>
@if (!empty($difficulties))
    <select id="filter-difficulties" class="filter-items">
        <option value="">Difficulty</option>
        @foreach ($difficulties as $difficulty)
            @if (isset($currentDifficulty) && $currentDifficulty == $difficulty->id)
                <option selected="selected" value="{{ $difficulty->id }}">{{ $difficulty->name }}</option>
            @else
                <option value="{{ $difficulty->id }}">{{ $difficulty->name }}</option>
            @endif
        @endforeach
    </select>
@endif
@if (!empty($types))
    <select id="filter-work-types" class="filter-items">
        <option value="">Work type</option>
        @foreach ($types as $type)
            @if (isset($currentType) && $currentType == $type->id)
                <option selected="selected" value="{{ $type->id }}">{{ $type->name }}</option>
            @else
                <option value="{{ $type->id }}">{{ $type->name }}</option>
            @endif
        @endforeach
    </select>
@endif
<select>
    <option>Photos quantity</option>
    <option>1</option>
    <option>5</option>
    <option>10</option>
    <option>20</option>
</select>