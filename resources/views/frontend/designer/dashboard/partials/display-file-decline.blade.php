<div class="photo-item" title="{{ $file->storage_name }}">
    <a href="#" target-id="{{ $file->id }}" class="details view-decline-reason">Details</a>
    {{--<a href="{{ route('batch.file.decline.download', $file->id) }}" class="download" title="Download"><img src="{{ asset('images/frontend/download-all-icon.png') }}" alt=""></a>--}}
    <label>
        <input type="checkbox" target-id="{{ $file->id }}">
        <div class="img-overlay">
            <div class="check"><i class="fa fa-check" aria-hidden="true"></i></div>
            <img class="main-thumb" src="/storage/{{ $file->modified->thumb_path }}" alt="">
            <div class="overlay"></div>
            <div class="small-thumb">
                <img src="/storage/{{ $file->thumb_path }}" alt="">
            </div>
            <div class="status declined">{{ $file->status->name }}</div>
        </div>
    </label>
    <a href="#details_{{ $file->id }}" class="fancy" id="details_fancy_{{ $file->id }}" style="display: none"></a>
</div>



<div id="details_{{ $file->id }}" class="modal_window info_window"></div>