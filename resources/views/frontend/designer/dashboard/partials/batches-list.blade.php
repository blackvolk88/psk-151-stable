@if ($availableBatches->count())
    <div class="panel-item batches-block">
        <strong>Batches in row</strong>
        <ul>
            @foreach ($availableBatches as $batch)
                <li>
                    @if (isset($currentBatch) && $currentBatch == $batch->id)
                        <a href="{{ route('batch.available.preview', $batch->id) }}" class="active">
                            <i class="fa" aria-hidden="true"></i>{{ $batch->id }}
                        </a>
                    @else
                        <a href="{{ route('batch.available.preview', $batch->id) }}">
                            <i class="fa" aria-hidden="true"></i>{{ $batch->id }}
                        </a>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if ($limitExceeded)
    <div class="panel-item batches-block">
        <div class="disable-message">
            <i class="fa fa-lock" aria-hidden="true"></i>
            <div>Your packet limit has been exceeded. You can not take more packages.</div>
        </div>
    </div>
@endif