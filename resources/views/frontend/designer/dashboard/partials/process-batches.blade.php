@if ($takenBatches->count())
    <div class="panel-item batches-block process">
        <strong>Batches in process</strong>
        <ul>
            @foreach ($takenBatches as $batch)
                <li>
                    @if (isset($currentBatch) && $currentBatch == $batch->id)
                        <a href="{{ route('batch.assigned.view', $batch->id) }}" class="active">
                            <i class="fa" aria-hidden="true"></i>{{ $batch->id }}
                        </a>
                    @else
                        <a href="{{ route('batch.assigned.view', $batch->id) }}">
                            <i class="fa" aria-hidden="true"></i>{{ $batch->id }}
                        </a>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
@endif