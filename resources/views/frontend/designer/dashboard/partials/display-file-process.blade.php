<div class="photo-item" title="{{ $file->storage_name }}">
    <label>
        <input type="checkbox" target-id="{{ $file->id }}">
        <div class="img-overlay">
            <div class="check"><i class="fa fa-check" aria-hidden="true"></i></div>
            <img class="main-thumb" src="/storage/{{ $file->thumb_path }}" alt="">
            <div class="overlay"></div>

            <div class="status default">{{ $file->status->name }}</div>
        </div>
    </label>
</div>