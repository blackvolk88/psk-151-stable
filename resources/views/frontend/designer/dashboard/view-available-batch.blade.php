@extends('frontend.layouts.designer')

@section('content')
    <div class="work-panel">
        @include('frontend.designer.dashboard.filters')
    </div>
    <div class="work-space">
        <div class="fixed-height">
            <h1>Photos of Batch {{ $batch->id }}</h1>
            <ul class="options">
                <li>Quantity: <strong>{{ $batch->files->count() }}</strong></li>
                <li>Value: <strong>{{ FileHelper::bytesToHuman($batch->files->sum('size')) }}</strong></li>
                <li>
                    Files:
                    <strong>
                        @foreach ($formats as $format)
                            {{ FileHelper::mimeToHuman($format) }}
                        @endforeach
                    </strong>
                </li>
                <li>Difficulty: <strong>{{ $batch->difficulty->name }}</strong></li>
                <li>Work type: <strong>{{ $batch->type->name }}</strong></li>
                <li>One photo cost: <strong>{{ Helper::formatPrice($costPhoto) }}</strong></li>
            </ul>
            @if (!is_null($batch->comment))
                <ul class="options">
                    <li><strong>{{ $batch->comment }}</strong></li>
                </ul>
            @endif
            <div class="buttons-row">
                <div class="buttons-td-left">
                    <form method="post" action="{{ route('batch.take', $batch->id) }}">
                        {{ csrf_field() }}
                        <button type="submit" class="action-button green-button"><img src="{{ asset('images/frontend/plus-icon.png') }}" alt="">Take to process</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="scroll-wrap">
            <div class="photos-wrapper">
                <div class="clearfix">
                    @foreach ($batch->files as $file)
                        <div class="photo-item">
                            <label>
                                <div class="img-overlay">
                                    <img class="main-thumb" src="/storage/{{ $file->thumb_path }}" alt="">
                                </div>
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection