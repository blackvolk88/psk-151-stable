@extends('frontend.layouts.designer')

@section('page-css')
    <link href="{{ asset('plugins/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="work-panel">
        @include('frontend.designer.dashboard.filters')
    </div>
    <div class="work-space">
        <div class="fixed-height">
            <h1>Photos of Batch {{ $batch->id }}</h1>
            <ul class="options">
                <li>Quantity: <strong>{{ $batch->files->count() }}</strong></li>
                <li>Value: <strong>{{ FileHelper::bytesToHuman($batch->files->sum('size')) }}</strong></li>
                <li>
                    Files:
                    <strong>
                        @foreach ($formats as $format)
                            {{ FileHelper::mimeToHuman($format) }}
                        @endforeach
                    </strong>
                </li>
                <li>Difficulty: <strong>{{ $batch->difficulty->name }}</strong></li>
                <li>Work type: <strong>{{ $batch->type->name }}</strong></li>
                <li>One photo cost: <strong>{{ Helper::formatPrice($costPhoto) }}</strong></li>
            </ul>
            @if (!is_null($batch->comment))
                <ul class="options">
                    <li><strong>{{ $batch->comment }}</strong></li>
                </ul>
            @endif
            <div class="buttons-row">
                <form class="dropzone" method="POST" enctype="multipart/form-data"
                      action="{{ route('batch.file.upload', $batch->id) }}">
                    {{ csrf_field() }}

                    <div class="dz-message">
                        <div class="drag-icon-cph">
                            <i class="fa fa-upload" aria-hidden="true"></i>
                        </div>
                        <strong>Drop files here or click to upload</strong>
                    </div>
                    <div class="fallback">
                        <input name="file" type="file" multiple/>
                    </div>
                </form>
            </div>
            <div class="buttons-row">
                <div class="buttons-td-left">
                    <button id="download-all-button" target-url="{{ route('batch.assigned.download', $batch->id) }}"
                            class="action-button blue-button"><img
                                src="{{ asset('images/frontend/download-all-icon.png') }}" alt="">Download All
                    </button>
                    <button id="download-selected-button" target-id="{{ $batch->id }}"
                            class="action-button green-button"><img
                                src="{{ asset('images/frontend/download-sel-icon.png') }}" alt="">Download Selected
                    </button>

                    @if($filesDeclineBatch->count() > 0)
                        <button id="download-decline-selected-button" target-url="{{ route('batch.files.decline.download', $batch->id) }}"
                                class="action-button orange-button"><img
                                    src="{{ asset('images/frontend/download-sel-icon.png') }}" alt="">Download Declined Files
                        </button>
                    @endif

                    <button id="unselect" class="cancel-button" style="display:none;">Unselect All</button>
                </div>
                <div class="buttons-td-right">

                    @if (isset($availableRefuse) && $availableRefuse)
                        <button id="refuse-batch-button" target-id="{{ $batch->id }}" class="action-button red-button">
                            <img src="{{ asset('images/frontend/close-icon.png') }}" alt="">Refuse Processing
                        </button>
                    @else
                        <label>Sort by:</label>
                        <select id="batch-sort-status" target-id="{{ $batch->id }}">
                            <option value="">Status</option>
                            @foreach ($fileStatuses as $status)
                                @if (isset($currentStatus) && $currentStatus == $status->id)
                                    <option selected="selected" value="{{ $status->id }}">{{ $status->name }}</option>
                                @else
                                    <option value="{{ $status->id }}">{{ $status->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    @endif
                </div>
            </div>
        </div>
        <div class="scroll-wrap">
            <div class="photos-wrapper">
                <div class="clearfix">
                    @forelse ($batchFiles as $file)
                        @include('frontend.designer.dashboard.partials.display-file-' . $file->status->template, ['file' => $file])
                    @empty
                        <div class="photo-item">Files not found</div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('plugins/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ asset('js/frontend/designer/batch.js') }}"></script>
    @if (isset($availableRefuse))
        <script>
            $(document).ready(function () {
                $.designerBatchModule.initRefuseNotice();
            });
        </script>
        <a href="#flash-refuse-notice" id="notice-refuse-message" class="fancy" style="display:none;"></a>
        <div id="flash-refuse-notice" class="modal_window">
            <div class="title">Refuse notification</div>
            <div id="refuse-notice-content">You can refuse this batch until you download at least one file</div>
            <div class="buttons-row">
                <button class="action-button blue-button confirm-refuse">confirm</button>
            </div>
        </div>
    @endif
@endsection