<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <title>PsGo</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="{{ asset('bsbmd/css/style.css') }}" rel="stylesheet">


    </head>
    <body class="web-login web-login-signup home-page">
    <div class="login-box">

            @if (Route::has('login'))
                <div class="top-right links">

                        <a href="{{ route('login') }}" class="btn btn-block bg-indigo waves-effect">Login</a>
                        <a href="{{ route('registration') }}"  class="btn btn-block bg-purple waves-effect">Register</a>

                </div>
            @endif

                    <div class="content">
                        <h1>
                        PsGoWork
                        </h1>

                        <div class="description">удаленная работа для ретушеров</div>
                    </div>

        </div>
    </body>
</html>
