<?php

/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@showLoginForm')->name('backend.login');
Route::post('login', 'Auth\LoginController@login')->name('backend.login.submit');
Route::post('logout', 'Auth\LoginController@logout')->name('backend.logout');

//Dashboard section
Route::group(['middleware' => ['auth:backend', 'permission:dashboard']], function () {
    Route::get('/', 'DashboardController@index')->name('backend.dashboard');
});

//List of Batches section
Route::group(['prefix' => 'batches/list', 'middleware' => ['auth:backend', 'permission:list_batches']], function () {
    Route::get('/', 'Batches\ListController@index')->name('backend.batches.list');
    Route::get('filter/{difficulty?}/{type?}/{status?}', 'Batches\ListController@filter');
    Route::delete('destroy/{batch}', 'Batches\ListController@destroy');
    Route::get('download/{batch}', 'Batches\ListController@download')->name('backend.batches.list.download');
    Route::get('preview/{batch}', 'Batches\ListController@preview')->name('backend.batches.list.preview');
    Route::patch('preview/update-comment/{batch}', 'Batches\ListController@updateComment')->name('backend.batches.update.comment');
});

//Batch moderating section
Route::group(['prefix' => 'batches/moderating', 'middleware' => ['auth:backend', 'permission:moderating_batches']], function () {
    Route::get('/', 'Batches\ManageController@index')->name('backend.batches.moderating');
    Route::get('filter/{format?}/{filmed?}/{uploaded?}', 'Batches\ManageController@filter');
    Route::get('view/{batch}', 'Batches\ManageController@view')->name('backend.batches.moderating.view');
    Route::delete('destroy/{batch}', 'Batches\ManageController@destroy');
    Route::post('ungroup-selected', 'Batches\ManageController@ungroupFiles');
    Route::post('new', 'Batches\ManageController@store')->name('backend.batches.create');
    Route::post('new/random', 'Batches\ManageController@storeRandom');
});

//List of Photos section
Route::group(['prefix' => 'photos/packages', 'middleware' => ['auth:backend', 'permission:list_photos']], function () {
    Route::get('list/{package?}', 'Photos\PackageController@index')->name('backend.photos.list');
    Route::get('new', 'Photos\PackageController@getStoreForm')->name('backend.photos.package.new.form');
    Route::post('new', 'Photos\PackageController@store')->name('backend.photos.package.new.form.submit');
    Route::get('view/{package}/{format?}/{search?}', 'Photos\PackageController@view')->name('backend.photos.package.view');
    Route::delete('destroy/{package}', 'Photos\PackageController@destroy');
    Route::post('upload/{package}', 'Photos\PackageController@upload')->name('backend.photos.package.upload.submit');
    Route::get('file/download/{file}', 'Photos\PackageController@download')->name('backend.photos.package.file.download');
    Route::get('file/preview/{file}', 'Photos\PackageController@previewFile');
    Route::delete('destroy-file/{package}/{file}', 'Photos\PackageController@destroyFile');
    Route::get('prepare-download/{package}', 'Photos\PackageController@getPrepareDownloadForm');
    Route::get('download-original/{package}', 'Photos\PackageController@downloadOriginalFiles')->name('backend.photos.package.download.original');
    Route::get('download-completed/{package}', 'Photos\PackageController@downloadCompletedFiles')->name('backend.photos.package.download.completed');
});

//Photos moderating section
Route::group(['prefix' => 'photos/moderating', 'middleware' => ['auth:backend', 'permission:moderating_photos']], function () {
    Route::get('/', 'Photos\ModeratingController@index')->name('backend.photos.moderating');
    Route::get('sort/{type}/{sort}', 'Photos\ModeratingController@sort');
    Route::get('view/{file}/{type}/{sort?}', 'Photos\ModeratingController@view')->name('backend.photos.moderating.view');
    Route::get('download/{file}', 'Photos\ModeratingController@download')->name('backend.photos.moderating.download');
    Route::get('decline-form/{file}', 'Photos\ModeratingController@getDeclineForm');
    Route::post('decline/{file}', 'Photos\ModeratingController@decline')->name('backend.photos.moderating.decline.submit');
    Route::post('approve', 'Photos\ModeratingController@approve');

    Route::get('multiple-moderating', 'Photos\ModeratingController@multipleMode')->name('backend.photos.moderating.multiple');
    Route::get('multiple-moderating/sort/{type}/{sort}', 'Photos\ModeratingController@sortMode');
    Route::post('multiple-moderating/view', 'Photos\ModeratingController@multipleView');
    Route::post('multiple-moderating/download-selected', 'Photos\ModeratingController@downloadSelected');
    Route::get('multiple-moderating/download', 'Photos\ModeratingController@prepareDownloadFiles')->name('backend.photos.moderating.prepare.download');
    Route::post('multiple-moderating/decline-form', 'Photos\ModeratingController@getMultipleDeclineForm');
    Route::post('multiple-moderating/decline', 'Photos\ModeratingController@multipleDecline');
});

//List of Releases section
Route::group(['prefix' => 'releases', 'middleware' => ['auth:backend', 'permission:list_releases']], function () {
    Route::get('list', 'Photos\ReleaseController@index')->name('backend.releases.list');
    Route::get('new', 'Photos\ReleaseController@getStoreForm')->name('backend.photos.release.new.form');
    Route::post('new', 'Photos\ReleaseController@store')->name('backend.photos.release.new.form.submit');
    Route::get('download/{release}', 'Photos\ReleaseController@download')->name('backend.photos.release.download');
    Route::get('view/{release}', 'Photos\ReleaseController@view')->name('backend.photos.release.view');
    Route::get('prepare-assign/{release}', 'Photos\ReleaseController@prepareAssignForm')->name('backend.photos.release.prepare.assign');
    Route::get('prepare-assign/{release}/package/{package}', 'Photos\ReleaseController@prepareAssignPackageFiles');
    Route::post('assign/{release}', 'Photos\ReleaseController@assign');
    Route::delete('destroy/{release}', 'Photos\ReleaseController@destroy');
});

//References section
Route::group(['prefix' => 'references', 'middleware' => ['auth:backend', 'permission:references']], function () {
    Route::get('/', 'References\MainController@index')->name('backend.references.list');
    Route::put('status/{status}', 'References\StatusController@edit');
    Route::get('work-type/new', 'References\WorkTypeController@getStoreForm')->name('backend.references.work-type.create');
    Route::post('work-type/new', 'References\WorkTypeController@store')->name('backend.references.work-type.create.submit');
    Route::get('work-type/edit/{type}', 'References\WorkTypeController@getEditForm')->name('backend.references.work-type.edit');
    Route::patch('work-type/edit/{type}', 'References\WorkTypeController@edit')->name('backend.references.work-type.edit.submit');
    Route::delete('work-type/destroy/{type}', 'References\WorkTypeController@destroy');
    Route::get('difficulty/new', 'References\DifficultyController@getStoreForm')->name('backend.references.difficulty.create');
    Route::post('difficulty/new', 'References\DifficultyController@store')->name('backend.references.difficulty.create.submit');
    Route::get('difficulty/edit/{difficulty}', 'References\DifficultyController@getEditForm')->name('backend.references.difficulty.edit');
    Route::patch('difficulty/edit/{difficulty}', 'References\DifficultyController@edit')->name('backend.references.difficulty.edit.submit');
    Route::delete('difficulty/destroy/{difficulty}', 'References\DifficultyController@destroy');
    Route::get('reject-reasons/new', 'References\RejectReasonsController@getStoreForm')->name('backend.references.reject.create');
    Route::post('reject-reasons/new', 'References\RejectReasonsController@store')->name('backend.references.reject.create.submit');
    Route::get('reject-reasons/edit/{reason}', 'References\RejectReasonsController@getEditForm')->name('backend.references.reject.edit');
    Route::patch('reject-reasons/edit/{reason}', 'References\RejectReasonsController@edit')->name('backend.references.reject.edit.submit');
    Route::delete('reject-reasons/destroy/{reason}', 'References\RejectReasonsController@destroy');
});

//Finance section
Route::group(['prefix' => 'finances', 'middleware' => ['auth:backend', 'permission:list_finances']], function () {
    Route::get('/', 'Finance\MainController@index')->name('backend.finance.list');
    Route::put('edit/{withdraw}', 'Finance\MainController@edit')->name('backend.finance.edit');
});

//Payment method
Route::group(['prefix' => 'payment-method', 'middleware' => ['auth:backend', 'permission:show_payment_method']], function () {
    Route::get('/', 'Finance\PaymentMethodController@index')->name('backend.payment.method.list');
    Route::get('new', 'Finance\PaymentMethodController@getStoreForm')->name('backend.payment.method.new.form');
    Route::post('new', 'Finance\PaymentMethodController@store')->name('backend.payment.method.new');
    Route::put('edit/{payment}', 'Finance\PaymentMethodController@edit')->name('backend.payment.method.edit');
    Route::put('edit/name/{payment}', 'Finance\PaymentMethodController@editName')->name('backend.payment.method.edit.name');
});


//Settings section
Route::group(['prefix' => 'settings', 'middleware' => ['auth:backend', 'permission:settings']], function () {
    Route::get('/', 'Settings\MainController@index')->name('backend.settings.list');
    Route::put('batch-coefficients/edit/{difficulty}', 'Settings\BatchCoefficientController@edit');
    Route::put('file-prices/edit/{price}', 'Settings\FilePricesController@edit');
    Route::put('designer-rates-coefficient/edit/{level}', 'Settings\DesignerRatesController@edit');
    Route::put('designer-batch-limit/edit/{level}', 'Settings\DesignerRatesController@editLimit');
    Route::put('global-setting/edit/{setting}', 'Settings\SettingsController@edit');
});

//Users clients section
Route::group(['prefix' => 'users/clients', 'middleware' => ['auth:backend', 'permission:clients']], function () {
    Route::get('list/{role?}/{search?}', 'Users\ClientsController@index')->name('backend.users.clients.list');
    Route::get('view/{user}', 'Users\ClientsController@view')->name('backend.users.clients.view');
    Route::get('edit/{user}', 'Users\ClientsController@getEditForm')->name('backend.users.clients.edit');
    Route::patch('edit/{user}', 'Users\ClientsController@edit')->name('backend.users.clients.edit.submit');
    Route::post('change-user-status/{user}', 'Users\ClientsController@changeStatus');
});

//Users members section
Route::group(['prefix' => 'users/members', 'middleware' => ['auth:backend', 'permission:members']], function () {
    Route::get('list/{role?}/{search?}', 'Users\MembersController@index')->name('backend.users.members.list');
    Route::get('new', 'Users\MembersController@getStoreForm')->name('backend.users.members.new.form');
    Route::post('new', 'Users\MembersController@store')->name('backend.users.members.new.submit');
    Route::get('view/{user}', 'Users\MembersController@view')->name('backend.users.members.view');
    Route::get('edit/{user}', 'Users\MembersController@getEditForm')->name('backend.users.members.edit');
    Route::patch('edit/{user}', 'Users\MembersController@edit')->name('backend.users.members.edit.submit');
    Route::post('change-user-status/{user}', 'Users\MembersController@changeStatus');
});

//Users roles section
Route::group(['prefix' => 'users/roles', 'middleware' => ['auth:backend', 'permission:roles']], function () {
    Route::get('/', 'Users\RolesController@index')->name('backend.roles.list');
    Route::get('edit/{role}', 'Users\RolesController@getEditForm')->name('backend.roles.edit');
    Route::patch('edit/{role}', 'Users\RolesController@edit')->name('backend.roles.edit.submit');
    Route::get('new', 'Users\RolesController@getStoreForm')->name('backend.roles.new');
    Route::post('new', 'Users\RolesController@store')->name('backend.roles.new.submit');
    Route::delete('destroy/{role}', 'Users\RolesController@destroy');
});