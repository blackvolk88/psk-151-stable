<?php

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WorkDeskController@welcome');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login.submit');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('registration', 'Auth\RegisterController@showRegistrationForm')->name('registration');
Route::post('registration', 'Auth\RegisterController@register')->name('registration.submit');
Route::get('registration/verify/{token}', 'Auth\RegisterController@verify');

Route::post('set-menu-mode', 'MenuController@mode');

Route::get('workdesk', 'WorkDeskController@index')->name('workdesk');

Route::get('workdesk/filter/{difficulty?}/{type?}', 'DesignerWorkDeskController@filter');
Route::get('workdesk/view-available-batch/{batch}', 'DesignerWorkDeskController@viewAvailableBatch')->name('batch.available.preview');
Route::post('workdesk/view-available-batch/{batch}', 'DesignerWorkDeskController@takeBatch')->name('batch.take');
Route::get('workdesk/view-assigned-batch/{batch}', 'DesignerWorkDeskController@viewAssignedBatch')->name('batch.assigned.view');
Route::get('workdesk/sort-assigned-batch/{batch}/status/{status}', 'DesignerWorkDeskController@sort');
Route::delete('workdesk/refuse-assigned-batch/{batch}', 'DesignerWorkDeskController@refuseBatch');
Route::get('workdesk/download-batch-files/{batch}', 'DesignerWorkDeskController@downloadFiles')->name('batch.assigned.download');
Route::post('workdesk/download-selected-files/{id}', 'DesignerWorkDeskController@downloadSelected');
Route::get('workdesk/download-decline-files/{batch}', 'DesignerWorkDeskController@downloadDeclineFiles')->name('batch.files.decline.download');
Route::get('workdesk/download-decline-file/{file}', 'DesignerWorkDeskController@downloadDeclineFile')->name('batch.file.decline.download');
Route::post('workdesk/upload-batch-files/{batch}', 'DesignerWorkDeskController@upload')->name('batch.file.upload');
Route::get('workdesk/view-designer-decline-file-reason/{file}', 'DesignerWorkDeskController@viewDeclineReason');

Route::get('financial', 'FinancialController@index')->name('financial');
Route::get('financial/filter/{date?}', 'FinancialController@filter');
Route::post('financial/withdraw/new', 'WithdrawController@store')->name('finance.withdraw.new.submit');

Route::get('profile', 'ProfileController@index')->name('profile');
Route::post('profile/edit', 'ProfileController@edit');
Route::post('profile/view-errors', 'ProfileController@displayErrors');
Route::post('profile/upload-photo', 'ProfileController@uploadPhoto')->name('profile.photo.upload');
Route::post('profile/upload-passport', 'ProfileController@uploadPassport')->name('profile.passport.upload');