<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MemberRegistration extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Send data (name, generating password)
     */
    protected $sendingData = [];

    /**
     * Create a new message instance.
     * @param array $data
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->sendingData = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.admin.members.new')->with([
            'name' => $this->sendingData['name'],
            'password' => $this->sendingData['password']
        ]);
    }
}