<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

use App\Models\Common\File;

class Accruals extends Model
{
    protected $fillable = ['user_id', 'file_id', 'accrual', 'coefficient', 'rate_coefficient', 'file_price'];

    public function getAccrualAttribute()
    {
        return number_format($this->attributes['accrual'], 2);
    }

    public function file()
    {
        return $this->belongsTo(File::class);
    }
}