<?php

namespace App\Models\Frontend;

use App\Models\Common\PaymentMethod;
use Illuminate\Database\Eloquent\Model;

class UserPaymentMethods extends Model
{
    public $timestamps = false;

    protected $fillable = ['user_id', 'payment_id', 'number_card', 'default'];


    public function paymentMethod(){
        return $this->belongsTo(PaymentMethod::class,  'payment_id', 'id');
    }
}
