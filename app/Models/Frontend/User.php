<?php

namespace App\Models\Frontend;

use App\Models\Common\ModifiedFile;
use App\Models\Common\Setting;
use App\Models\Common\Withdraw;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Laratrust\Traits\LaratrustUserTrait;

use App\Models\Common\Batch;
use App\Models\Common\DesignerLevel;

class User extends Authenticatable
{
    use Notifiable, LaratrustUserTrait;

    protected $guarded = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'active', 'verified', 'verify_token',
        'password', 'photo_url', 'phone', 'skype', 'last_login'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'last_login'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'users';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'verified' => 'boolean'
    ];

    public function batches()
    {
        return $this->belongsToMany(Batch::class, 'batches_executors');
    }

    public function balance()
    {
        return $this->hasOne(UserBalance::class);
    }

    public function accruals()
    {
        return $this->hasMany(Accruals::class);
    }

    public function designerLevel()
    {
        return $this->belongsToMany(DesignerLevel::class, 'user_designer_level', 'user_id', 'level_id');
    }

    public function withdraws()
    {
        return $this->hasMany(Withdraw::class);
    }

    public function paymentsMethod()
    {
        return $this->hasMany(UserPaymentMethods::class);
    }

    public function existsPaymentMethod()
    {
        return !empty($this->hasMany(UserPaymentMethods::class)->whereNotNull('number_card')->get()->first()) ? true : false;
    }

    public function canCacheOut()
    {
        $minCacheOut = Setting::where('key', 'min_cash_out')->get();

        if ($minCacheOut->count() > 0) {
            $valueMin = $minCacheOut->first()->value;
            if ((float)$valueMin > (float)$this->balance->balance) {
                return false;
            } else{
                return true;
            }
        } else {
            return false;
        }
    }
}