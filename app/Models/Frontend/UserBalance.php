<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{
    protected $fillable = ['user_id', 'balance'];

    public function getBalanceAttribute()
    {
       return $this->attributes['balance'];
    }

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
