<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class RejectReason extends Model
{
    protected $fillable = ['name'];

    public function decline()
    {
        return $this->belongsToMany(DeclineFile::class, 'decline_file_reasons', 'reason_id', 'ticket_id');
    }
}
