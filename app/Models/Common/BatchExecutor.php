<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;
use App\Models\Frontend\User as FrontEndUser;

class BatchExecutor extends Model
{
    protected $table = 'batches_executors';

    protected $fillable = ['batch_id', 'user_id'];


    public function user(){
        return $this->belongsTo(FrontEndUser::class);
    }
}