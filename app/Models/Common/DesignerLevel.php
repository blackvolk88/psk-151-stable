<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

use App\Models\Frontend\User;

class DesignerLevel extends Model
{
    const LEVEL_BEGINNER = 'beginner';

    protected $fillable = [
        'level', 'coefficient', 'limit'
    ];

    public function user()
    {
        return $this->belongsToMany(User::class, 'user_designer_level', 'level_id', 'user_id');
    }

    /**
     * Scope a query include to only beginner level
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLevelBeginner($query)
    {
        return $query->where(['level' => self::LEVEL_BEGINNER])->first();
    }
}