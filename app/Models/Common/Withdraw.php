<?php

namespace App\Models\Common;

use App\Models\Frontend\User;
use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    const STATUS_NEW = 1;
    const STATUS_COMPLETED = 2;
    const STATUS_DECLINED = 3;

    const STATUS_NAME = [
        1 => 'New',
        2 => 'Completed',
        3 => 'Declined'
    ];

    protected $fillable = [
        'status', 'amount', 'number_card', 'type', 'user_id'
    ];

    public function paymentMethod()
    {
        return $this->hasOne(PaymentMethod::class, 'id', 'type');
    }

    public function getNameStatus()
    {
        return self::STATUS_NAME[$this->status];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
