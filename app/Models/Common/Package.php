<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

use App\Models\Common\FileStatus;

class Package extends Model
{
    const USER_TYPE_BACKEND = 'App\Models\Backend\User';
    const USER_TYPE_FRONTEND = 'App\Models\Frontend\User';

    const STATUS_NEW = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'user_id', 'user_type', 'status_id'];

    /**
     * Define a one-to-one relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne($this->user_type, 'id', 'user_id');
    }

    /**
     * Define a many-to-many relationship.
     *
      * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function files()
    {
        return $this->belongsToMany(File::class, 'package_file');
    }

    public function completedFilesCount()
    {
        return $this->files()->where(['status_id' => FileStatus::STATUS_APPROVED])->count();
    }
}