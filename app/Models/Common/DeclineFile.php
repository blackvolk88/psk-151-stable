<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class DeclineFile extends Model
{
    protected $fillable = ['file_id', 'comment'];

    public function reasons()
    {
        return $this->belongsToMany(RejectReason::class, 'decline_file_reasons', 'ticket_id', 'reason_id');
    }
}
