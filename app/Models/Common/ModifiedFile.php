<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class ModifiedFile extends Model
{
    protected $fillable = ['parent_id', 'batch_id', 'thumb_path', 'path', 'storage_path'];
}
