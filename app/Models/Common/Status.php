<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    const STATUS_NEW = 1;
    const STATUS_PROCESS = 2;
    const STATUS_PROCESSED = 3;
    const STATUS_COMPLETED = 4;

    protected $fillable = ['name'];

    public $timestamps = false;

    public function batches()
    {
        return $this->belongsTo(Batch::class, 'id', 'status_id');
    }
}