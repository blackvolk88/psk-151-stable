<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'name', 'value'
    ];
}
