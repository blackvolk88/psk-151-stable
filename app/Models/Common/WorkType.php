<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class WorkType extends Model
{
    protected $fillable = ['name'];

    public function batches()
    {
        return $this->belongsTo(Batch::class, 'id', 'work_type_id');
    }

    /**
     * Scope a query to only include not system work types.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutSystem($query)
    {
        return $query->where(['system' => false]);
    }

    /**
     * Scope a query to only include system work type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSystem($query)
    {
        return $query->where(['system' => true])->first();
    }
}
