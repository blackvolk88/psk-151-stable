<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class FileStatus extends Model
{
    const STATUS_NEW = 1;
    const STATUS_NOT_PROCESSED = 2;
    const STATUS_DESIGNED = 3;
    const STATUS_DECLINED = 4;
    const STATUS_APPROVED = 5;

    protected $fillable = ['name'];

    public $timestamps = false;


    /**
     * Scope a query to include all formats withour default status "new"
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutDefault($query)
    {
        return $query->where('id', '!=', self::STATUS_NEW);
    }

    /**
     * Scope a query to includes statuses for moderating files
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeModerated($query)
    {
        return $query->where(['id' => self::STATUS_DESIGNED]);
    }
}