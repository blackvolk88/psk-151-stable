<?php

namespace App\Models\Common;

use Laratrust\LaratrustPermission;

class Permission extends LaratrustPermission
{
    protected $fillable = ['name', 'display_name', 'description'];
}