<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Difficulty extends Model
{
    protected $fillable = [
        'name', 'coefficient'
    ];

    public function batches()
    {
        return $this->belongsTo(Batch::class, 'id', 'difficulty_id');
    }

    /**
     * Scope a query to only include not system difficulties.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutSystem($query)
    {
        return $query->where(['system' => false]);
    }

    /**
     * Scope a query to only include system difficulty.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSystem($query)
    {
        return $query->where(['system' => true])->first();
    }
}