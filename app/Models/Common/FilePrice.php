<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class FilePrice extends Model
{
    const TYPE_DESIGNER = 'design';

    /**
     * Scope a query to include price for design work
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDesign($query)
    {
        return $query->where(['key' => self::TYPE_DESIGNER])->first();
    }
}