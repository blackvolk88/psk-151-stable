<?php

namespace App\Models\Common;

use App\Models\Frontend\User;
use App\Models\Frontend\UserPaymentMethods;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    protected $fillable = ['status', 'name'];

    public $timestamps = false;


    public function users()
    {
        return $this->belongsToMany(Withdraw::class);
    }


    public function userPaymentMethod()
    {
        return $this->hasOne(UserPaymentMethods::class, 'payment_id', 'id')->where('user_id', auth()->user()->getAuthIdentifier());
    }

}
