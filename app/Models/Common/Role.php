<?php

namespace App\Models\Common;

use Laratrust\LaratrustRole;

class Role extends LaratrustRole
{
    const ROLE_DESIGNER = 'designer';

    protected $fillable = ['name', 'description'];

    /**
     * Scope a query include to only designer role
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDesignerRole($query)
    {
        return $query->where(['name' => self::ROLE_DESIGNER])->first();
    }

    /**
     * Scope a query include to only roles using on frontend
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFrontendRoles($query)
    {
        return $query->where(['name' => self::ROLE_DESIGNER]);
    }

    /**
     * Scope a query include to roles without roles using on frontend
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutFrontendRoles($query)
    {
        return $query->where('name', '!=', self::ROLE_DESIGNER);
    }
}