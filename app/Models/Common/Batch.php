<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

use App\Models\Frontend\User as FrontEndUser;
use App\Models\Backend\User as BackEndUser;

class Batch extends Model
{
    protected $fillable = [
        'status_id', 'work_type_id', 'difficulty_id', 'author_id',
        'started_at', 'finished_at', 'comment', 'admin_comment'
    ];

    protected $dates = ['created_at', 'updated_at', 'started_at', 'finished_at', 'download_at'];

    public function files()
    {
        return $this->belongsToMany(File::class, 'batches_files');
    }

    public function author()
    {
        return $this->hasOne(BackEndUser::class, 'id', 'author_id');
    }

    public function executor()
    {
        return $this->belongsToMany(FrontEndUser::class, 'batches_executors');
    }

    public function status()
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function type()
    {
        return $this->hasOne(WorkType::class, 'id', 'work_type_id');
    }

    public function difficulty()
    {
        return $this->hasOne(Difficulty::class, 'id', 'difficulty_id');
    }

    /**
     * Scope a query to only available to assign batches.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAvailableToAssign($query)
    {
        return $query->where(['status_id' => Status::STATUS_NEW]);
    }

    /**
     * Scope a query to only not completed batches.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutCompleted($query)
    {
        return $query->where('status_id', '!=', Status::STATUS_COMPLETED);
    }

    /**
     * check batch completed or not
     *
     * @return boolean
     */
    public function isCompleted()
    {
        return $this->status->id == Status::STATUS_COMPLETED ? true : false;
    }
}