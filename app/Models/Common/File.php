<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

use App\Models\Backend\Release;

class File extends Model
{
    protected $fillable = [
        'mime_type', 'size', 'width', 'height', 'make', 'model', 'aperture',
        'exposure_time', 'focal_length', 'iso', 'thumb_path',  'path', 'original_name',
        'storage_name', 'storage_path', 'status_id', 'filmed_at', 'hash'
    ];

    public function batch()
    {
        return $this->belongsToMany(Batch::class, 'batches_files');
    }

    public function releases()
    {
        return $this->belongsToMany(Release::class, 'release_file');
    }

    public function decline()
    {
        return $this->belongsTo(DeclineFile::class, 'id', 'file_id');
    }

    public function status()
    {
        return $this->hasOne(FileStatus::class, 'id', 'status_id');
    }

    public function modified()
    {
        return $this->belongsTo(ModifiedFile::class, 'id', 'parent_id');
    }

    public function modifieds()
    {
        return $this->belongsToMany(ModifiedFile::class, 'id', 'parent_id');
    }

    /**
     * Scope a query to includes not assign to batch files
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotUsed($query)
    {
        return $query->where(['status_id' => FileStatus::STATUS_NEW]);
    }

    /**
     * Scope a query to includes designed files
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDesigned($query)
    {
        return $query->where(['status_id' => FileStatus::STATUS_DESIGNED]);
    }

    /**
     * Scope a query to includes not approved files
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutApproved($query)
    {
        return $query->where('status_id', '!=', FileStatus::STATUS_APPROVED);
    }

    /**
     * Scope a query to includes not approved files
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsDecline($query)
    {
        return $query->where('status_id', '=', FileStatus::STATUS_DECLINED);
    }

    /**
     *  get user who design file
     * @return string name
     */
    public function getWhoWorked()
    {
        return $this->batch()->first()->executor()->first()->name;
    }
}