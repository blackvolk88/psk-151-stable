<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerValidationRules($this->app['validator']);
    }

    protected function registerValidationRules(\Illuminate\Contracts\Validation\Factory $validator)
    {
        $validator->extend('number_bank_card', 'App\Validators\NumberBankCardValidator@validate', 'Card is not valid');
    }
}
