<?php

namespace App\Helpers;


class Helper
{
    public static function formatPrice(float $price = 0.00): string
    {
        return '₴' . number_format($price, 2) . ' UAH';
    }
}