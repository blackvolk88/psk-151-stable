<?php

namespace App\Helpers;

class FileHelper
{
    public static function bytesToHuman($bytes)
    {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }

        return round($bytes, 2) . ' ' . $units[$i];
    }

    public static function mimeToHuman($mime)
    {
        $mimes = [
            'image/jpeg' => 'JPEG',
            'image/png' => 'PNG',
            'image/tiff' => 'TIFF',
            'image/raw' => 'RAW'
        ];

        return isset($mimes[$mime]) ? $mimes[$mime] : $mime;
    }
}