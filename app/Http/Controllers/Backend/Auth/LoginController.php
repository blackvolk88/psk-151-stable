<?php

namespace App\Http\Controllers\Backend\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('backend.auth.login');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();

        return redirect(route('backend.login'));
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'active' => 1
        ], $request->has('remember'));
    }

    protected function guard()
    {
        return Auth::guard('backend');
    }
}
