<?php

namespace App\Http\Controllers\Backend\Users;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Models\Frontend\User;
use App\Models\Common\DesignerLevel;
use App\Models\Common\Role;

class ClientsController extends Controller
{
    public function index()
    {
        $response = [
            'clients' => $this->prepareFilterUsers(),
            'roles' => Role::frontendRoles()->get(),
            'currentSearch' => request()->has('search') ? request('search') : ''
        ];

        return view('backend.pages.users.clients.index', $response);
    }

    private function prepareFilterUsers()
    {
        $this->validate(request(), [
            'role' => 'integer',
            'search' => 'string',
            'page' => 'integer'
        ]);

        $conditions = [];

        if (Input::has('search')) {
            $conditions[] = ['name', 'like', '%' . Input::get('search') . '%'];
        }

        if (empty($conditions)) {
            return User::orderBy('created_at', 'desc')->paginate(config('app.pagination.backend.size'));
        }

        return User::where($conditions)
            ->orderBy('created_at', 'desc')
            ->paginate(config('app.pagination.backend.size'))
            ->appends(Input::except('page'));
    }

    public function view(User $user)
    {
        return view('backend.pages.users.clients.view', compact('user'));
    }

    public function changeStatus(User $user)
    {
        $this->validate(request(), [
            'status' => 'required|integer'
        ]);

        $user->active = request('status');
        $user->save();

        return response('Status changed successfully', 200);
    }

    public function getEditForm(User $user)
    {
        $response = [
            'user' => $user,
            'currentLevel' => $user->designerLevel()->first()->id,
            'designerLevels' => DesignerLevel::all()
        ];

        return view('backend.pages.users.clients.edit', $response);
    }

    public function edit(User $user)
    {
        $this->validate(request(), [
            'name' => 'required|string|min:3',
            'email' => 'unique:users,email,'. $user->id .'|required|email',
            'phone' => 'required|phone:AUTO',
            'skype' => 'required|string|min:3',
            'passport' => 'nullable|mimes:jpeg,jpg,png',
            'level' => 'required|integer'
        ]);

        if (request()->has('passport')) {
            $requestFile = request()->file('passport');
            $storage = Storage::disk('public');
            if ($storage->putFileAs('passport/' . $user->id, $requestFile, $requestFile->getClientOriginalName())) {
                $storage->delete(storage_path() . $user->passport_url);

                $user->passport_url = 'passport/' . $user->id . '/' . $requestFile->getClientOriginalName();
            }
        }

        $level = DesignerLevel::find(request('level'));

        $user->name = request('name');
        $user->email = request('email');
        $user->phone = request('phone');
        $user->skype = request('skype');
        $user->designerLevel()->sync($level);
        $user->save();

        return redirect(route('backend.users.clients.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'User updated successfully'
        ]);
    }
}