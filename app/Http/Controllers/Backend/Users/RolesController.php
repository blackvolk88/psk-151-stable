<?php

namespace App\Http\Controllers\Backend\Users;

use App\Http\Controllers\Controller;

use App\Models\Common\Role;
use App\Models\Common\Permission;
use App\Models\Backend\User;

class RolesController extends Controller
{
    public function index()
    {
        $roles = Role::withoutFrontendRoles()->get();

        return view('backend.pages.users.roles.index', compact('roles'));
    }

    public function getStoreForm()
    {
        $permissions = Permission::all();

        return view('backend.pages.users.roles.create', compact('permissions'));
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required|string|unique:roles',
            'permissions' => 'required',
            'permissions.*' => 'integer'
        ]);

        $role = Role::create([
            'name' => request('name')
        ]);

        $permissions = Permission::whereIn('id', request('permissions'))->get();

        $role->attachPermissions($permissions);

        return redirect(route('backend.roles.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Role created successfully'
        ]);
    }

    public function getEditForm(Role $role)
    {
        $response = [
            'role' => $role,
            'permissions' => Permission::all()
        ];

        return view('backend.pages.users.roles.edit', $response);
    }

    public function edit(Role $role)
    {
        $this->validate(request(), [
            'permissions' => 'required',
            'permissions.*' => 'integer'
        ]);

        $permissions = Permission::whereIn('id', request('permissions'))->get();

        $role->syncPermissions($permissions);

        return redirect(route('backend.roles.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Role uploaded successfully'
        ]);
    }

    public function destroy(Role $role)
    {
        $assignedToUsersCount = User::whereRoleIs($role->name)->count();

        if ($assignedToUsersCount > 0) {
            return response('This role assign to user(s)', 400);
        }

        $role->delete();

        return response('Role deleted successfully', 200);
    }
}