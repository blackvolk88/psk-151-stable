<?php

namespace App\Http\Controllers\Backend\Users;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Models\Backend\User;
use App\Models\Common\Role;
use App\Jobs\SendMemberRegistration;

class MembersController extends Controller
{
    public function index()
    {
        $response = [
            'members' => $this->prepareFilterUsers(),
            'roles' => Role::withoutFrontendRoles()->get(),
            'currentSearch' => request()->has('search') ? request('search') : ''
        ];

        return view('backend.pages.users.members.index', $response);
    }

    private function prepareFilterUsers()
    {
        $this->validate(request(), [
            'role' => 'integer',
            'search' => 'string',
            'page' => 'integer'
        ]);

        $conditions = [];

        if (Input::has('search')) {
            $conditions[] = ['name', 'like', '%' . Input::get('search') . '%'];
        }

        if (empty($conditions)) {
            return User::orderBy('created_at', 'desc')->paginate(config('app.pagination.backend.size'));
        }

        return User::where($conditions)
            ->orderBy('created_at', 'desc')
            ->paginate(config('app.pagination.backend.size'))
            ->appends(Input::except('page'));
    }

    public function view(User $user)
    {
        return view('backend.pages.users.members.view', compact('user'));
    }

    public function changeStatus(User $user)
    {
        $this->validate(request(), [
            'status' => 'required|integer'
        ]);

        $user->active = request('status');
        $user->save();

        return response('Status changed successfully', 200);
    }

    public function getEditForm(User $user)
    {
        $response = [
            'user' => $user,
            'roles' => Role::withoutFrontendRoles()->get()
        ];

        return view('backend.pages.users.members.edit', $response);
    }

    public function edit(User $user)
    {
        $this->validate(request(), [
            'name' => 'required|string|min:3',
            'email' => 'unique:backend_users,email,'. $user->id .'|required|email',
            'role' => 'required|integer'
        ]);

        $user->name = request('name');
        $user->email = request('email');

        $role = Role::find(request('role'));
        $user->syncRoles($role);

        $user->save();

        return redirect(route('backend.users.members.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Member updated successfully'
        ]);
    }

    public function getStoreForm()
    {
        $roles = Role::withoutFrontendRoles()->get();

        return view('backend.pages.users.members.create', compact('roles'));
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required|string|min:3',
            'email' => 'required|email|unique:backend_users',
            'role' => 'required|integer'
        ]);

        $password = str_random(8);

        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt($password)
        ]);

        $role = Role::find(request('role'));
        $user->attachRole($role);

        $sendingData = [
            'name' => request('name'),
            'password' => $password
        ];

        dispatch(new SendMemberRegistration($user, $sendingData));

        return redirect(route('backend.users.members.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Member created successfully'
        ]);
    }
}