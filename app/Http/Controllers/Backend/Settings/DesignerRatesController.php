<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;

use App\Models\Common\DesignerLevel;

class DesignerRatesController extends Controller
{
    public function edit(DesignerLevel $level)
    {
        $this->validate(request(), [
            'coefficient' => 'required'
        ]);

        $level->coefficient = request('coefficient');
        $level->save();

        return response('Designer rate coefficient changed successfully', 200);
    }

    public function editLimit(DesignerLevel $level)
    {
        $this->validate(request(), [
            'limit' => 'required|integer|min:1'
        ]);

        $level->limit = request('limit');
        $level->save();

        return response('Max number batches for designer changed successfully', 200);
    }
}