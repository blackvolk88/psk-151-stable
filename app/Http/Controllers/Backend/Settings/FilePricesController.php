<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;

use App\Models\Common\FilePrice;

class FilePricesController extends Controller
{
    public function edit(FilePrice $price)
    {
        $this->validate(request(), [
            'price' => 'required'
        ]);

        $price->price = request('price');
        $price->save();

        return response('file price changed successfully', 200);
    }
}