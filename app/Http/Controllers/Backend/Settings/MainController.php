<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;

use App\Models\Common\FilePrice;
use App\Models\Common\Difficulty;
use App\Models\Common\DesignerLevel;
use App\Models\Common\Setting;

class MainController extends Controller
{
    public function index()
    {
        $response = [
            'globalSettings' => Setting::all(),
            'prices' => FilePrice::all(),
            'difficulties' => Difficulty::all(),
            'designerLevels' => DesignerLevel::all()
        ];

        return view('backend.pages.settings.index', $response);
    }
}