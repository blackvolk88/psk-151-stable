<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Models\Common\Setting;

use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function edit(Setting $setting){

        $this->validate(request(), [
            'value' => 'required'
        ]);

        $setting->value = request('value');
        $setting->save();

        return response('Global setting changed successfully', 200);

    }
}
