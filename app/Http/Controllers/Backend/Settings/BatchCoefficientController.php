<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;

use App\Models\Common\Difficulty;

class BatchCoefficientController extends Controller
{
    public function edit(Difficulty $difficulty)
    {
        $this->validate(request(), [
            'coefficient' => 'required'
        ]);

        $difficulty->coefficient = request('coefficient');
        $difficulty->save();

        return response('coefficient changed successfully', 200);
    }
}