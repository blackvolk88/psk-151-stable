<?php

namespace App\Http\Controllers\Backend\References;

use App\Http\Controllers\Controller;

use App\Models\Common\RejectReason;

class RejectReasonsController extends Controller
{
    public function getStoreForm()
    {
        return view('backend.pages.references.reject.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required|unique:reject_reasons'
        ]);

        RejectReason::create(['name' => request('name')]);

        return redirect(route('backend.references.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Reject reason added successfully'
        ]);
    }

    public function getEditForm(RejectReason $reason)
    {
        return view('backend.pages.references.reject.edit', compact('reason'));
    }

    public function edit(RejectReason $reason)
    {
        $this->validate(request(), [
            'name' => 'required|unique:reject_reasons'
        ]);

        $reason->name = request('name');
        $reason->save();

        return redirect(route('backend.references.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Reject reason updated successfully'
        ]);
    }

    public function destroy(RejectReason $reason)
    {
        if ($reason->decline()->count() > 0) {
            return response('Current reject reason assign to decline ticket', 400);
        }

        $reason->delete();

        return response('reject reason deleted successfully', 200);
    }
}