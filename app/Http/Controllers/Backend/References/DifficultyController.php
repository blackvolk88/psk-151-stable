<?php

namespace App\Http\Controllers\Backend\References;

use App\Http\Controllers\Controller;

use App\Models\Common\Difficulty;

class DifficultyController extends Controller
{
    public function getStoreForm()
    {
        return view('backend.pages.references.difficulty.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required|unique:difficulties'
        ]);

        Difficulty::create(['name' => request('name')]);

        return redirect(route('backend.references.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Difficulty added successfully'
        ]);
    }

    public function destroy(Difficulty $difficulty)
    {
        if (!is_null($difficulty->batches)) {
            return response('This difficulty assign to batch', 400);
        }

        $difficulty->delete();

        return response('difficulty removed successfully', 200);
    }

    public function getEditForm(Difficulty $difficulty)
    {
        return view('backend.pages.references.difficulty.edit', compact('difficulty'));
    }

    public function edit(Difficulty $difficulty)
    {
        $this->validate(request(), [
            'name' => 'required|unique:difficulties'
        ]);

        $difficulty->name = request('name');
        $difficulty->save();

        return redirect(route('backend.references.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Difficulty updated successfully'
        ]);
    }
}