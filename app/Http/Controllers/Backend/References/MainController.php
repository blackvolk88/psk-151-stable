<?php

namespace App\Http\Controllers\Backend\References;

use App\Http\Controllers\Controller;

use App\Models\Common\Status;
use App\Models\Common\WorkType;
use App\Models\Common\Difficulty;
use App\Models\Common\RejectReason;

class MainController extends Controller
{
    public function index()
    {
        $response = [
            'statuses' => Status::all(),
            'types' => WorkType::withoutSystem()->get(),
            'difficulties' => Difficulty::withoutSystem()->get(),
            'reasons' => RejectReason::all()
        ];

        return view('backend.pages.references.index', $response);
    }
}