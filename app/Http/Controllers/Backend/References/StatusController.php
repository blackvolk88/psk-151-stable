<?php

namespace App\Http\Controllers\Backend\References;

use App\Http\Controllers\Controller;

use App\Models\Common\Status;

class StatusController extends Controller
{
    public function edit(Status $status)
    {
        $this->validate(request(), [
            'name' => 'required|string|min:3'
        ]);

        $status->name = request('name');
        $status->save();

        return response('status changed successfully', 200);
    }
}