<?php

namespace App\Http\Controllers\Backend\References;

use App\Http\Controllers\Controller;

use App\Models\Common\WorkType;

class WorkTypeController extends Controller
{
    public function getStoreForm()
    {
        return view('backend.pages.references.work-type.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required|unique:work_types'
        ]);

        WorkType::create(['name' => request('name')]);

        return redirect(route('backend.references.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Work type added successfully'
        ]);
    }

    public function getEditForm(WorkType $type)
    {
        return view('backend.pages.references.work-type.edit', compact('type'));
    }

    public function edit(WorkType $type)
    {
        $this->validate(request(), [
            'name' => 'required|unique:work_types'
        ]);

        $type->name = request('name');
        $type->save();

        return redirect(route('backend.references.work-type.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Work type updated successfully'
        ]);
    }

    public function destroy(WorkType $type)
    {
        if (!is_null($type->batches)) {
            return response('This work type assign to batch', 400);
        }

        $type->delete();

        return response('Work type removed successfully', 200);
    }
}