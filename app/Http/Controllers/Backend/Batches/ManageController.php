<?php

namespace App\Http\Controllers\Backend\Batches;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;

use App\Models\Common\File;
use App\Models\Common\FileStatus;
use App\Models\Common\Batch;
use App\Models\Common\Status;
use App\Models\Common\WorkType;
use App\Models\Common\Difficulty;

use App\Models\Common\Role;
use App\Models\Frontend\User;

class ManageController extends Controller
{
    private $response;

    public function index()
    {
        return view('backend.pages.batches.moderating.create')->with($this->response()->main());
    }

    public function filter()
    {
        return view('backend.pages.batches.moderating.create')->with($this->response()->filtered());
    }

    private function response()
    {
        $designers = User::with(['roles' => function($query) {
            $query->where('name', Role::ROLE_DESIGNER);
        }])->where(['active' => true])->get();

        $this->response['batches'] = Batch::withoutCompleted()->get();
        $this->response['designers'] = $designers;
        $this->response['work_types'] = WorkType::withoutSystem()->get();
        $this->response['difficulties'] = Difficulty::withoutSystem()->get();

        return $this;
    }

    private function main()
    {
        $this->response['formats'] = File::notUsed()->distinct('mime_type')->pluck('mime_type');
        $this->response['files'] = File::notUsed()->get();
        $this->response['currentFormat'] = '';
        $this->response['filmed'] = '';
        $this->response['uploaded'] = '';

        return $this->response;
    }

    private function filtered()
    {
        $this->validate(request(), [
            'format' => 'string',
            'filmed' => 'date',
            'uploaded' => 'date'
        ]);

        $files = File::notUsed();

        if (Input::has('format')) {
            $files->where('mime_type', Input::get('format'));
        }

        if (Input::has('filmed')) {
            $files->whereDate('filmed_at', Input::get('filmed'));
        }

        if (Input::has('uploaded')) {
            $files->whereDate('created_at', Input::get('uploaded'));
        }

        $this->response['files'] = $files->get();
        $this->response['formats'] = $files->distinct('mime_type')->pluck('mime_type');
        $this->response['currentFormat'] = request()->has('format') ? request('format') : '';
        $this->response['filmed'] = request()->has('filmed') ? request('filmed') : '';
        $this->response['uploaded'] = request()->has('uploaded') ? request('uploaded') : '';

        return $this->response;
    }

    public function view(Batch $batch)
    {
        $response = [
            'currentBatch' => $batch->id,
            'batches' => Batch::withoutCompleted()->get(),
            'batch' => $batch,
            'files' => $batch->files()->get(),
            'formats' => $batch->files()->distinct('mime_type')->pluck('mime_type')
        ];

        return view('backend.pages.batches.moderating.edit')->with($response);
    }

    public function store()
    {
        $this->validate(request(), [
            'images' => 'required|array',
            'type' => 'required|integer',
            'difficulty' => 'required|integer',
            'comment' => 'nullable|string|min:1|max:1500',
            'executor' => 'nullable|integer'
        ]);

        return DB::transaction(function() {
            $requestCollection = collect(request('images'));

            $files = File::notUsed()
                ->whereIn('id', $requestCollection)
                ->lockForUpdate();

            $usedFiles = $requestCollection->diff($files->pluck('id'));

            if ($usedFiles->count() > 0) {
                return response($usedFiles, 400);
            }

            $batch = Batch::create([
                'status_id' => Status::STATUS_NEW,
                'work_type_id' => request('type'),
                'difficulty_id' => request('difficulty'),
                'author_id' => auth()->user()->id,
                'comment' => request()->has('comment') ? request('comment') : null
            ]);

            $batch->files()->attach($files->get());

            $files->update(['status_id' => FileStatus::STATUS_NOT_PROCESSED]);

            if (request()->has('executor')) {
                $executor = User::findOrFail(request('executor'));

                $batch->executor()->attach($executor);

                $batch->status_id = Status::STATUS_PROCESSED;
                $batch->started_at = Carbon::now();
                $batch->save();
            }

            return response('Batch created successfully', 200);
        });
    }

    public function storeRandom()
    {
        $this->validate(request(), [
            'count' => 'required|integer|min:1',
            'comment' => 'nullable|string|min:1|max:191',
            'executor' => 'nullable|integer'
        ]);

        return DB::transaction(function() {
            $files = File::notUsed()
                ->inRandomOrder()
                ->limit(request('count'))
                ->lockForUpdate();

            if (request(('count')) > $files->count()) {
                $message = 'Current files count not available. In system available ' . $files->count() . ' files';
                return response($message, 400);
            }

            $batch = Batch::create([
                'status_id' => Status::STATUS_NEW,
                'work_type_id' => WorkType::system()->id,
                'difficulty_id' => Difficulty::system()->id,
                'author_id' => auth()->user()->id,
                'comment' => request()->has('comment') ? request('comment') : null
            ]);

            $batch->files()->attach($files->get());

            $batch->files()->update(['status_id' => FileStatus::STATUS_NOT_PROCESSED]);

            if (request()->has('executor')) {
                $executor = User::findOrFail(request('executor'));

                $batch->executor()->attach($executor);

                $batch->status_id = Status::STATUS_PROCESSED;
                $batch->started_at = Carbon::now();
                $batch->save();
            }

            return response('Batch created successfully', 200);
        });
    }

    public function destroy(Batch $batch)
    {
        return DB::transaction(function() use ($batch) {
            if ($batch->status->id !== Status::STATUS_NEW) {
                return response('This batch cannot be ungrouped', 400);
            }

            $batch->files()->update(['status_id' => FileStatus::STATUS_NEW]);

            $batch->delete();

            return response('Batch ungrouped successfully', 200);
        });
    }

    public function ungroupFiles()
    {
        $this->validate(request(), [
            'target' => 'required|integer',
            'images' => 'required|array'
        ]);

        $batch = Batch::findOrFail(request('target'));

        if ($batch->status->id !== Status::STATUS_NEW) {
            return response('File(s) from this batch cannot be ungrouped', 400);
        }

        $requestCollection = collect(request('images'));

        if ($batch->files->count() == $requestCollection->count()) {
            return $this->destroy(Batch::find(request('target')));
        }

        return DB::transaction(function() use ($batch, $requestCollection) {
            $batch->files()->detach($requestCollection);

            File::whereIn('id', $requestCollection)->update(['status_id' => FileStatus::STATUS_NEW]);

            return response('Batch file(s) ungrouped successfully', 200);
        });
    }
}