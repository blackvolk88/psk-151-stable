<?php

namespace App\Http\Controllers\Backend\Batches;

use App\Http\Controllers\Controller;
use App\Models\Common\BatchExecutor;
use Illuminate\Support\Facades\Input;
use DB;
use Carbon\Carbon;
use ZipStream\ZipStream;

use App\Models\Common\Batch;
use App\Models\Common\Difficulty;
use App\Models\Common\WorkType;
use App\Models\Common\Status;
use App\Models\Common\FileStatus;

class ListController extends Controller
{
    private $response;

    public function index()
    {
        return view('backend.pages.batches.list.index')->with($this->response()->main());
    }

    public function filter()
    {
        return view('backend.pages.batches.list.index')->with($this->response()->filtered());
    }

    private function response()
    {
        $this->response['difficulties'] = Difficulty::all();
        $this->response['statuses'] = Status::all();
        $this->response['executors'] = BatchExecutor::groupBy('user_id')->get();

        $this->response['work_types'] = WorkType::all();
        $this->response['statusesInfo'] = [
            'new' => Batch::where(['status_id' => Status::STATUS_NEW])->count(),
            'process' => Batch::where(['status_id' => Status::STATUS_PROCESS])->count(),
            'processed' => Batch::where(['status_id' => Status::STATUS_PROCESSED])->count(),
            'completed' => Batch::where(['status_id' => Status::STATUS_COMPLETED])->count()
        ];

        return $this;
    }

    private function main()
    {
        $this->response['batches'] = Batch::latest()->paginate(config('app.pagination.backend.size'));
        $this->response['currentDifficulty'] = '';
        $this->response['currentType'] = '';
        $this->response['currentStatus'] = '';
        $this->response['currentExecutors'] = '';

        return $this->response;
    }

    private function filtered()
    {
        $this->validate(request(), [
            'difficulty' => 'integer',
            'type' => 'integer',
            'status' => 'integer',
            'executors' => 'integer',
            'page' => 'integer'
        ]);

        $conditions = [];
        $availableFilters = [
            'difficulty' => 'difficulty_id',
            'type' => 'work_type_id',
            'status' => 'status_id',
            'executors' => 'executor.user_id'
        ];

        foreach (request()->all() as $filter => $param) {
            if (isset($availableFilters[$filter])) {
                $conditions[] = [$availableFilters[$filter], $param];
            }
        }

        if (empty($conditions)) {
            $this->response['batches'] = Batch::latest()->paginate(config('app.pagination.backend.size'))->appends(Input::except('page'));
        } else {
            $batches = Batch::latest();


            foreach ($conditions as $condition) {
                $colums = explode('.', current($condition));

                if (sizeof($colums) > 1) {
                    $name = array_shift($colums);
                    $batches->whereHas($name, function ($query) use ($colums, $condition) {
                        $query->where(array_pop($colums), array_pop($condition));
                    });
                } else {
                    $batches->where([$condition]);
                }
            }

            $this->response['batches'] = $batches->paginate(config('app.pagination.backend.size'))
                ->appends(Input::except('page'));
        }

        $this->response['currentDifficulty'] = request()->has('difficulty') ? request('difficulty') : '';
        $this->response['currentType'] = request()->has('type') ? request('type') : '';
        $this->response['currentStatus'] = request()->has('status') ? request('status') : '';
        $this->response['currentExecutors'] = request()->has('executors') ? request('executors') : '';

        return $this->response;
    }

    public function destroy(Batch $batch)
    {
        return DB::transaction(function () use ($batch) {
            if ($batch->status->id !== Status::STATUS_NEW) {
                return response('This batch cannot be ungrouped', 400);
            }

            $batch->files()->update(['status_id' => FileStatus::STATUS_NEW]);

            $batch->delete();

            return response('Batch ungrouped successfully', 200);
        });
    }

    public function download(Batch $batch)
    {
        $batch->download_at = Carbon::now();
        $batch->save();

        return response()->stream(function () use ($batch) {
            $zip = new ZipStream(uniqid() . ".zip", [
                'content_type' => 'application/octet-stream'
            ]);

            $batch->files()->each(function ($file) use ($zip) {
                $zip->addFile($file->storage_name, file_get_contents($file->modified->path), [], "store");
            });

            $releaseFilesCollection = collect();

            $batch->files()->each(function ($file) use ($releaseFilesCollection) {
                if ($file->releases()->count()) {
                    $file->releases()->get()->each(function ($releaseFile) use ($releaseFilesCollection) {
                        if (!$releaseFilesCollection->contains($releaseFile)) {
                            $releaseFilesCollection->push($releaseFile);
                        }
                    });
                }
            });

            $releaseFilesCollection->each(function ($item) use ($zip) {
                $zip->addFile($item->storage_name, file_get_contents($item->file_path), [], "store");
            });

            $zip->finish();
        });
    }

    public function preview(Batch $batch)
    {
        $response = [
            'batch' => $batch,
            'files' => $batch->files()->get()
        ];

        return view('backend.pages.batches.list.preview', $response);
    }

    public function updateComment(Batch $batch)
    {
        $this->validate(request(), [
            'comment' => 'nullable|string'
        ]);

        $batch->admin_comment = request('comment');
        $batch->save();

        return redirect(route('backend.batches.list.preview', $batch))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Comment for administrators changed successfully'
        ]);
    }
}