<?php

namespace App\Http\Controllers\Backend\Photos;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use DB;

use App\Models\Backend\Release;
use App\Models\Common\Package;

class ReleaseController extends Controller
{
    public function index()
    {
        $response = [
            'releases' => Release::latest()->get()
        ];

        return view('backend.pages.photos.release.index', $response);
    }

    public function getStoreForm()
    {
        return view('backend.pages.photos.release.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required|string|unique:releases',
            'file' => 'required|mimes:jpeg,jpg,png,tiff,raw'
        ]);

        $requestFile = request()->file('file');

        $storage = Storage::disk('s3');

        $storageName = $this->getUniqueStorageName($requestFile->getClientOriginalName());

        $path = 'releases/' . $storageName;

        if ($storage->put($path, file_get_contents($requestFile), 'public')) {
            Release::create([
                'name' => request('name'),
                'user_id' => auth()->user()->id,
                'storage_name' => $storageName,
                'file_path' => $storage->url($path)
            ]);

            return redirect(route('backend.releases.list'))->with('modal-message', [
                'type' => 'bg-green',
                'message' => 'Release created successfully'
            ]);
        }

        return redirect(route('backend.releases.list'))->with('modal-message', [
            'type' => 'bg-red',
            'message' => 'Error from create release'
        ]);
    }

    private function getUniqueStorageName($original)
    {
        $name = 'release_' . auth()->user()->id . '_' . uniqid()  . '_' . $original;

        $count = Release::where('storage_name', $original)->count();

        if ($count == 0) {
            return $name;
        }

        $this->getUniqueStorageName($original);
    }

    public function download(Release $release)
    {
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=" . basename($release->file_path));
        header("Content-Type: image/*");

        return readfile($release->file_path);
    }

    public function view(Release $release)
    {
        $response = [
            'files' => $release->files()->get()
        ];

        return view('backend.pages.photos.release.view', $response);
    }

    public function prepareAssignForm(Release $release)
    {
        $response = [
            'release' => $release,
            'packages' => Package::all(),
            'currentPackage' => '',
            'files' => []
        ];

        return view('backend.pages.photos.release.prepare-assign', $response);
    }

    public function prepareAssignPackageFiles(Release $release, Package $package)
    {
        $response = [
            'release' => $release,
            'packages' => Package::all(),
            'currentPackage' => $package->id,
            'files' => $package->files()->get()
        ];

        return view('backend.pages.photos.release.prepare-assign', $response);
    }

    public function assign(Release $release)
    {
        $this->validate(request(), [
            'files' => 'required|array',
            'files.*' => 'integer'
        ]);

        return DB::transaction(function () use ($release) {

            $release->files()->attach(request('files'));

            return response('File(s) assigned successfully', 200);
        });
    }

    public function destroy(Release $release)
    {
        if ($release->files()->count()) {
            return response('Release assign to file(s)', 400);
        }

        return DB::transaction(function() use ($release) {
            $s3 = Storage::disk('s3');

            $s3->delete($release->file_path);

            $release->delete();

            return response('Release deleted successfully', 200);
        });
    }
}