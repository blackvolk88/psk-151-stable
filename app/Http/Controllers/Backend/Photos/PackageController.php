<?php

namespace App\Http\Controllers\Backend\Photos;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File as FileFacade;
use Intervention\Image\ImageManager;
use DB;
use ZipStream\ZipStream;

use App\Models\Common\Package;
use App\Models\Common\File;
use App\Models\Common\FileStatus;

class PackageController extends Controller
{
    public function index()
    {
        $response = [
            'packages' => $this->prepareFilterPackages(),
            'currentSearch' => request()->has('package') ? request('package') : ''
        ];

        return view('backend.pages.photos.package.index', $response);
    }

    public function getStoreForm()
    {
        return view('backend.pages.photos.package.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required|string|unique:packages'
        ]);

        Package::create([
            'name' => request('name'),
            'user_id' => auth()->user()->id,
            'user_type' => Package::USER_TYPE_BACKEND,
            'status_id' => Package::STATUS_NEW
        ]);

        return redirect(route('backend.photos.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Package created successfully'
        ]);
    }

    public function view(Package $package)
    {
        $response = [
            'package' => $package,
            'files' => $this->prepareFilterFiles($package),
            'formats' => $package->files()->distinct('mime_type')->pluck('mime_type'),
            'currentFormat' => request()->has('format') ? request('format') : '',
            'currentSearch' => request()->has('search') ? request('search') : ''
        ];

        return view('backend.pages.photos.package.view', $response);
    }

    public function destroy(Package $package)
    {
        return DB::transaction(function() use ($package) {
            $s3 = Storage::disk('s3');
            $storage = Storage::disk('public');

            $package->files()->each(function($file) use ($s3, $storage) {
                $s3->delete($file->storage_path);
                $storage->delete($file->thumb_path);
            });

            $packageFiles = $package->files()->pluck('id');
            File::whereIn('id', $packageFiles)->delete();

            $package->delete();

            return response('Package deleted successfully', 200);

        });
    }

    public function destroyFile(Package $package, File $file)
    {
        return DB::transaction(function() use ($package, $file) {
            $s3 = Storage::disk('s3');
            $storage = Storage::disk('public');

            $s3->delete($file->storage_path);
            $storage->delete($file->thumb_path);

            $package->files()->detach($file);
            $file->delete();

            return response('File deleted successfully', 200);
        });
    }

    public function upload(Package $package)
    {
        $validator = validator()->make(request()->all(), [
            'file' => 'required|mimes:jpeg,jpg,png,tiff,raw'
        ]);

        if ($validator->fails()) {
            return response('File format not supported', 400);
        }

        $requestFile = request()->file('file');

        $fileHash = md5_file($requestFile->getRealPath());

        $alreadyExist = File::where(['hash' => $fileHash])->count();

        if ($alreadyExist) {
            return response('File already exist', 400);
        }

        $storage = Storage::disk('s3');
        $publicStorage = Storage::disk('public');

        $storageName = $this->getUniqueStorageName($requestFile->getClientOriginalName());
        $path = 'original/' . date('mY') . '/' . auth()->user()->id . '/' . $storageName;

        if ($storage->put($path, file_get_contents($requestFile), 'public')) {
            $manager = new ImageManager(['driver' => 'imagick']);
            $originalImg = $manager->make($requestFile);
            $width = $originalImg->width();
            $height = $originalImg->height();
            $exif = $originalImg->exif();

            $originalImg->resize(350, 350, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $thumbDirectory = 'thumbs/' . date('mY') . '/' . auth()->user()->id;

            if (!FileFacade::isDirectory($publicStorage->path($thumbDirectory))) {
                FileFacade::makeDirectory($publicStorage->path($thumbDirectory), 493, true);
            }

            $originalImg->save($publicStorage->path($thumbDirectory . '/' . $originalImg->filename . '.png'));

            $iso = null;

            if (isset($exif['ISOSpeedRatings'])) {
                if (is_array($exif['ISOSpeedRatings'])) {
                    $iso = $exif['ISOSpeedRatings'][0];
                }
                else {
                    $iso = $exif['ISOSpeedRatings'];
                }
            }

            $file = File::create([
                'mime_type' => $requestFile->getClientMimeType(),
                'size' => $requestFile->getClientSize(),
                'width' => $width,
                'height' => $height,
                'make' => isset($exif['Make']) ? $exif['Make'] : null,
                'model' => isset($exif['Model']) ? $exif['Model'] : null,
                'aperture' => $this->extractAperture($exif),
                'exposure_time' => isset($exif['ExposureTime']) ? $exif['ExposureTime'] : null,
                'focal_length' => $this->extractFocalLength($exif),
                'iso' => $iso,
                'thumb_path' => $thumbDirectory . '/' . $originalImg->filename . '.png',
                'path' => $storage->url($path),
                'original_name' => $requestFile->getClientOriginalName(),
                'storage_name' => $storageName,
                'storage_path' => $path,
                'status_id' => FileStatus::STATUS_NEW,
                'filmed_at' => isset($exif['DateTimeOriginal']) ? $exif['DateTimeOriginal'] : null,
                'hash' => $fileHash
            ]);

            $package->files()->attach($file);

            return response('File uploaded successfully', 200);
        }

        return response('File not uploaded', 400);
    }

    public function download(File $file)
    {
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=" . basename($file->path));
        header("Content-Type: " . $file->mime_type);

        return readfile($file->path);
    }

    public function previewFile(File $file)
    {
        return view('backend.pages.photos.package.preview', compact('file'));
    }

    public function getPrepareDownloadForm(Package $package)
    {
        return view('backend.pages.photos.package.prepare-download', compact('package'));
    }

    public function downloadOriginalFiles(Package $package)
    {
        return response()->stream(function () use ($package) {
            $zip = new ZipStream(uniqid() . ".zip", [
                'content_type' => 'application/octet-stream'
            ]);

            $package->files()->each(function($file) use ($zip) {
                $zip->addFile($file->storage_name, file_get_contents($file->path), [], "store");
            });

            $zip->finish();
        });
    }

    public function downloadCompletedFiles(Package $package)
    {
        $completedFiles = $package->files()->where(['status_id' => FileStatus::STATUS_APPROVED]);

        if ($completedFiles->count() == 0) {
            return redirect(route('backend.photos.list'))->with('modal-message', [
                'type' => 'bg-red',
                'message' => 'Completed files for current batch not found'
            ]);
        }

        return response()->stream(function () use ($completedFiles) {
            $zip = new ZipStream(uniqid() . ".zip", [
                'content_type' => 'application/octet-stream'
            ]);

            $completedFiles->get()->each(function($file) use ($zip) {
                $zip->addFile($file->storage_name, file_get_contents($file->modified->path), [], "store");
            });

            $zip->finish();
        });
    }

    private function prepareFilterPackages()
    {
        $this->validate(request(), [
            'package' => 'string',
            'page' => 'integer'
        ]);

        $conditions = [];

        if (Input::has('package')) {
            $conditions[] = ['name', 'like', '%' . Input::get('package') . '%'];
        }

        if (empty($conditions)) {
            return Package::latest()->paginate(config('app.pagination.backend.size'));
        }

        return Package::where($conditions)
            ->latest()
            ->paginate(config('app.pagination.backend.size'))
            ->appends(Input::except('page'));
    }

    private function prepareFilterFiles($package)
    {
        $this->validate(request(), [
            'format' => 'string',
            'search' => 'string',
            'page' => 'integer'
        ]);

        $conditions = [];

        if (Input::has('format')) {
            $conditions[] = ['mime_type', Input::get('format')];
        }
        if (Input::has('search')) {
            $conditions[] = ['original_name', 'like', '%' . Input::get('search') . '%'];
        }

        if (empty($conditions)) {
            return $package->files()->latest()->paginate(config('app.pagination.backend.size'));
        }

        return $package->files()->where($conditions)
            ->latest()
            ->paginate(config('app.pagination.backend.size'))
            ->appends(Input::except('page'));
    }

    private function getUniqueStorageName($original)
    {
        $name = auth()->user()->id . '_' . uniqid()  . '_' . $original;

        $count = File::where('storage_name', $name)->count();

        if ($count == 0) {
            return $name;
        }

        $this->getUniqueStorageName($original);
    }

    private function extractAperture($exif)
    {
        if (!isset($exif['FNumber'])) {
            return null;
        }

        $aperture = $exif['FNumber'];
        $aperture = explode('/', $aperture);

        if (isset($aperture[1])) {
            return $aperture[0] / $aperture[1];
        }

        return $aperture[0];
    }

    private function extractFocalLength($exif)
    {
        if (!isset($exif['FocalLength'])) {
            return null;
        }

        $focal = $exif['FocalLength'];
        $focal = explode('/', $focal);

        if (isset($focal[1])) {
            return $focal[0] / $focal[1];
        }

        return $focal[0];
    }
}