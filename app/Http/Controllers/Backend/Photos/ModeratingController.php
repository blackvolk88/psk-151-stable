<?php

namespace App\Http\Controllers\Backend\Photos;

use App\Http\Controllers\Controller;

use DB;
use Illuminate\Database\QueryException;
use ZipStream\ZipStream;

use App\Models\Common\File;
use App\Models\Common\FileStatus;
use App\Models\Common\RejectReason;
use App\Models\Common\DeclineFile;
use App\Models\Common\FilePrice;
use App\Models\Common\Status;
use App\Models\Frontend\Accruals;

class ModeratingController extends Controller
{
    public function index()
    {
        $response = [
            'files' => File::designed()->orderBy('id', 'desc')->get(),
            'file' => File::designed()->orderBy('id', 'desc')->first(),
            'fileStatuses' => FileStatus::moderated()->get(),
            'currentFileStatus' => FileStatus::STATUS_DESIGNED,
            'typeSort' => 'id',
            'currentSort' => 'desc',
        ];

        return view('backend.pages.photos.moderating.index', $response);
    }

    public function sort($type, $sort)
    {
        try {
            $response = [
                'files' => File::designed()->orderBy($type, $sort)->get(),
                'file' => File::designed()->orderBy($type, $sort)->first(),
                'fileStatuses' => FileStatus::moderated()->get(),
                'currentFileStatus' => FileStatus::STATUS_DESIGNED,
                'typeSort' => $type,
                'currentSort' => $sort,
            ];

            return view('backend.pages.photos.moderating.index', $response);
        } catch (QueryException $exception) {
            return redirect(route('backend.photos.moderating'));
        }
    }

    public function view(File $file, $type, $sort)
    {
        if ($file->status_id !== FileStatus::STATUS_DESIGNED) {
            return redirect(route('backend.photos.moderating'))->with('modal-message', [
                'type' => 'bg-red',
                'message' => 'Сurrent file changed by another moderator'
            ]);
        }

        $response = [
            'files' => File::designed()->orderBy('id', $sort)->get(),
            'file' => $file,
            'fileStatuses' => FileStatus::moderated()->get(),
            'currentFileStatus' => FileStatus::STATUS_DESIGNED,
            'typeSort' => $type,
            'currentSort' => $sort
        ];

        return view('backend.pages.photos.moderating.index', $response);
    }

    public function download(File $file)
    {
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=" . basename($file->modified->path));
        header("Content-Type: " . $file->mime_type);

        return readfile($file->modified->path);
    }

    public function getDeclineForm(File $file)
    {
        $reasons = RejectReason::all();

        if ($reasons->count() == 0) {
            return response('Please contact from admin and specify reject reasons', 400);
        }

        if ($file->status_id !== FileStatus::STATUS_DESIGNED) {
            return response('Сurrent file changed by another moderator', 403);
        }

        $response = [
            'file' => $file,
            'reasons' => $reasons
        ];

        return view('backend.pages.photos.moderating.decline-form', $response);
    }

    public function decline(File $file)
    {
        $this->validate(request(), [
            'reasons' => 'required|array',
            'reasons.*' => 'integer',
            'comment' => 'string|min:1|nullable'
        ]);

        return DB::transaction(function () use ($file) {
            if ($file->status_id !== FileStatus::STATUS_DESIGNED) {
                return redirect(route('backend.photos.moderating'))->with('modal-message', [
                    'type' => 'bg-red',
                    'message' => 'Сurrent file changed by another moderator'
                ]);
            }

            $file->status_id = FileStatus::STATUS_DECLINED;
            $file->save();

            $declineTicket = DeclineFile::create([
                'file_id' => $file->id,
                'comment' => request()->has('comment') ? request('comment') : ''
            ]);

            $reasons = RejectReason::whereIn('id', request('reasons'))->get();
            $declineTicket->reasons()->attach($reasons);

            return redirect(route('backend.photos.moderating'))->with('modal-message', [
                'type' => 'bg-green',
                'message' => 'File declined successfully'
            ]);
        });
    }

    public function multipleMode()
    {
        $response = [
            'files' => File::designed()->orderBy('id', 'desc')->get(),
            'fileStatuses' => FileStatus::moderated()->get(),
            'currentFileStatus' => FileStatus::STATUS_DESIGNED,
            'typeSort' => 'id',
            'currentSort' => 'desc',
        ];

        return view('backend.pages.photos.moderating.multiple', $response);
    }

    public function sortMode($type, $sort)
    {
        try {
            $response = [
                'files' => File::designed()->orderBy($type, $sort)->get(),
                'fileStatuses' => FileStatus::moderated()->get(),
                'currentFileStatus' => FileStatus::STATUS_DESIGNED,
                'typeSort' => $type,
                'currentSort' => $sort,
            ];

            return view('backend.pages.photos.moderating.multiple', $response);
        } catch (QueryException $exception){
            return redirect(route('backend.photos.moderating.multiple'));
        }


    }

    public function multipleView()
    {
        $this->validate(request(), [
            'files' => 'required|array',
            'files.*' => 'integer'
        ]);

        $requestCollection = collect(request('files'));

        $files = File::designed()->whereIn('id', $requestCollection);

        $notAvailableFiles = $requestCollection->diff($files->pluck('id'));

        if ($notAvailableFiles->count() > 0) {
            return response($notAvailableFiles, 400);
        }

        $response = [
            'files' => $files->get(),
            'formats' => $files->distinct('mime_type')->pluck('mime_type')
        ];

        return [
            'info' => view('backend.pages.photos.moderating.multiple-info', $response)->render(),
            'content' => view('backend.pages.photos.moderating.multiple-view', ['files' => $files->get()])->render()
        ];
    }

    public function downloadSelected()
    {
        $this->validate(request(), [
            'files' => 'required|array',
            'files.*' => 'integer'
        ]);

        session(['selected-files' => request('files')]);

        return response(route('backend.photos.moderating.prepare.download'), 200);
    }

    public function prepareDownloadFiles()
    {
        return response()->stream(function () {
            $zip = new ZipStream(uniqid() . ".zip", [
                'content_type' => 'application/octet-stream'
            ]);

            File::whereIn('id', request()->session()->pull('selected-files'))->get()->each(function ($file) use ($zip) {
                $zip->addFile($file->storage_name, file_get_contents($file->modified->path), [], "store");
            });

            $zip->finish();
        });
    }

    public function getMultipleDeclineForm()
    {
        $this->validate(request(), [
            'files' => 'required|array',
            'files.*' => 'integer'
        ]);

        $reasons = RejectReason::all();

        if ($reasons->count() == 0) {
            return response('Please contact from admin and specify reject reasons', 400);
        }

        $requestCollection = collect(request('files'));

        $files = File::designed()->whereIn('id', $requestCollection);

        $notAvailableFiles = $requestCollection->diff($files->pluck('id'));

        if ($notAvailableFiles->count() > 0) {
            return response($notAvailableFiles, 400);
        }

        return view('backend.pages.photos.moderating.multiple-decline-form', compact('reasons'));
    }

    public function multipleDecline()
    {
        $this->validate(request(), [
            'files' => 'required|array',
            'files.*' => 'integer',
            'reasons' => 'required|array',
            'reasons.*' => 'integer',
            'comment' => 'string|min:1|nullable'
        ]);

        return DB::transaction(function () {
            $files = File::designed()->whereIn('id', request('files'))->sharedLock()->get();
            if ($files->count() == 0) {
                return response('Files for moderating not found', 400);
            }

            $files->each(function ($file) {
                $file->status_id = FileStatus::STATUS_DECLINED;
                $file->save();

                $declineTicket = DeclineFile::create([
                    'file_id' => $file->id,
                    'comment' => request()->has('comment') ? request('comment') : ''
                ]);

                $reasons = RejectReason::whereIn('id', request('reasons'))->get();
                $declineTicket->reasons()->attach($reasons);
            });

            return response('Files declined successfully', 200);
        });
    }

    public function approve()
    {
        $this->validate(request(), [
            'files' => 'required|array',
            'files.*' => 'integer'
        ]);

        $requestCollection = collect(request('files'));

        $files = File::designed()->whereIn('id', $requestCollection);

        $notAvailableFiles = $requestCollection->diff($files->pluck('id'));

        if ($notAvailableFiles->count() > 0) {
            return response($notAvailableFiles, 400);
        }

        return DB::transaction(function () {
            $files = File::designed()->whereIn('id', request('files'))->sharedLock()->get();
            if ($files->count() == 0) {
                return response('Files for moderating not found', 400);
            }

            $files->each(function ($file) {
                $file->status_id = FileStatus::STATUS_APPROVED;
                $file->save();

                $fileBatch = $file->batch->first();
                $approvedFilesCount = File::whereIn('id', $fileBatch->files()->pluck('id'))
                    ->where(['status_id' => FileStatus::STATUS_APPROVED])
                    ->count();

                if ($approvedFilesCount == $fileBatch->files()->count()) {
                    $fileBatch->status_id = Status::STATUS_COMPLETED;
                    $fileBatch->finished_at = date('Y-m-d H:i:s');
                    $fileBatch->save();
                }

                $batchExecutor = $file->batch->first()->executor->first();
                $currentBalance = $batchExecutor->balance;
                $designerRate = $file->batch->first()->executor->first()->designerLevel()->first();
                $batchCoefficient = $file->batch->first()->difficulty;
                $designWorkPrice = FilePrice::design();
                $workPrice = $batchCoefficient->coefficient * $designerRate->coefficient * $designWorkPrice->price;
                $currentBalance->balance = (float)$workPrice + (float)$currentBalance->balance;
                $currentBalance->save();

                Accruals::create([
                    'user_id' => $batchExecutor->id,
                    'file_id' => $file->id,
                    'accrual' => $workPrice,
                    'coefficient' => $batchCoefficient->coefficient,
                    'rate_coefficient' => $designerRate->coefficient,
                    'file_price' => $designWorkPrice->price
                ]);
            });

            return response('File(s) approved successfully', 200);
        });
    }
}