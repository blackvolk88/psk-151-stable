<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\Common\File;
use App\Models\Common\FileStatus;

use App\Models\Frontend\User;
use App\Models\Common\Role;

use App\Models\Frontend\Accruals;

use App\Models\Common\Batch;
use App\Models\Common\Status;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalFiles = File::all()->count();

        $newFiles = File::where(['status_id' => FileStatus::STATUS_NEW])
            ->count();

        $designedFiles = File::where(['status_id' => FileStatus::STATUS_DESIGNED])
            ->count();

        $approvedFiles = File::where(['status_id' => FileStatus::STATUS_APPROVED])
            ->count();

        $designerUsersCount = User::whereRoleIs(Role::ROLE_DESIGNER)
            ->count();

        $sumAccruals = Accruals::all()->pluck('accrual')->sum();

        $totalBatches = Batch::all()->count();

        $processedBatches = Batch::where(['status_id' => Status::STATUS_PROCESSED])
            ->count();

        $completedBatches = Batch::where(['status_id' => Status::STATUS_COMPLETED])
            ->count();

        $response = [
            'files' => [
                'total' => $totalFiles,
                'new' => $newFiles,
                'designed' => $designedFiles,
                'approved' => $approvedFiles
            ],
            'users' => [
                'designers' => $designerUsersCount
            ],
            'batches' => [
                'total' => $totalBatches,
                'processed' => $processedBatches,
                'completed' => $completedBatches
            ],
            'payouts' => [
                'accruals' => $sumAccruals
            ]
        ];

        return view('backend.pages.dashboard', $response);
    }
}
