<?php

namespace App\Http\Controllers\Backend\Finance;

use App\Http\Controllers\Controller;
use App\Models\Common\PaymentMethod;

class PaymentMethodController extends Controller
{

    public function index()
    {

        $response = [
            'paymentMethods' => PaymentMethod::all(),
            'activeStatus' => PaymentMethod::STATUS_ACTIVE,
            'inActiveStatus' => PaymentMethod::STATUS_INACTIVE,
        ];

        return view('backend.pages.finances.payment-method.show', $response);
    }

    public function store(){
        $this->validate(request(), [
            'name' => 'required|string|unique:payment_methods'
        ]);

        PaymentMethod::create([
            'name' => request('name'),
            'status' => PaymentMethod::STATUS_ACTIVE
        ]);

        return redirect(route('backend.payment.method.list'))->with('modal-message', [
            'type' => 'bg-green',
            'message' => 'Payment method created successfully'
        ]);
    }

    public function editName(PaymentMethod $payment){
        $this->validate(request(), [
            'name' => 'required|string|unique:payment_methods'
        ]);

        $payment->name = request('name');
        $payment->save();

        return response('Changed successfully', 200);
    }

    public function edit(PaymentMethod $payment)
    {
        $this->validate(request(), [
            'status' => 'required|int'
        ]);

        $payment->status = request('status');
        $payment->save();

        return response('Changed successfully', 200);
    }

    public function getStoreForm()
    {
        return view('backend.pages.finances.payment-method.create');
    }

}
