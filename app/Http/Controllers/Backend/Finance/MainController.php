<?php

namespace App\Http\Controllers\Backend\Finance;

use App\Models\Common\Withdraw;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function index()
    {
        $response = [
            'withdraws' => Withdraw::all(),
            'status_new' => Withdraw::STATUS_NEW,
        ];

        return view('backend.pages.finances.index', $response);
    }

    public function edit(Withdraw $withdraw)
    {

        $this->validate(request(), [
            'action' => 'required|string'
        ]);

        if($withdraw->status == Withdraw::STATUS_NEW) {
            switch (request('action')) {
                case 'approved':
                    $withdraw->status = Withdraw::STATUS_COMPLETED;
                    break;
                case 'decline':
                    $withdraw->status = Withdraw::STATUS_DECLINED;

                    $balanceUser = $withdraw->user->balance;
                    $balanceUser->balance = ((float)$balanceUser->balance + (float)$withdraw->amount);
                    $balanceUser->save();
                    break;
            }

            $withdraw->save();
        }

        session()->flash('modal-message', ['type' => 'bg-green', 'message' => 'Changed successfully']);
        return response('Changed successfully', 200);

    }

}
