<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Common\PaymentMethod;
use App\Models\Common\Setting;
use App\Models\Common\Withdraw;
use App\Models\Frontend\UserPaymentMethods;

class FinancialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {
        $accruals = auth()->user()->accruals()->orderBy('created_at', 'desc')->get();

        $payment_method = UserPaymentMethods::whereNotNull('number_card')->get()->mapWithKeys(function ($item) {
            if($item->paymentMethod->status == PaymentMethod::STATUS_ACTIVE){
                return [$item->paymentMethod->id => $item->paymentMethod->name];
            }
            return [];
        });

        $defaultPaymentMethod = auth()->user()->paymentsMethod()->where('default', 1)->get()->first();
        $defaultSelectPayment = !empty($defaultPaymentMethod) ? $defaultPaymentMethod->payment_id : null;

        $withdraws = auth()->user()->withdraws()->orderBy('created_at', 'desc')->get();

        return view('frontend.pages.financial.index', compact('accruals', 'payment_method', 'withdraws', 'defaultSelectPayment'));
    }

    public function filter()
    {
        $accruals = auth()->user()->accruals()->whereDate('created_at', request('date'))->orderBy('created_at', 'desc')->get();
        $payment_method = UserPaymentMethods::whereNotNull('number_card')->get()->mapWithKeys(function ($item) {
            if($item->paymentMethod->status == PaymentMethod::STATUS_ACTIVE){
                return [$item->paymentMethod->id => $item->paymentMethod->name];
            }
            return [];
        });

        $defaultPaymentMethod = auth()->user()->paymentsMethod()->where('default', 1)->get()->first();
        $defaultSelectPayment = !empty($defaultPaymentMethod) ? $defaultPaymentMethod->payment_id : null;

        $withdraws = auth()->user()->withdraws()->whereDate('created_at', request('date'))->orderBy('created_at', 'desc')->get();

        return view('frontend.pages.financial.index', compact('accruals', 'payment_method', 'withdraws', 'defaultSelectPayment'));
    }
}