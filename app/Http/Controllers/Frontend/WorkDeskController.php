<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

class WorkDeskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web')->except('welcome');
    }

    public function welcome()
    {
        return view('frontend.welcome');
    }

    public function index()
    {
        $role = auth()->user()->roles()->first();

        switch($role->name) {
            case 'designer': {
                return (new DesignerWorkDeskController)->index();
            }
        }
    }
}