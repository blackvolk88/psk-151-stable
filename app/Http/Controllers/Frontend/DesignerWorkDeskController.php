<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Common\FilePrice;
use Illuminate\Support\Facades\File as FileFacade;
use Intervention\Image\ImageManager;

use Storage;
use DB;
use ZipStream\ZipStream;
use App\Models\Common\Batch;
use App\Models\Common\File;
use App\Models\Common\FileStatus;
use App\Models\Common\ModifiedFile;
use App\Models\Common\Status;

class DesignerWorkDeskController extends Controller
{
    private $response;

    private $supportedColorSpace = [1, 13];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {
        $takenBatch = auth()->user()->batches()->where('status_id', '!=', Status::STATUS_COMPLETED)->first();
        $availableBatch = Batch::availableToAssign()->first();

        if (!is_null($takenBatch)) {
            return redirect(route('batch.assigned.view', $takenBatch));
        }

        if (!is_null($availableBatch)) {
            return redirect(route('batch.available.preview', $availableBatch));
        }

        return view('frontend.designer.dashboard.index')->with($this->response()->main());
    }

    public function filter()
    {
        return view('frontend.designer.dashboard.index')->with($this->response()->filtered());
    }

    private function response()
    {
        if (is_null(auth()->user()->passport_url)) {
            session()->flash('modal-message', 'Photo copy of passport has to be uploaded');
        }

        $difficultyCollection = collect();
        $workTypeCollection = collect();
        Batch::availableToAssign()->get()->each(function ($batch) use ($difficultyCollection, $workTypeCollection) {
            if (!$difficultyCollection->contains($batch->difficulty)) {
                $difficultyCollection->push($batch->difficulty);
            }

            if (!$workTypeCollection->contains($batch->type)) {
                $workTypeCollection->push($batch->type);
            }
        });

        $this->response['difficulties'] = $difficultyCollection;
        $this->response['types'] = $workTypeCollection;
        $this->response['takenBatches'] = auth()->user()->batches()->where('status_id', '!=', Status::STATUS_COMPLETED)->get();

        return $this;
    }

    private function main()
    {
        $takenBatches = auth()->user()->batches()->where('status_id', '!=', Status::STATUS_COMPLETED)->count();
        $limit = auth()->user()->designerLevel()->first()->limit;

        if ($takenBatches == $limit) {
            $this->response['availableBatches'] = collect();
            $this->response['limitExceeded'] = true;
        } else {
            $this->response['availableBatches'] = Batch::availableToAssign()->get();
            $this->response['limitExceeded'] = false;
        }

        return $this->response;
    }

    private function filtered()
    {
        $this->validate(request(), [
            'difficulty' => 'integer',
            'type' => 'integer',
        ]);

        $conditions = [];
        $availableFilters = [
            'difficulty' => 'difficulty_id',
            'type' => 'work_type_id'
        ];

        foreach (request()->all() as $filter => $param) {
            if (isset($availableFilters[$filter])) {
                $conditions[] = [$availableFilters[$filter], $param];
            }
        }

        $takenBatches = auth()->user()->batches()->where('status_id', '!=', Status::STATUS_COMPLETED)->count();
        $limit = auth()->user()->designerLevel()->first()->limit;

        if ($takenBatches == $limit) {
            $this->response['availableBatches'] = collect();
            $this->response['limitExceeded'] = true;
        } else {
            $this->response['availableBatches'] = Batch::availableToAssign()->where($conditions)->get();
            $this->response['limitExceeded'] = false;
        }

        $this->response['currentDifficulty'] = (!is_null(request('difficulty'))) ? request('difficulty') : '';
        $this->response['currentType'] = (!is_null(request('type'))) ? request('type') : '';

        return $this->response;
    }

    public function viewAvailableBatch(Batch $batch)
    {
        if ($batch->status->id !== Status::STATUS_NEW) {
            return redirect(route('workdesk'))
                ->with('modal-message', 'This batch now is not available');
        }

        $designerRate = auth()->user()->designerLevel()->first();
        $batchCoefficient = $batch->difficulty;
        $designWorkPrice = FilePrice::design();
        $workPrice = $batchCoefficient->coefficient * $designerRate->coefficient * $designWorkPrice->price;

        $response = [
            'batch' => $batch,
            'currentBatch' => $batch->id,
            'costPhoto' => (float)$workPrice,
            'formats' => $batch->files()->distinct('mime_type')->pluck('mime_type')
        ];

        $response = array_merge($response, $this->response()->main());

        return view('frontend.designer.dashboard.view-available-batch', $response);
    }

    public function takeBatch(Batch $batch)
    {
        if (!auth()->user()->active) {
            return redirect()->back()
                ->with('modal-message', 'Your account is not activated');
        }

        return DB::transaction(function () use ($batch) {
            if ($batch->status->id !== Status::STATUS_NEW) {
                return redirect(route('workdesk'))
                    ->with('modal-message', 'This batch now is not available');
            }

            $batch->executor()->attach(auth()->user()->id);
            $batch->status_id = Status::STATUS_PROCESS;
            $batch->save();

            return redirect(route('batch.assigned.view', $batch));
        });
    }

    public function refuseBatch(Batch $batch)
    {
        return DB::transaction(function () use ($batch) {
            if ($batch->status->id !== Status::STATUS_PROCESS) {
                return response('This batch cannot be refused', 400);
            }

            $batch->executor()->detach(auth()->user()->id);
            $batch->status_id = Status::STATUS_NEW;
            $batch->save();

            session()->flash('modal-message', 'Batch refused successfully');
            return response('Batch refused successfully', 200);
        });
    }

    public function viewAssignedBatch(Batch $batch)
    {
        if ($batch->status->id == Status::STATUS_NEW || $batch->status->id == Status::STATUS_COMPLETED) {
            return redirect(route('workdesk'))
                ->with('modal-message', 'This batch is not available');
        }


        $designerRate = auth()->user()->designerLevel()->first();
        $batchCoefficient = $batch->difficulty;
        $designWorkPrice = FilePrice::design();
        $workPrice = $batchCoefficient->coefficient * $designerRate->coefficient * $designWorkPrice->price;

        $response = [
            'batch' => $batch,
            'currentBatch' => $batch->id,
            'batchFiles' => $batch->files()->get(),
            'formats' => $batch->files()->distinct('mime_type')->pluck('mime_type'),
            'fileStatuses' => FileStatus::withoutDefault()->get(),
            'costPhoto' => (float)$workPrice,
            'filesDeclineBatch' => $batch->files()->isDecline()
        ];

        if ($batch->status->id !== Status::STATUS_PROCESSED) {
            $response['availableRefuse'] = true;
            $response['notice'] = 'You can refuse this batch until you download at least one file';
        }

        $response = array_merge($response, $this->response()->main());

        return view('frontend.designer.dashboard.view-assigned-batch', $response);
    }

    public function sort(Batch $batch, $status)
    {
        $response = [
            'batch' => $batch,
            'currentBatch' => $batch->id,
            'batchFiles' => $batch->files()->where(['status_id' => $status])->get(),
            'currentStatus' => $status,
            'formats' => $batch->files()->distinct('mime_type')->pluck('mime_type'),
            'fileStatuses' => FileStatus::withoutDefault()->get()
        ];

        if ($batch->status->id !== Status::STATUS_PROCESSED) {
            $response['availableRefuse'] = true;
        }

        $response = array_merge($response, $this->response()->main());

        return view('frontend.designer.dashboard.view-assigned-batch', $response);
    }


    public function downloadSelected($id)
    {
        $this->validate(request(), [
            'files' => 'required|array'
        ]);

        session(['selected-files' => request('files')]);

        return response(route('batch.assigned.download', $id), 200);
    }

    public function downloadFiles(Batch $batch)
    {
        return DB::transaction(function () use ($batch) {
            if (request()->session()->exists('selected-files')) {
                $requestedFiles = File::withoutApproved()->whereIn('id', request()->session()->pull('selected-files'))->get();

                if ($requestedFiles->count() == 0) {
                    return redirect(route('batch.assigned.view', $batch))
                        ->with('modal-message', 'File(s) for downloads are not available');
                }

                if ($batch->status_id == Status::STATUS_PROCESS) {
                    $batch->status_id = Status::STATUS_PROCESSED;
                    $batch->started_at = date('Y-m-d H:i:s');
                    $batch->save();
                }

                return $this->streamZip($batch->id, $requestedFiles);
            } else {
                $batchFiles = $batch->files()->withoutApproved()->get();

                if ($batchFiles->count() == 0) {
                    return redirect(route('batch.assigned.view', $batch))
                        ->with('modal-message', 'File(s) for downloads are not available');
                }

                if ($batch->status_id == Status::STATUS_PROCESS) {
                    $batch->status_id = Status::STATUS_PROCESSED;
                    $batch->started_at = date('Y-m-d H:i:s');
                    $batch->save();
                }

                return $this->streamZip($batch->id, $batchFiles);
            }
        });
    }

    private function streamZip($name, $requestedFiles)
    {
        return response()->stream(function () use ($name, $requestedFiles) {
            $zip = new ZipStream($name . ".zip", [
                'content_type' => 'application/octet-stream'
            ]);

            $requestedFiles->each(function ($file) use ($zip) {
                $zip->addFile($file->storage_name, file_get_contents($file->path), [], "store");
            });

            $zip->finish();
        });
    }

    public function downloadDeclineFiles(Batch $batch)
    {
        $batchFilesDecline = $batch->files()->isDecline();

        return response()->stream(function () use ($batch, $batchFilesDecline) {
            $zip = new ZipStream($batch->id . ".zip", [
                'content_type' => 'application/octet-stream'
            ]);

            $batchFilesDecline->each(function ($file) use ($zip) {
                $zip->addFile($file->storage_name, file_get_contents($file->modified->path), [], "store");
            });

            $zip->finish();
        });
    }

    public function downloadDeclineFile(File $file)
    {
        return response()->stream(function () use ($file) {
            $zip = new ZipStream(uniqid() . ".zip", [
                'content_type' => 'application/octet-stream'
            ]);

            $zip->addFile($file->storage_name, file_get_contents($file->modified->path), [], "store");

            $zip->finish();
        });
    }

    public function upload(Batch $batch)
    {
        $validator = validator()->make(request()->all(), [
            'file' => 'required|mimes:jpeg,jpg,png,tiff,raw'
        ]);

        if ($validator->fails()) {
            return response('File format not supported', 400);
        }

        $requestFile = request()->file('file');

        $originalFile = $batch->files()->where(['storage_name' => $requestFile->getClientOriginalName()])->first();

        if (is_null($originalFile)) {
            return response('File not found from assign for this batch', 400);
        }

        if ($originalFile->status_id == FileStatus::STATUS_APPROVED) {
            return response('File already approved', 400);
        }

        $manager = new ImageManager(['driver' => 'imagick']);

        $tmpFile = $manager->make($requestFile);
        $exif = $tmpFile->exif();

        if (!isset($exif['ColorSpace'])) {
            return response('Color space not set', 400);
        }

        if (!in_array($exif['ColorSpace'], $this->supportedColorSpace)) {
            return response('Color space in not supported', 400);
        }

        $fileHash = md5_file($requestFile->getRealPath());

        if (!is_null($originalFile->hash)) {
            if ($fileHash == $originalFile->hash) {
                return response('File not modified', 400);
            }
        }

        return DB::transaction(function () use ($batch, $originalFile, $requestFile) {
            $storage = Storage::disk('s3');
            $publicStorage = Storage::disk('public');

            $manager = new ImageManager(['driver' => 'imagick']);

            $path = 'modified/' . date('mY') . '/' . auth()->user()->id . '/' . $requestFile->getClientOriginalName();

            if ($storage->put($path, file_get_contents($requestFile), 'public')) {
                $thumbImg = $manager->make($requestFile);

                $thumbImg->resize(1024, 1024, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $thumbDirectory = 'modified-thumbs/' . date('mY') . '/' . auth()->user()->id;
                if (!FileFacade::isDirectory($publicStorage->path($thumbDirectory))) {
                    FileFacade::makeDirectory($publicStorage->path($thumbDirectory), 493, true);
                }

                $extension = $requestFile->getClientOriginalExtension();
                $name = substr($requestFile->getClientOriginalName(), 0, -(strlen($extension) + 1));

                $thumbImg->save($publicStorage->path($thumbDirectory . '/' . $name . '.png'));

                ModifiedFile::updateOrCreate([
                    'parent_id' => $originalFile->id,
                    'batch_id' => $batch->id
                ],
                    [
                        'parent_id' => $originalFile->id,
                        'batch_id' => $batch->id,
                        'thumb_path' => $thumbDirectory . '/' . $thumbImg->filename . '.png',
                        'path' => $storage->url($path),
                        'storage_path' => $path
                    ]);

                $originalFile->status_id = FileStatus::STATUS_DESIGNED;
                $originalFile->save();

                return response('File uploaded successfully', 200);
            }

            return response('Error from upload file', 400);
        });
    }

    public function viewDeclineReason(File $file)
    {
        $declineTicket = $file->decline()->orderBy('created_at', 'desc')->first();

        $response = [
            'filename' => $file->storage_name,
            'reasons' => $declineTicket->reasons,
            'comment' => $declineTicket->comment
        ];

        return view('frontend.designer.dashboard.view-decline-reason', $response);
    }
}