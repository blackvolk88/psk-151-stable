<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Common\Setting;
use App\Models\Common\Withdraw;
use Illuminate\Support\Facades\DB;

class WithdrawController extends Controller
{
    public function store()
    {
        $this->validate(request(), [
            'amount' => 'required|numeric|max:' . auth()->user()->balance->balance . '|min:' . Setting::where('key', 'min_cash_out')->get()->first()->value,
            'type' => 'required|int|exists:payment_methods,id'
        ]);

        DB::beginTransaction();

        try {
            $withdraw = Withdraw::create([
                'status' => Withdraw::STATUS_NEW,
                'amount' => request('amount'),
                'number_card' => auth()->user()->paymentsMethod()->where('payment_id', request('type'))->get()->first()->number_card,
                'type' => request('type'),
                'user_id' => auth()->user()->id,
            ]);

            $balanceUser = auth()->user()->balance;
            $balanceUser->balance = ((float)$balanceUser->balance - (float)request('amount'));
            $balanceUser->save();

            DB::commit();

            return response('The application was successfully accepted', 200);

        } catch (Exception $ex) {
            DB::rollback();
            return back();
        }
    }
}
