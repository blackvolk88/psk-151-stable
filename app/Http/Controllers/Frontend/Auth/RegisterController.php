<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;

use Exception;

use App\Models\Frontend\User;
use App\Models\Frontend\UserBalance;
use App\Models\Common\DesignerLevel;
use App\Models\Common\Role;
use App\Jobs\SendVerificationEmail;
use App\Jobs\SendNotificationUserRegistration;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show registration form
     */
    public function showRegistrationForm()
    {
        return view('frontend.auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|min:3|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'skype' => 'required|string|min:3',
            'phone' => 'required|phone:AUTO',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\Frontend\User
     */
    protected function create(array $data)
    {
        DB::beginTransaction();

        try {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'skype' => $data['skype'],
                'phone' => $data['phone'],
                'password' => bcrypt($data['password']),
                'verify_token' => base64_encode($data['email'])
            ]);

            UserBalance::create([
                'user_id' => $user->id,
                'balance' => '0.00'
            ]);

            $user->designerLevel()->attach(DesignerLevel::levelBeginner());
            $user->attachRole(Role::designerRole());
            $user->save();

            DB::commit();

            return $user;
        }
        catch (Exception $ex) {
            DB::rollback();
            return back();
        }
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        dispatch(new SendVerificationEmail($user));

        dispatch(new SendNotificationUserRegistration($user));

        return view('frontend.pages.verify.verification');
    }

    /**
     * Handle a registration request for the application
     * @param $token
     * @return \Illuminate\Http\Response;
     */
    public function verify($token)
    {
        $user = User::where(['verify_token' => $token])
            ->where(['verified' => 0])
            ->firstOrFail();

        $user->verified = 1;
        $user->save();

        return view('frontend.pages.verify.confirmation', ['user' => $user]);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('web');
    }
}