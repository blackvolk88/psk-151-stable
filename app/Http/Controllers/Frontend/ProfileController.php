<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Models\Common\PaymentMethod;
use App\Models\Frontend\UserPaymentMethods;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File as FileFacade;
use Intervention\Image\ImageManager;

use App\Models\Common\FileStatus;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function index()
    {
        $userFilesCount = 0;
        auth()->user()->batches()->get()->each(function ($batch) use (&$userFilesCount) {
            $userFilesCount += $batch->files()->where(['status_id' => FileStatus::STATUS_APPROVED])->count();
        });

        $response = [
            'processed_files' => $userFilesCount,
            'payment_methods' => PaymentMethod::where('status', PaymentMethod::STATUS_ACTIVE)->get()
        ];

        return view('frontend.pages.profile.index', $response);
    }

    public function uploadPhoto()
    {
        $validator = validator()->make(request()->all(), [
            'photo' => 'required|mimes:jpeg,jpg,png'
        ]);

        if ($validator->fails()) {
            return redirect(route('profile'))
                ->with('modal-message', 'File type not supported');
        }

        $requestFile = request()->file('photo');
        $storage = Storage::disk('public');

        $storage->delete(storage_path() . auth()->user()->photo_url);

        $manager = new ImageManager(['driver' => 'imagick']);

        $originalImg = $manager->make($requestFile);
        $width = $originalImg->width();
        $height = $originalImg->height();

        if ($width < $height) {
            $originalImg->crop($width, $width);
        } else {
            $originalImg->crop($height, $height);
        }

        $photoDirectory = 'photos/' . auth()->user()->id;
        $path = $photoDirectory . '/' . $requestFile->getClientOriginalName();

        if (!FileFacade::isDirectory($storage->path($photoDirectory))) {
            FileFacade::makeDirectory($storage->path($photoDirectory), 493, true);
        }

        if ($originalImg->save($storage->path($path))) {
            auth()->user()->photo_url = $path;
            auth()->user()->save();

            return redirect(route('profile'))
                ->with('modal-message', 'File uploaded successfully');
        }

        return redirect(route('profile'))
            ->with('modal-message', 'File not uploaded');
    }

    public function uploadPassport()
    {
        $validator = validator()->make(request()->all(), [
            'file' => 'required|mimes:jpeg,jpg,png'
        ]);

        if ($validator->fails()) {
            return redirect(route('profile'))
                ->with('modal-message', 'File type not supported');
        }

        if (auth()->user()->active) {
            return redirect(route('profile'))
                ->with('modal-message', 'Profile already approved');
        }

        $requestFile = request()->file('file');
        $storage = Storage::disk('public');

        if ($storage->putFileAs('passport/' . auth()->user()->id, $requestFile, $requestFile->getClientOriginalName())) {
            $storage->delete(storage_path() . auth()->user()->passport_url);

            auth()->user()->passport_url = 'passport/' . auth()->user()->id . '/' . $requestFile->getClientOriginalName();
            auth()->user()->save();

            return redirect(route('profile'))
                ->with('modal-message', 'Passport uploaded successfully');
        }

        return redirect(route('profile'))
            ->with('modal-message', 'Passport not uploaded');
    }

    public function edit()
    {
        $this->validate(request(), [
            'name' => 'string|min:3',
            'email' => 'unique:users,email,' . auth()->user()->id,
            'phone' => 'string|phone:AUTO',
            'skype' => 'string|min:3'
        ]);


        $user = auth()->user();

        if (request()->has('name')) {
            $user->name = request('name');
        }
        if (request()->has('email')) {
            $user->email = request('email');
        }
        if (request()->has('phone')) {
            $user->phone = request('phone');
        }
        if (request()->has('skype')) {
            $user->skype = request('skype');
        }


      
        if(!empty(request('paymentMethods'))) {
            foreach (request('paymentMethods') as $payment) {
                $paymentsMethod = auth()->user()->paymentsMethod()->where('payment_id', $payment['paymentId'])->get()->first();
                if (empty($paymentsMethod)) {
                    UserPaymentMethods::create([
                        'user_id' => auth()->user()->getAuthIdentifier(),
                        'payment_id' => $payment['paymentId'],
                        'number_card' => $payment['paymentCard'],
                        'default' => isset($payment['default']) ? $payment['default'] : 0,
                    ]);
                } else {
                    $paymentsMethod->number_card = $payment['paymentCard'];
                    $paymentsMethod->default = isset($payment['default']) ? $payment['default'] : 0;
                    $paymentsMethod->save();
                }
            }
        }

        $user->save();

        session()->flash('modal-message', 'Profile updated successfully');

        return response('profile edited', 200);
    }

    public function displayErrors()
    {
        $this->validate(request(), [
            'messages' => 'required|array'
        ]);

        return view('frontend.pages.profile.view-errors')->withMessages(request('messages'));
    }
}