<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;


class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function mode()
    {
        $this->validate(request(), [
            'mode' => 'required|string'
        ]);

        if (request('mode') == 'close') {
            return session()->push('menu-close-mode', true);
        }

        return session()->forget('menu-close-mode');
    }
}