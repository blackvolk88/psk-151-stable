function calcHeights() {
    var userH = $('aside .fixed-height').outerHeight();
    $('.filters-block').css('min-height', userH + 'px');
}

function calcPanelScrollHeight() {
    var scrollH = $(window).height() - $('.work-panel .fixed-height').outerHeight();
    $('.work-panel .scroll-wrap').css('height', scrollH + 'px');
}

function calcPhotosScrollHeight() {
    var scrollH = $(window).height() - $('.work-space .fixed-height').outerHeight();
    $('.work-space .scroll-wrap').css('height', scrollH + 'px');
}

$(document).ready(function () {
    calcHeights();
    calcPanelScrollHeight();
    calcPhotosScrollHeight();

    $('select,input[type="file"]').styler();
    $('.user-data').find('.jq-file__name').text($('.user-data input[type="file"]').attr('placeholder'));
    $('.user-data input[type="file"]').on('change',function(){
        if($('.user-data input[type="file"]').val() == ''){
            $('.user-data').find('.jq-file__name').text($('.user-data input[type="file"]').attr('placeholder'));
        }
    });
    $('.user-photo').find('.jq-file__browse').html('<img src="img/photo-icon.png" alt="">' + $('.user-photo input[type="file"]').attr('placeholder'));

    $(".scroll-wrap").mCustomScrollbar({
        axis: "y",
        mouseWheelPixels: 200,
        moveDragger: true
    });


    $('.toggle-aside').on('click', function () {
        $('.page').toggleClass('close-panel');

    });

    $('.fancy').fancybox();

    $(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonText: "",
            dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
            showOtherMonths: true,
            dateFormat: "dd/mm/yy"
        });
    });
    $('.picker-wrap').on('click', function () {
        $('.ui-datepicker-trigger').trigger('click');
    });
    var toolTimeOut;
    $(".tooltip").on('mousemove', function (pos) {

        var src = $(this).attr('data-src'), cursorX = pos.pageX,
            maxX = $(window).width() - 220,
            heigpop = $(".image-tooltip").height(),
            widthpop = $(".image-tooltip").width(),
            topW = pos.pageY - $(window).scrollTop();

        $(".image-tooltip .main-thumb").attr("src", src);
        clearTimeout(toolTimeOut);
        $('.image-tooltip').addClass("visible");
        if (cursorX > maxX) {
            $(".image-tooltip").css({
                left: pos.pageX - widthpop - 6 + 'px'
            });

        } else {
            $(".image-tooltip").css({
                left: pos.pageX + 6 + 'px'
            });
        }

        if (topW < heigpop) {
            $(".image-tooltip").css({
                top: pos.pageY + 6 + 'px',

            });

        } else {
            $(".image-tooltip").css({
                top: pos.pageY - heigpop - 6 + 'px',

            });
        }

    });
    $(".tooltip").mouseleave(function () {
            toolTimeOut = setTimeout(function () {
                $('.image-tooltip').removeClass("visible");
            }, 300);
        }
    );

    $('.user-data .edit-action').on('click',function(){
        $('.user-data').addClass('editing-data');
        $(this).fadeOut(0);
        $('.user-data input[type="submit"]').fadeIn(300);
    });



    $(window).resize(function () {
        calcHeights();
        calcPanelScrollHeight();
        calcPhotosScrollHeight();
    });
});