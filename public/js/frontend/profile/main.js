$.profileModule = {
    editProfile: function () {
        var data = {};

        var name = $('#profile-name').val();
        var oldName = $('#profile-name').parent().find('span').text();

        if (name !== oldName) {
            data.name = name;
        }

        var email = $('#profile-email').val();
        var oldEmail = $('#profile-email').parent().find('span').text();

        if (email !== oldEmail) {
            data.email = email;
        }

        var phone = $('#profile-phone').val();
        var oldPhone = $('#profile-phone').parent().find('span').text();

        if (phone !== oldPhone) {
            data.phone = phone;
        }

        var skype = $('#profile-skype').val();
        var oldSkype = $('#profile-skype').parent().find('span').text();

        if (skype !== oldSkype) {
            data.skype = skype;
        }

        data.paymentMethods = new Array();
        $('input.payment-methods').each(function () {
            var paymentCard = $(this).val();
            var paymentIdMethod = $(this).attr('id');
            var defaultPaymentMethod = $(this).parent().find('input.default-payment-method:checked').val();

            data.paymentMethods.push({
                paymentId: paymentIdMethod,
                paymentCard: paymentCard,
                default: defaultPaymentMethod
            });
        });


        if (Object.keys(data).length) {
            var req = $.helperModule.ajax('POST', '/profile/edit', data);
            req.success(function () {
                window.location.reload();
            });
            req.error(function (response) {
                var req = $.helperModule.ajax('POST', '/profile/view-errors', {messages: response.responseJSON});
                req.success(function (response) {
                    $.helperModule.notice(response);
                });
            });
        }
    }
};

$.profileModule.events = {
    initForm: function () {

        $('.user-data .edit-action').on('click', function () {
            $('.user-data').addClass('editing-data');
            $('.user-data').removeClass('static-data');
            $(this).fadeOut(0);
            $('.user-data input[type="submit"]').fadeIn(300);
        });

        $('.user-data input[type="submit"]').on('click', function (e) {
            e.preventDefault();
            $('.user-data').removeClass('editing-data');
            $('.user-data').addClass('static-data');

            $('.user-data input[type="submit"]').fadeOut(0);
            $('.user-data .edit-action').fadeIn(300);
            setTimeout(function () {
                $('.user-data').find('.jq-file__name').text($('.user-data input[type="file"]').attr('placeholder'));
            }, 300);

            $.profileModule.editProfile();
        });
    },

    initUploadPhotoSubmit: function () {
        $('#upload-photo').on('change', function () {
            $('#upload-photo-form').submit();
        });
    },

    uploadPassport: function () {
        $('#upload-passport-file').on('change', function () {
            $('#upload-passport-file-form').submit();
        });
    },

    changePaymentMethod: function () {
        $('input.payment-methods').on('keyup change keydown', function () {
            var val = $(this).val();
            var inputDefault = $(this).closest('tr').find('input.default-payment-method');

            if (val.length > 0) {
                inputDefault.prop('disabled', false);
                if ($('input.default-payment-method:checked').length == 0) {
                    inputDefault.prop('checked', true);
                }
            } else {
                inputDefault.prop('disabled', true);
                inputDefault.prop('checked', false);

                if ($('input.default-payment-method:checked').length == 0) {
                    $('input.default-payment-method:not([disabled])').first().prop('checked', true);
                }
            }

        });
    }
};

$(document).ready(function () {
    $.profileModule.events.initForm();
    $.profileModule.events.initUploadPhotoSubmit();
    $.profileModule.events.uploadPassport();
    $.profileModule.events.changePaymentMethod();

    $('.user-data').find('.jq-file__name').text($('.user-data input[type="file"]').attr('placeholder'));
    $('.user-data input[type="file"]').on('change', function () {
        if ($('.user-data input[type="file"]').val() == '') {
            $('.user-data').find('.jq-file__name').text($('.user-data input[type="file"]').attr('placeholder'));
        }
    });
    $('.user-photo').find('.jq-file__browse').html('<img src="/images/frontend/photo-icon.png" alt="">' + $('.user-photo input[type="file"]').attr('placeholder'));
});