$.designerBatchModule = {
    initRefuseNotice: function () {
        $.designerBatchModule.refuseAvailable = true;
    },

    executeDownloadSelected: function (target, files) {
        var fileIDs = [];
        $.each(files, function (key, input) {
            fileIDs.push($(input).attr('target-id'));
        });

        var req = $.helperModule.ajax('POST', '/workdesk/download-selected-files/' + target, {files: fileIDs});
        req.success(function (response) {
            if ($.designerBatchModule.refuseAvailable) {
                window.open(response, "_blank");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }

            window.location.href = response;
        });
    }
};

$.designerBatchModule.refuseAvailable = false;

$.designerBatchModule.events = {
    refuse: function () {
        $('#refuse-batch-button').click(function () {
            var req = $.helperModule.ajax('DELETE', '/workdesk/refuse-assigned-batch/' + $(this).attr('target-id'));
            req.success(function () {
                window.location.href = '/workdesk';
            });
        });
    },

    downloadAll: function () {
        $('#download-all-button').click(function () {
            var target = $(this).attr('target-url');

            if ($.designerBatchModule.refuseAvailable) {
                $('#notice-refuse-message').trigger('click');
            }
            else {
                window.location.href = target;
            }

            $('.confirm-refuse').click(function () {
                $('#refuse-batch-button').remove();
                $('.fancybox-close-small').trigger('click');
                window.open(target, "_blank");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            });
        });
    },

    downloadSelected: function () {
        $('#download-selected-button').click(function () {
            var target = $(this).attr('target-id');
            var selectedFiles = $("input:checked");

            if (selectedFiles.length === 0) {
                return;
            }

            if ($.designerBatchModule.refuseAvailable) {
                $('#notice-refuse-message').trigger('click');
            }
            else {
                $.designerBatchModule.executeDownloadSelected(target, selectedFiles);
            }

            $('.confirm-refuse').click(function () {
                $('#refuse-batch-button').remove();
                $('.fancybox-close-small').trigger('click');
                $.designerBatchModule.executeDownloadSelected(target, selectedFiles);
            });
        });
    },

    downloadDeclineFiles: function () {
      $('#download-decline-selected-button').click(function () {
          var target = $(this).attr('target-url');

          if ($.designerBatchModule.refuseAvailable) {
              $('#notice-refuse-message').trigger('click');
          }
          else {
              window.location.href = target;
          }

          $('.confirm-refuse').click(function () {
              $('#refuse-batch-button').remove();
              $('.fancybox-close-small').trigger('click');
              window.open(target, "_blank");
              setTimeout(function () {
                  window.location.reload();
              }, 2000);
          });

      });  
    },

    sort: function () {
        $('#batch-sort-status').on('change', function () {
            var batchID = $(this).attr('target-id');
            var status = $('#batch-sort-status option:selected').val();

            if (status === '') {
                window.location.href = '/workdesk/view-assigned-batch/' + batchID;
            }
            else {
                window.location.href = '/workdesk/sort-assigned-batch/' + batchID + '/status/' + status;
            }
        });
    },

    viewDeclineReason: function () {
        $('.view-decline-reason').click(function (e) {
            e.preventDefault();
            var targetID = $(this).attr('target-id');
            var currentContent = $('#details_' + targetID).html();

            if (currentContent !== '') {
                $('#details_fancy_' + targetID).trigger('click');
                return false;
            }

            var req = $.helperModule.ajax('GET', '/workdesk/view-designer-decline-file-reason/' + targetID);
            req.success(function (response) {
                $('#details_' + targetID).html(response);
                $('#details_fancy_' + targetID).trigger('click');
            });
        });
    },

    selectOnShift: function () {
        var ind = 1,
            shiftAction = false;
        $('.photos-wrapper .photo-item input[type="checkbox"]').each(function () {
            $(this).attr('data-first', '0');
            $(this).attr('data-last', '0');
            $(this).attr('data-number', ind);
            ind += 1;
        });
        $(document).keydown(function (event) {

            if (event.keyCode == 16) {

                shiftAction = true;
            }
        }).keyup(function(event){
            if (event.keyCode == 16) {
                shiftAction = false;
            }
        });

        $('.photos-wrapper .photo-item input[type="checkbox"]').on('click',function(){
            if($('.photos-wrapper .photo-item input:checked').length){
                $('#unselect').fadeIn(200);
            }else{
                $('#unselect').fadeOut(200);
            }
            $('.photos-wrapper .photo-item input[type="checkbox"]').attr('data-last','0');
            if(!$('.photos-wrapper .photo-item input[data-first="1"]').length){
                $(this).attr('data-first','1');
            }else{
                if(shiftAction){
                    $(this).attr('data-last','1');
                }else{
                    $('.photos-wrapper .photo-item input[type="checkbox"]').attr('data-first','0');
                    $(this).attr('data-first','1');
                }
            }

            if($(this).prop('checked')){

                if($('.photos-wrapper .photo-item input[data-last="1"]').length && $('.photos-wrapper .photo-item input[data-first="1"]').length){
                    var start = $('.photos-wrapper .photo-item input[data-first="1"]').attr('data-number')*1,
                        finish = $('.photos-wrapper .photo-item input[data-last="1"]').attr('data-number')*1;

                    if(finish > start){
                        $('.photos-wrapper .photo-item input[type="checkbox"]').each(function(){
                           var num = $(this).attr('data-number')*1;
                            if(num < finish && num > start){
                                $(this).prop('checked',true);
                            }
                        });
                    }else{
                        $('.photos-wrapper .photo-item input[type="checkbox"]').each(function(){
                            var num = $(this).attr('data-number')*1;
                            if(num < start && num > finish){
                                $(this).prop('checked',true);
                            }
                        });
                    }
                }
            }
        });
    },

    unselect: function () {
        $('#unselect').on('click',function(){
            $('.photos-wrapper .photo-item input:checked').prop('checked',false);
            $(this).fadeOut(200);
        });
    }

};

$(document).ready(function () {
    $.designerBatchModule.events.refuse();
    $.designerBatchModule.events.downloadAll();
    $.designerBatchModule.events.downloadDeclineFiles();
    $.designerBatchModule.events.downloadSelected();
    $.designerBatchModule.events.sort();
    $.designerBatchModule.events.viewDeclineReason();
    $.designerBatchModule.events.selectOnShift();
    $.designerBatchModule.events.unselect();
});