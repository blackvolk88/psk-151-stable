$.designerWorkdeskFiltersModule = {};
$.designerWorkdeskFiltersModule.events = {
    filters: function() {
        $('.filter-items').on('change', function() {
            var difficulty = $('#filter-difficulties option:selected').val();
            var workType = $('#filter-work-types option:selected').val();
            var url = '/workdesk/filter?';

            if ('' !== difficulty) {
                url += 'difficulty=' + difficulty + '&';
            }
            if ('' !== workType) {
                url += 'type=' + workType + '&';
            }

            url = url.slice(0 , -1);
            window.location.href = url;
        });
    }
};

$(document).ready(function() {
    $.designerWorkdeskFiltersModule.events.filters();
});