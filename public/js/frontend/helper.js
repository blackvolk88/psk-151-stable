$.helperModule = {
    ajax: function (type, url, data) {
        return $.ajax({
            url: url,
            type: type,
            data: data || ''
        });
    },

    notice: function (message) {
        $('#notice-content').html(message);
        $('#notice-message').trigger('click');
    },
};

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$.helperForm = {
    errorsForm: function (form, key, val) {
        var element = form.find('[name="' + key + '"]');
        if (form.length === 1) {
            element.addClass('error');
            element.after('<span class="massege-error">' + val + '</span>');
        }

        $('input, select').trigger('refresh');
    },

    refreshErrorsForm: function (form) {
        form.find('input, select').removeClass('error');
        form.find('span.massege-error').remove();
    }
}


$.helperCookie = {

    setCookie: function (name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    },

    getCookie: function (name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    },

    deleteCookie: function (name) {
        $.helperCookie.setCookie(name, "", {
            expires: -1
        })
    }
};
