$.signupModule = {
    initPhoneMask: function() {
        $('.mobile-phone-number').inputmask('+99 (999) 999-99-99', { placeholder: '+__ (___) ___-__-__' });
    }
};

$(document).ready(function() {
    $.signupModule.initPhoneMask();
});