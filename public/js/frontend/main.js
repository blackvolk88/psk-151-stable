if (typeof jQuery === "undefined") {
    throw new Error("jQuery plugins need to be before this file");
}

$.main = {
    displayFlashMessage: function() {
        $('#call-message').trigger('click');
    },

    initMenu: function() {
        $('.toggle-aside').on('click', function () {
            $('.page').toggleClass('close-panel');

            if ($('.page').hasClass('close-panel')) {
                $.helperModule.ajax('POST', '/set-menu-mode', {mode: 'close'});
            }
            else {
                $.helperModule.ajax('POST', '/set-menu-mode', {mode: 'open'});
            }
        });
    },

    calcHeights: function() {
        var userH = $('aside .fixed-height').outerHeight();
        $('.filters-block').css('min-height', userH + 'px');
    },

    calcPanelScrollHeight: function() {
        var scrollH = $(window).height() - $('.work-panel .fixed-height').outerHeight();
        $('.work-panel .scroll-wrap').css('height', scrollH + 'px');
    },

    calcPhotosScrollHeight: function() {
        var scrollH = $(window).height() - $('.work-space .fixed-height').outerHeight();
        $('.work-space .scroll-wrap').css('height', scrollH + 'px');
    },

    initScrollBar: function() {
        $(".scroll-wrap").mCustomScrollbar({
            axis: "y",
            mouseWheelPixels: 200,
            moveDragger: true
        });
    },

    initFancy: function() {
        $('.fancy').fancybox();
    },

    initTooltip: function() {
        var toolTimeOut;
        $(".tooltip").on('mousemove', function (pos) {
            var src = $(this).attr('data-src'), cursorX = pos.pageX,
                maxX = $(window).width() - 220,
                heigpop = $(".image-tooltip").height(),
                widthpop = $(".image-tooltip").width(),
                topW = pos.pageY - $(window).scrollTop();

            $(".image-tooltip .main-thumb").attr("src", src);
            clearTimeout(toolTimeOut);
            $('.image-tooltip').addClass("visible");
            if (cursorX > maxX) {
                $(".image-tooltip").css({
                    left: pos.pageX - widthpop - 6 + 'px'
                });

            } else {
                $(".image-tooltip").css({
                    left: pos.pageX + 6 + 'px'
                });
            }

            if (topW < heigpop) {
                $(".image-tooltip").css({
                    top: pos.pageY + 6 + 'px',

                });

            } else {
                $(".image-tooltip").css({
                    top: pos.pageY - heigpop - 6 + 'px',

                });
            }

        });
        $(".tooltip").mouseleave(function () {
                toolTimeOut = setTimeout(function () {
                    $('.image-tooltip').removeClass("visible");
                }, 300);
            }
        );
    },

    initMasks: function() {
        $('.mobile-phone-number').inputmask('+99 (999) 999-99-99', { placeholder: '+__ (___) ___-__-__' });
    }
};

$.main.events = {
    logout: function() {
        $('#logout-button').click(function(e) {
            e.preventDefault();
            $('#logout-form').submit();
        });
    }
};

$(document).ready(function () {
    $(window).load(function () {
        $.main.calcHeights();
        $.main.calcPanelScrollHeight();
        $.main.calcPhotosScrollHeight();
    });

    $.main.initMenu();
    $.main.initScrollBar();
    $.main.initFancy();
    $.main.initTooltip();
    $.main.initMasks();
    $.main.events.logout();

    $('select,input[type="file"]').styler();

    $('.upload-form').find('.jq-file__browse').html('<img src="/images/frontend/upload-icon.png" alt="">' + $('.upload-form input[type="file"]').attr('placeholder'));
});