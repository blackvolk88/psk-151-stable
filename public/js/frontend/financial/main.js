$.financialModule = {};

$.financialModule.events = {
    initDatePicker: function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonText: "",
            dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
            showOtherMonths: true,
            dateFormat: "yy-mm-dd"
        });

        $('.picker-wrap').on('click', function () {
            $('.ui-datepicker-trigger').trigger('click');
        });

        $("#datepicker").on("change", function () {
            window.location.href = '/financial/filter/?date=' + $(this).val();
        });
    },
    createWithdraw: function () {
        $('#create-withdraw').submit(function (e) {
            e.preventDefault();
            var formWithdraw = $(this);
            var data = $(this).serialize();

            var req = $.helperModule.ajax('POST', '/financial/withdraw/new', data);
            req.success(function (response) {
                $.helperModule.notice(response);
                setTimeout(function () {
                    $.helperCookie.setCookie('finance_tab', 'tab2');
                    window.location.reload();
                }, 1500);
            });

            req.error(function (response) {
                if (response.responseJSON) {
                    $.helperForm.refreshErrorsForm(formWithdraw);
                    $.each(response.responseJSON, function (key, val) {
                        $.helperForm.errorsForm(formWithdraw, key, val);
                    });
                }
            });
        });
    },
    tabInit: function () {
        var tabActive = $.helperCookie.getCookie('finance_tab');
        $('#' + tabActive).prop('checked', true);
    },
    tabClick: function () {
        $('.tabs-buttons label').on('click ontouchend', function () {
            var idTab = $(this).attr('for');
            $.helperCookie.setCookie('finance_tab', idTab);
        });
    }
};

$(document).ready(function () {
    $.financialModule.events.initDatePicker();
    $.financialModule.events.createWithdraw();
    $.financialModule.events.tabInit();
    $.financialModule.events.tabClick();
});