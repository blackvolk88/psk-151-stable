$.adminHelper = {
    modalObj: {},

    ajax: function(type, url, data) {
        return $.ajax({
            url: url,
            type: type,
            data: data || ''
        });
    },

    overlay: function(action) {
        if ('show' === action) {
            setTimeout(function () { $('.page-loader-wrapper').fadeIn(); }, 50);
        }
        else {
            setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
        }
    },

    notification: function(color, message) {
        var allowDismiss = true;
        $.notify(
            {
                message: message
            },
            {
                type: color,
                allow_dismiss: allowDismiss,
                newest_on_top: true,
                timer: 2000,
                placement: {
                    from: 'top',
                    align: 'center'
                },
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
                template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    },

    modal: function(settings) {
        if ($.isEmptyObject(this.modalObj)) {
            this.modalObj = new jBox('Modal', {
                closeOnEsc: true,
                closeButton: 'title',
                closeOnClick: 'title',
                width: settings.width || 'auto',
                maxWidth: settings.width || 'auto',
                height: settings.height || 'auto',
                maxHeight: settings.height || 'auto',
                zIndex: 100,
                delayOpen: 500
            });
        }

        this.modalObj.setTitle(settings.title);
        this.modalObj.setContent(settings.content, true);
    },

    modalAction: function(action) {
        if ('open' === action) {
            this.modalObj.open();
        }
        else {
            this.modalObj.close();
        }
    },

    bytesToHuman: function(bytes) {
        if (bytes === 0) {
            return 0;
        }

        var units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];

        for (var i = 0; bytes > 1024; i++) {
            bytes /= 1024;
        }

        return bytes.toFixed(2) + ' ' + units[i];
    },

    onlyUnique: function(value, index, self) {
        return self.indexOf(value) === index;
    }
};

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});