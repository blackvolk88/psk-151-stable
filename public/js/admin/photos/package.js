$.adminPhotosPackageModule = {
    redirect: function() {
        var packageID = $('#package-id').val();
        var url = '/admin/photos/packages/view/' + packageID + '?';
        var format = $('#file-format option:selected').val();
        var search = $('#search-file-name').val();

        if ('' !== format) {
            url += 'format=' + format + '&';
        }

        if ('' !== search) {
            url += 'search=' + search + '&';
        }

        url = url.slice(0 , -1);
        window.location.href = url;
    },

    filterPackageRedirect: function() {
        var url = 'list?';
        var search =  $('#search-package-name').val();

        if ('' !== search) {
            url += 'package=' + search + '&';
        }

        url = url.slice(0 , -1);
        window.location.href = url;
    }
};

$.adminPhotosPackageModule.events = {
    filterPackage: function() {
        $('#search-package-button').click(function() {
            if ('' == $('#search-package-name').val()) {
                $.adminHelper.notification('bg-red', 'Please specify package name');
                return false;
            }

            $.adminPhotosPackageModule.filterPackageRedirect();
        });

        $('#search-package-name').keyup(function(e){
            if(e.keyCode == 13) {
                $.adminPhotosPackageModule.filterPackageRedirect();
            }
        });
    },

    prepareDownloadPackage: function() {
        $('.prepare-download-package').click(function(e) {
            e.preventDefault();

            var req = $.adminHelper.ajax('GET', '/admin/photos/packages/prepare-download/' + $(this).attr('target-id'));
            req.success(function(response) {
                var settings = {
                    title: 'Prepare download package',
                    content: response
                };
                $.adminHelper.modal(settings);
                $.adminHelper.modalAction('open');
            });
        });
    },

    initCloseDownloadPackage: function() {
        $('.download-package-files').click(function() {
            $.adminHelper.modalAction('close');
        });
    },

    deletePackage: function() {
        $('.delete-package').click(function(e) {
            e.preventDefault();
            $.adminHelper.overlay('show');

            var req = $.adminHelper.ajax('DELETE', '/admin/photos/packages/destroy/' + $(this).attr('target-id'));
            req.success(function(response) {
                $.adminHelper.overlay('hide');
                $.adminHelper.notification('bg-green', response);
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            });
            req.error(function(response) {
                $.adminHelper.overlay('hide');
                $.adminHelper.notification('bg-red', response.responseText);
            });
        });
    },

    filterFiles: function() {
        $('#file-format').on('change', function() {
            $.adminPhotosPackageModule.redirect();
        });

        $('#search-file-button').click(function() {
            $.adminPhotosPackageModule.redirect();
        });

        $('#search-file-name').keyup(function(e){
            if(e.keyCode == 13) {
                $.adminPhotosPackageModule.redirect();
            }
        });
    },

    previewFile: function() {
        $('.preview-image').click(function(e) {
            e.preventDefault();

            var req = $.adminHelper.ajax('GET', '/admin/photos/packages/file/preview/' + $(this).attr('target-id'));
            req.success(function(response) {
                var settings = {
                    title: 'Preview file',
                    content: response
                };
                $.adminHelper.modal(settings);
                $.adminHelper.modalAction('open');
            });
            req.error(function(response) {
                $.adminHelper.notification('bg-red', response.responseText);
            });
        });
    },

    deleteFile: function() {
        $('.delete-file').click(function(e) {
            e.preventDefault();
            $.adminHelper.overlay('show');

            var req = $.adminHelper.ajax('DELETE', '/admin/photos/packages/destroy-file/' + $(this).attr('parent-id')  + '/'+ $(this).attr('target-id'));
            req.success(function(response) {
                $.adminHelper.overlay('hide');
                $.adminHelper.notification('bg-green', response);
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            });
            req.error(function(response) {
                $.adminHelper.overlay('hide');
                $.adminHelper.notification('bg-red', response.responseText);
            });
        });
    },
};

$(document).ready(function() {
    $.adminPhotosPackageModule.events.filterPackage();
    $.adminPhotosPackageModule.events.prepareDownloadPackage();
    $.adminPhotosPackageModule.events.deletePackage();
    $.adminPhotosPackageModule.events.filterFiles();
    $.adminPhotosPackageModule.events.previewFile();
    $.adminPhotosPackageModule.events.deleteFile();
});