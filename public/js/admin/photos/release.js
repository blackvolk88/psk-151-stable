$.adminPhotosReleaseModule = {};

$.adminPhotosReleaseModule.images = [];

$.adminPhotosReleaseModule.events = {
    deleteRelease: function() {
        $('.delete-release').click(function(e) {
            e.preventDefault();

            var req = $.adminHelper.ajax('DELETE', '/admin/releases/destroy/' + $(this).attr('target-id'));
            req.success(function(response) {
                $.adminHelper.notification('bg-green', response);
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            });
            req.error(function(response) {
                $.adminHelper.notification('bg-red', response.responseText);
            });
        });
    },

    changePackage: function() {
        $('#package-list').change(function() {
            var target = $(this).attr('target-id');
            var selectItem = $('#package-list option:selected').val();

            if (selectItem === '') {
                window.location.href = '/admin/releases/prepare-assign/' + target;
            }
            else {
                window.location.href = '/admin/releases/prepare-assign/' + target + '/package/' + selectItem;
            }
        });
    },

    bindAddFile: function() {
        $('.batch-preview-thumb a').click(function() {
            $(this).toggleClass('check');
            var $this = $(this).find('.batch-image');
            $this.toggleClass('batch-image-selected');
            $this.closest('.preview-thumb').toggleClass('wrap-image-selected');
            var imageID = $this.attr('target-id');

            if ($.inArray(imageID, $.adminPhotosReleaseModule.images) === -1) {
                $.adminPhotosReleaseModule.images.push(imageID);
            }
            else {
                $.adminPhotosReleaseModule.images = $.grep($.adminPhotosReleaseModule.images, function(value) {
                    return value != imageID;
                });
            }
        });
    },

    bindAssignFiles: function() {
        $('#assign-files').click(function() {
            if ($.adminPhotosReleaseModule.images.length) {
                var target = $(this).attr('target-id');

                var req = $.adminHelper.ajax('POST', '/admin/releases/assign/' + target, {files: $.adminPhotosReleaseModule.images});
                req.success(function(response) {
                    $.adminHelper.notification('bg-green', response);
                    setTimeout(function() {
                        window.location.href = '/admin/releases/prepare-assign/' + target;
                    }, 2000);
                });
                req.error(function() {
                    $.adminHelper.notification('bg-red', 'Please contact from administrator');
                });
            }
            else {
                $.adminHelper.notification('bg-red', 'File(s) for assign not selected');
            }
        });
    },

    resizeHeights: function () {
        var wH = $(window).height(),
            headH = $('.card .header').outerHeight();
        $('.card').css('height', wH + 'px');
        $('.card .body').css('height', (wH - headH) + 'px');

        var fixH = $('.fixed-height').height();

        $('.scrolling-batches').css('height', (fixH) + 'px');


        $(window).resize(function () {
            wH = $(window).height(),
                headH = $('.card .header').outerHeight();
            $('.card').css('height', wH + 'px');
            $('.card .body').css('height', (wH - headH) + 'px');

            fixH = $('.fixed-height').height();

            $('.scrolling-batches').css('height', (fixH) + 'px');
        });
        $(".scrolling-batches").mCustomScrollbar({
            axis: "y",
            mouseWheelPixels: 200,
            moveDragger: true
        });
    },

    selectAll: function() {
        $('#select-all-files').on('click', function() {
            $('#unselect').prop('disabled',false);
            $('.preview-thumb a').addClass('check');
            $(this).prop('disabled',true);
                var files = $('.batch-preview-thumb a');

                $.each(files, function(key, file) {
                    var $this = $(this).find('.batch-image');
                    $this.addClass('batch-image-selected');
                    $this.closest('.preview-thumb').addClass('wrap-image-selected');
                    var imageID = $this.attr('target-id');
                    $.adminPhotosReleaseModule.images.push(imageID);
                });

        });
    },
    selectOnShift: function () {
        var ind = 1,
            shiftAction = false;
        $('.preview-thumb a').each(function () {
            $(this).attr('data-first', '0');
            $(this).attr('data-last', '0');
            $(this).attr('data-number', ind);
            ind += 1;
        });
        $(document).keydown(function (event) {

            if (event.keyCode == 16) {
                shiftAction = true;
            }
        }).keyup(function (event) {
            if (event.keyCode == 16) {
                shiftAction = false;
            }
        });

        $('.preview-thumb a').on('click', function () {
            if($('.preview-thumb a.check').length){
                $('#unselect').prop('disabled',false);
            }else{
                $('#unselect').prop('disabled',true);
            }
            if($('.preview-thumb a.check').length == $('.preview-thumb a').length){
                $('#select-all-files').prop('disabled',true);
            }else{
                $('#select-all-files').prop('disabled',false);
            }
            if(!$(this).hasClass('triggered')) {
                $('.preview-thumb a').attr('data-last', '0');

                if (!$('.preview-thumb a[data-first="1"]').length) {

                    $(this).attr('data-first', '1');
                } else {
                    if (shiftAction) {
                        $(this).attr('data-last', '1');
                    } else {
                        $('.preview-thumb a').attr('data-first', '0');
                        $(this).attr('data-first', '1');
                    }
                }

                if ($(this).hasClass('check')) {
                    if ($('.preview-thumb a[data-last="1"]').length && $('.preview-thumb a[data-first="1"]').length) {
                        var start = $('.preview-thumb a[data-first="1"]').attr('data-number') * 1,
                            finish = $('.preview-thumb a[data-last="1"]').attr('data-number') * 1;
                        if (finish > start) {
                            $('.preview-thumb a:not(.check)').each(function () {

                                var num = $(this).attr('data-number') * 1;

                                if (num < finish && num > start) {
                                    $(this).addClass('triggered');
                                    $(this).trigger('click');
                                }
                            });
                        } else {
                            $('.preview-thumb a:not(.check)').each(function () {
                                var num = $(this).attr('data-number') * 1;
                                if (num < start && num > finish) {
                                    $(this).addClass('triggered');
                                    $(this).trigger('click');
                                }
                            });
                        }
                    }
                }
            }else{
                $(this).removeClass('triggered');
            }
        });
    },
    unselect: function () {
        $('#unselect').on('click',function(){
            $('.preview-thumb a.check').trigger('click');
            $(this).prop('disabled',true);
            $('#select-all-files').prop('disabled',false);
        });
    }

};

$(document).ready(function() {
    $.adminPhotosReleaseModule.events.deleteRelease();
    $.adminPhotosReleaseModule.events.changePackage();
    $.adminPhotosReleaseModule.events.bindAddFile();
    $.adminPhotosReleaseModule.events.bindAssignFiles();
    $.adminPhotosReleaseModule.events.resizeHeights();
    $.adminPhotosReleaseModule.events.selectAll();
    $.adminPhotosReleaseModule.events.selectOnShift();
    $.adminPhotosReleaseModule.events.unselect();
});