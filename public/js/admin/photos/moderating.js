$.adminPhotosModeratingModule = {
    resizeTable: function () {
        var butH = $('.card .buttons-bottom').outerHeight(),
            fixH = $('.fixed-height').height();
        $('.card .scroll-table').css('max-height', (fixH - butH ) + 'px');

        $(window).resize(function () {
            butH = $('.card .buttons-bottom').outerHeight(),
                fixH = $('.fixed-height').height();
            $('.card .scroll-table').css('max-height', (fixH - butH ) + 'px');

        });
        $(".scroll-table").mCustomScrollbar({
            axis: "y",
            mouseWheelPixels: 200,
            moveDragger: true
        });
    }
};

$.adminPhotosModeratingModule.images = [];

$.adminPhotosModeratingModule.events = {
    initSelectSort: function () {
        $('#change-sort-multiple, #change-sort').each(function () {
            var type = $(this).attr('data-selected-type');
            var val = $(this).attr('data-selected-value');
            $(this).find('option[value="' + val + '"][data-type="' + type + '"]').prop('selected', true);
        });
    },
    sort: function () {
        $('#change-sort').on('change', function () {
            var $change = $('#change-sort option:selected');
            window.location.href = '/admin/photos/moderating/sort/' + $change.attr('data-type') + '/' + $change.val();
        });
    },

    sortMode: function () {
        $('#change-sort-multiple').on('change', function () {
            var $change = $('#change-sort-multiple option:selected');
            window.location.href = '/admin/photos/moderating/multiple-moderating/sort/' + $change.attr('data-type') + '/' + $change.val();
        });
    },

    decline: function () {
        $('#decline-button').click(function () {
            var req = $.adminHelper.ajax('GET', '/admin/photos/moderating/decline-form/' + $(this).attr('target-id'));
            req.success(function (response) {
                var settings = {
                    title: 'Decline file',
                    width: 400,
                    content: response
                };
                $.adminHelper.modal(settings);
                $.adminHelper.modalAction('open');
            });
            req.error(function (response) {
                if (response.status == 400) {
                    $.adminHelper.notification('bg-red', response.responseText);
                }
                else {
                    $.adminHelper.notification('bg-red', response.responseText);
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
            });
        });
    },

    initSubmitDeclineForm: function () {
        $('.submit-decline-form').click(function () {
            var selectedReasons = $("input:checked");

            if (selectedReasons.length === 0) {
                $.adminHelper.notification('bg-red', 'Please select reason');
                return false;
            }

            $('#decline-form').submit();
        });
    },

    approve: function () {
        $('#approve-button').click(function () {
            var files = [];
            files.push($(this).attr('target-id'));

            var req = $.adminHelper.ajax('POST', '/admin/photos/moderating/approve', {files: files});
            req.success(function (response) {
                $.adminHelper.notification('bg-green', response);
                setTimeout(function () {
                    window.location.href = '/admin/photos/moderating';
                }, 2000);
            });
            req.error(function (response) {
                $.adminHelper.notification('bg-green', response.responseText);
            });
        });
    },

    removeSelectListFiles: function () {
        $('.unselect-list').click(function () {
            var $this = $(this);
            var imageID = $this.attr('target-id');
            $('.scrolling-previews .main-preview[target-id="' + imageID + '"]').closest('.multiple-thumb').find('a').trigger('click');
        });
    },

    multipleSelectFiles: function () {
        $('.multiple-thumb a').click(function () {
            if (window.shiftAction == false) {
                $.adminPhotosModeratingModule.events.requestSelectFiles($(this), true);
            } else {
                $.adminPhotosModeratingModule.events.requestSelectFiles($(this), false);
            }
        });
    },

    requestSelectFiles: function (element, isRequest) {
        if (element != null) {
            element.toggleClass('check');
            var $this = element.find('.main-preview');
            $this.toggleClass('batch-image-selected');
            $this.closest('.preview-thumb').toggleClass('wrap-image-selected');
            var imageID = $this.attr('target-id');

            if ($.inArray(imageID, $.adminPhotosModeratingModule.images) === -1) {
                $.adminPhotosModeratingModule.images.push(imageID);
            }
            else {
                $.adminPhotosModeratingModule.images = $.grep($.adminPhotosModeratingModule.images, function (value) {
                    return value != imageID;
                });
            }

            if ($('.multiple-thumb a.check').length) {
                $('#unselect').prop('disabled', false);
            } else {
                $('#unselect').prop('disabled', true);
            }
        }

        if (isRequest === true) {
            if ($.adminPhotosModeratingModule.images.length) {
                var req = $.adminHelper.ajax('POST', '/admin/photos/moderating/multiple-moderating/view', {files: $.adminPhotosModeratingModule.images});
                req.success(function (response) {
                    $('#multiple-info').html(response.info);
                    $('#multiple-content').html(response.content);
                    $.adminPhotosModeratingModule.resizeTable();
                    $.adminPhotosModeratingModule.events.removeSelectListFiles();
                });
            } else {
                $('#multiple-info').empty();
                $('#multiple-content').empty();
            }
        }

    },

    downloadSelected: function () {
        $('.download-selected').click(function () {
            var req = $.adminHelper.ajax('POST', '/admin/photos/moderating/multiple-moderating/download-selected', {files: $.adminPhotosModeratingModule.images});
            req.success(function (response) {
                window.location.href = response;
            });
        });
    },

    declineSelected: function () {
        $('.decline-selected').click(function () {
            var req = $.adminHelper.ajax('POST', '/admin/photos/moderating/multiple-moderating/decline-form', {files: $.adminPhotosModeratingModule.images});
            req.success(function (response) {
                var settings = {
                    title: 'Decline files',
                    width: 400,
                    content: response
                };
                $.adminHelper.modal(settings);
                $.adminHelper.modalAction('open');
            });
            req.error(function (response) {
                $.adminHelper.notification('bg-red', response.responseText);
            });
        });
    },

    initSubmitMultipleDecline: function () {
        $('.submit-decline-form').click(function () {
            var selectedReasons = $("input:checked");

            if (selectedReasons.length === 0) {
                $.adminHelper.notification('bg-red', 'Please select reason');
                return false;
            }

            var comment = $('#decline-comment').val();

            var reasons = [];
            $.each(selectedReasons, function (key, reason) {
                reasons.push($(reason).val());
            });

            var data = {
                files: $.adminPhotosModeratingModule.images,
                reasons: reasons,
                comment: comment
            };

            var req = $.adminHelper.ajax('POST', '/admin/photos/moderating/multiple-moderating/decline', data);
            req.success(function (response) {
                $.adminHelper.notification('bg-green', response);
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            });
            req.error(function (response) {
                $.adminHelper.notification('bg-green', response.responseText);
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            });
        });
    },

    approveSelected: function () {
        $('.approve-selected').click(function () {
            var req = $.adminHelper.ajax('POST', '/admin/photos/moderating/approve', {files: $.adminPhotosModeratingModule.images});
            req.success(function (response) {
                $.adminHelper.notification('bg-green', response);
                setTimeout(function () {
                    window.location.href = '/admin/photos/moderating/multiple-moderating';
                }, 2000);
            });
            req.error(function (response) {
                $.adminHelper.notification('bg-green', response.responseText);
            });
        });
    },
    resizeHeights: function () {
        var wH = $(window).height(),
            headH = $('.card .header').outerHeight(),
            butH = $('.card .buttons-bottom').outerHeight(),
            thW = $('.card .big-thumb').width();
        $('.card').css('height', wH + 'px');
        $('.card .body').css('height', (wH - headH) + 'px');

        var fixH = $('.fixed-height').height();
        $('.card .big-thumb').css('height', thW + 'px');
        $('.card .big-thumb').css('max-height', (fixH - butH ) + 'px');
        $('.scrolling-previews').css('height', fixH + 'px');


        $(window).resize(function () {
            wH = $(window).height(),
                headH = $('.card .header').outerHeight(),
                butH = $('.card .buttons-bottom').outerHeight(),
                thW = $('.card .big-thumb').width();
            $('.card').css('height', wH + 'px');
            $('.card .body').css('height', (wH - headH) + 'px');
            fixH = $('.fixed-height').height();
            $('.card .big-thumb').css('height', thW + 'px');
            $('.card .big-thumb').css('max-height', (fixH - butH ) + 'px');
            $('.scrolling-previews').css('height', fixH + 'px');
        });
        $(".scrolling-previews").mCustomScrollbar({
            axis: "y",
            mouseWheelPixels: 200,
            moveDragger: true
        });
    },

    previewOriginalFile: function () {
        $('.preview-image').click(function (e) {
            e.preventDefault();

            var req = $.adminHelper.ajax('GET', '/admin/photos/packages/file/preview/' + $(this).attr('target-id'));
            req.success(function (response) {
                var settings = {
                    title: 'Preview original file',
                    content: response
                };
                $.adminHelper.modal(settings);
                $.adminHelper.modalAction('open');
            });
            req.error(function (response) {
                $.adminHelper.notification('bg-red', response.responseText);
            });
        });
    },
    selectOnShift: function () {
        var ind = 1;
        window.shiftAction = false;
        $('.multiple-thumb a').each(function () {
            $(this).attr('data-first', '0');
            $(this).attr('data-last', '0');
            $(this).attr('data-number', ind);
            ind += 1;
        });
        $(document).keydown(function (event) {

            if (event.keyCode == 16) {
                window.shiftAction = true;
            }
        }).keyup(function (event) {
            if (event.keyCode == 16) {
                window.shiftAction = false;
            }
        });

        $('.multiple-thumb a').on('click', function () {
            if ($('.multiple-thumb a.check').length == $('.multiple-thumb a').length) {
                $('#select-all-files').prop('disabled', true);
            } else {
                $('#select-all-files').prop('disabled', false);
            }
            if (!$(this).hasClass('triggered')) {
                $('.multiple-thumb a').attr('data-last', '0');

                if (!$('.multiple-thumb a[data-first="1"]').length) {

                    $(this).attr('data-first', '1');
                } else {
                    if (window.shiftAction) {
                        $(this).attr('data-last', '1');
                    } else {
                        $('.multiple-thumb a').attr('data-first', '0');
                        $(this).attr('data-first', '1');
                    }
                }

                if ($(this).hasClass('check')) {
                    if ($('.multiple-thumb a[data-last="1"]').length && $('.multiple-thumb a[data-first="1"]').length) {
                        var start = $('.multiple-thumb a[data-first="1"]').attr('data-number') * 1,
                            finish = $('.multiple-thumb a[data-last="1"]').attr('data-number') * 1;
                        if (finish > start) {
                            $('.multiple-thumb a:not(.check)').each(function () {

                                var num = $(this).attr('data-number') * 1;

                                if (num < finish && num > start) {
                                    $(this).addClass('triggered');

                                    $.adminPhotosModeratingModule.events.requestSelectFiles($(this), false);
                                }
                            });
                        } else {

                            $('.multiple-thumb a:not(.check)').each(function () {
                                var num = $(this).attr('data-number') * 1;
                                if (num < start && num > finish) {
                                    $(this).addClass('triggered');
                                    $.adminPhotosModeratingModule.events.requestSelectFiles($(this), false);
                                }
                            });

                        }

                        $.adminPhotosModeratingModule.events.requestSelectFiles(null, true);
                    }
                }
            } else {
                $(this).removeClass('triggered');
            }
        });
    },
    unSelect: function () {
        $('#unselect').on('click', function () {
            if(!$(this).prop('disabled')){
                $('#select-all-files').prop('disabled',false);
            }
            $('.multiple-thumb a.check').each(function () {
                $.adminPhotosModeratingModule.events.requestSelectFiles($(this), false);
            });

            $.adminPhotosModeratingModule.events.requestSelectFiles(null, true);
        });
    },

    selectAll: function () {
        $('#select-all-files').on('click', function () {
            if(!$(this).prop('disabled')){
                $(this).prop('disabled',true);
            }
            $('.multiple-thumb a:not(.check)').each(function () {
                $.adminPhotosModeratingModule.events.requestSelectFiles($(this), false);
            });

            $.adminPhotosModeratingModule.events.requestSelectFiles(null, true);

        });
    }
};

$(document).ready(function () {
    $.adminPhotosModeratingModule.events.initSelectSort();
    $.adminPhotosModeratingModule.events.sort();
    $.adminPhotosModeratingModule.events.sortMode();
    $.adminPhotosModeratingModule.events.decline();
    $.adminPhotosModeratingModule.events.approve();
    $.adminPhotosModeratingModule.events.multipleSelectFiles();
    $.adminPhotosModeratingModule.events.previewOriginalFile();
    $.adminPhotosModeratingModule.events.selectOnShift();
    $.adminPhotosModeratingModule.events.selectAll();
    $.adminPhotosModeratingModule.events.unSelect();
    $(window).load(function () {
        $.adminPhotosModeratingModule.events.resizeHeights();
    });

});
