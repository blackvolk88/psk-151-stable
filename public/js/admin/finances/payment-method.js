$.adminPaymentMethodModule = {};

$.adminPaymentMethodModule.events = {
    toggleActivePaymentMethod: function () {
        $('.toogle-edit').on('click ontouchend', function (e) {
            e.preventDefault();
            var element = $(this);
            var resources = element.attr('href');
            var status = element.attr('target-status');

            var req = $.adminHelper.ajax('PUT', resources, {status: status});

            req.success(function (response) {
                $.adminHelper.notification('bg-green', response);

                switch (status) {
                    case '2':
                        element.attr('target-status', 1);
                        element.find('i').text('visibility_off');
                        break;
                    default:
                        element.attr('target-status', 2);
                        element.find('i').text('visibility');
                        break;
                }

            });
            req.error(function (response) {
                $.adminHelper.notification('bg-red', response.responseText);
                return false;
            });

        });
    },

    editPaymentMethodName: function () {
        $('#payment-method-table').editableTableWidget({preventColumns: [0, 1], needEdits: [0]});

        $('#payment-method-table td.edit-name').on('change', function(event, value) {
            var targetID = $(this).attr('target-id');
            if (targetID === undefined) {
                return false;
            }

            var req = $.adminHelper.ajax('PUT', '/admin/payment-method/edit/name/' + targetID, {name: value});
            req.success(function(response) {
                $.adminHelper.notification('bg-green', response);
            });
            req.error(function(response) {
                $.each(response.responseJSON, function(key, message) {
                    $.adminHelper.notification('bg-red', message);
                });

                return false;
            });
        });

    }
};

$(document).ready(function () {
    $.adminPaymentMethodModule.events.editPaymentMethodName();
    $.adminPaymentMethodModule.events.toggleActivePaymentMethod();
});