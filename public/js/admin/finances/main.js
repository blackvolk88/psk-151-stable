$.adminFinanceModule = {};

$.adminFinanceModule.events = {
    editWithdraw: function () {
        $('.edit-withdraw').on('click ontouchend', function (e) {
            var resources = $(this).attr('href');
            var action = $(this).attr('target-action');
            e.preventDefault();

            var req = $.adminHelper.ajax('PUT', resources, {action: action});

            req.success(function (response) {
                window.location.reload();
            });
            req.error(function (response) {
                $.adminHelper.notification('bg-red', response.responseText);
                return false;
            });
        });
    },
    editWithdrawDisabled: function () {
        $('.edit-withdraw-disabled').on('click ontouchend', function (e) {
            e.preventDefault();
            $.adminHelper.notification('bg-red', 'This request can not be changed');
            return false;
        });
    }
};


$(document).ready(function () {
    $.adminFinanceModule.events.editWithdraw();
});