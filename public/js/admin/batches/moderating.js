$.adminBatchModeratinghModule = {
    prepareWorkType: function () {
        var type = $('#work-type option:selected').val();

        if ('' === type) {
            $.adminHelper.notification('bg-red', 'Please select work type');
            return false;
        }

        return type;
    },

    prepareDifficulty: function () {
        var difficulty = $('#difficulty option:selected').val();

        if ('' === difficulty) {
            $.adminHelper.notification('bg-red', 'Please select difficulty');
            return false;
        }

        return difficulty;
    },

    prepareDisabledFiles: function (files) {
        $.each(files, function (key, id) {
            $('#image_' + id).removeClass('batch-image-selected');
            $('#image_' + id).addClass('batch-image-disabled');
            $('#image_' + id).closest('.preview-thumb').toggleClass('wrap-image-selected');

            $.adminBatchModeratinghModule.images = $.grep($.adminBatchModeratinghModule.images, function (value) {
                return value != id;
            });

            $.adminBatchModeratinghModule.ungroupImages = $.grep($.adminBatchModeratinghModule.ungroupImages, function (value) {
                return value != id;
            });
        });
    },

    createRandomBatch: function () {
        var data = {
            count: $('#random-batching-count').val()
        };

        var comment = $('#batch-comment').val();

        if ('' !== comment) {
            data.comment = comment;
        }

        var designer = $('#designers option:selected').val();

        if ('' !== designer) {
            data.executor = designer;
        }

        var req = $.adminHelper.ajax('POST', '/admin/batches/moderating/new/random', data);
        req.success(function (response) {
            $.adminHelper.notification('bg-green', response);
            setTimeout(function () {
                window.location.reload();
            }, 2000);
        });
        req.error(function (response) {
            if (response.responseJSON) {
                $.each(response.responseJSON, function (key, val) {
                    $.adminHelper.notification('bg-red', val);
                });
            }
            else {
                $.adminHelper.notification('bg-red', response.responseText);
            }
        });
    },

    executeFilter: function () {
        var format = $('#files-format option:selected').val();
        var filmed = $('#filmed-date').val();
        var uploaded = $('#uploaded-date').val();

        if (format === '' && filmed === '' && uploaded === '') {
            return window.location.href = '/admin/batches/moderating';
        }

        var url = '/admin/batches/moderating/filter/?';

        if (format !== '') {
            url += 'format=' + format + '&';
        }
        if (filmed !== '') {
            url += 'filmed=' + filmed + '&';
        }
        if (uploaded !== '') {
            url += 'uploaded=' + uploaded + '&';
        }

        url = url.slice(0, -1);

        window.location.href = url;
    },

    watchSelectFiles: function () {
        $('#info-selected-formats').text($.adminBatchModeratinghModule.selectedFormats.filter($.adminHelper.onlyUnique));
        $('#info-selected-count').text($.adminBatchModeratinghModule.images.length);
        $('#info-selected-size').text($.adminHelper.bytesToHuman($.adminBatchModeratinghModule.totalSize));
    }
};

$.adminBatchModeratinghModule.images = [];
$.adminBatchModeratinghModule.ungroupImages = [];
$.adminBatchModeratinghModule.selectedFormats = [];
$.adminBatchModeratinghModule.totalSize = 0;
$.adminBatchModeratinghModule.random = false;

$.adminBatchModeratinghModule.events = {
    bindGroupBatch: function () {
        $('#group-batch').click(function () {
            if ($.adminBatchModeratinghModule.random) {
                $.adminBatchModeratinghModule.createRandomBatch();
                return true;
            }

            var type = $.adminBatchModeratinghModule.prepareWorkType();
            var difficulty = $.adminBatchModeratinghModule.prepareDifficulty();

            if (typeof type === 'boolean' && typeof difficulty === 'boolean') {
                return false;
            }

            if ($.adminBatchModeratinghModule.images.length) {
                var data = {
                    images: $.adminBatchModeratinghModule.images,
                    type: type,
                    difficulty: difficulty
                };

                var comment = $('#batch-comment').val();

                if ('' !== comment) {
                    data.comment = comment;
                }

                var designer = $('#designers option:selected').val();

                if ('' !== designer) {
                    data.executor = designer;
                }

                var req = $.adminHelper.ajax('POST', '/admin/batches/moderating/new', data);
                req.success(function (response) {
                    $.adminHelper.notification('bg-green', response);
                    setTimeout(function () {
                        // window.location.reload();
                    }, 2000);
                });
                req.error(function (response) {
                    $.adminHelper.notification('bg-red', 'Some images already using in other batch');
                    $.adminBatchModeratinghModule.prepareDisabledFiles(response.responseJSON);
                });
            }
        });
    },


    bindBatchedImage: function () {
        $('.batch-preview-thumb a').click(function () {
            $(this).toggleClass('check');

            var $this = $(this).find('.batch-image');
            $this.toggleClass('batch-image-selected');
            $this.closest('.preview-thumb').toggleClass('wrap-image-selected');
            var imageID = $this.attr('target-id');
            var imageFormat = $this.attr('target-format');
            var imageSize = $this.attr('target-size');

            if ($.inArray(imageID, $.adminBatchModeratinghModule.images) === -1) {
                $.adminBatchModeratinghModule.images.push(imageID);
                $.adminBatchModeratinghModule.selectedFormats.push(imageFormat);
                $.adminBatchModeratinghModule.totalSize += parseInt(imageSize);

                $('#group-batch').prop('disabled', false);
            }
            else {
                $.adminBatchModeratinghModule.images = $.grep($.adminBatchModeratinghModule.images, function (value) {
                    return value != imageID;
                });

                $.adminBatchModeratinghModule.selectedFormats.splice($.adminBatchModeratinghModule.selectedFormats.indexOf(imageFormat), 1);
                $.adminBatchModeratinghModule.totalSize -= parseInt(imageSize);

                if (!$.adminBatchModeratinghModule.images.length) {
                    $('#group-batch').prop('disabled', true);
                }
            }
            $.adminBatchModeratinghModule.watchSelectFiles();
        });
    },

    changeBatch: function () {
        $('#batch-list').change(function () {
            var selectItem = $('#batch-list option:selected').val();

            if (selectItem === '') {
                window.location.href = '/admin/batches/moderating';
            }
            else {
                window.location.href = '/admin/batches/moderating/view/' + selectItem;
            }
        });
    },

    bindRandomBatching: function () {
        $('#random-batching-count').keyup(function () {
            if ('' == $(this).val()) {
                $.adminBatchModeratinghModule.random = false;

                $('#work-type').removeAttr('disabled');
                $('#difficulty').removeAttr('disabled');
                $('#group-batch').prop('disabled', true);
            }
            else {
                $.adminBatchModeratinghModule.random = true;

                $('#work-type').prop('disabled', true);
                $('#difficulty').prop('disabled', true);
                $('#group-batch').removeAttr('disabled');
            }
        });
    },

    filterFormat: function () {
        $('#files-format').on('change', function () {
            $.adminBatchModeratinghModule.executeFilter();
        });
    },

    filmedFilter: function () {
        $('#filmed-date').bootstrapMaterialDatePicker({
            weekStart: 0,
            time: false
        }).on('change', function (e, date) {
            $.adminBatchModeratinghModule.executeFilter();
        });

        $('#uploaded-date').bootstrapMaterialDatePicker({
            weekStart: 0,
            time: false
        }).on('change', function (e, date) {
            $.adminBatchModeratinghModule.executeFilter();
        });
    },

    ungroup: function () {
        $('#ungroup-batch').click(function (e) {
            e.preventDefault();

            var req = $.adminHelper.ajax('DELETE', '/admin/batches/moderating/destroy/' + $(this).attr('target-id'));
            req.success(function (response) {
                $.adminHelper.notification('bg-green', response);
                setTimeout(function () {
                    window.location.href = '/admin/batches/moderating';
                }, 2000);
            });
            req.error(function (response) {
                $.adminHelper.notification('bg-red', response.responseText);
            });
        });
    },

    bindUngroupedImage: function () {
        $('.batch-group-thumb a').click(function () {
            $(this).toggleClass('check');

            var $this = $(this).find('.batch-file-image');
            $this.closest('.preview-thumb').toggleClass('wrap-image-selected');
            $this.toggleClass('batch-image-selected');

            var imageID = $this.attr('target-id');

            if ($.inArray(imageID, $.adminBatchModeratinghModule.ungroupImages) === -1) {
                $.adminBatchModeratinghModule.ungroupImages.push(imageID);

                $('#ungroup-files-batch').prop('disabled', false);
            }
            else {
                $.adminBatchModeratinghModule.ungroupImages = $.grep($.adminBatchModeratinghModule.ungroupImages, function (value) {
                    return value != imageID;
                });

                if (!$.adminBatchModeratinghModule.ungroupImages.length) {
                    $('#ungroup-files-batch').prop('disabled', true);
                }
            }
        });
    },

    ungroupSelected: function () {
        $('#ungroup-files-batch').click(function () {
            var data = {
                target: $(this).attr('target-id'),
                images: $.adminBatchModeratinghModule.ungroupImages
            };

            var req = $.adminHelper.ajax('POST', '/admin/batches/moderating/ungroup-selected', data);
            req.success(function (response) {
                $.adminHelper.notification('bg-green', response);
                setTimeout(function () {
                    window.location.href = '/admin/batches/moderating';
                }, 2000);
            });
            req.error(function (response) {
                $.adminHelper.notification('bg-red', response.responseText);
            });
        });
    },

    resizeHeights: function () {
        var wH = $(window).height(),
            headH = $('.card .header').outerHeight();
        $('.card').css('height', wH + 'px');
        $('.card .body').css('height', (wH - headH) + 'px');

        var fixH = $('.fixed-height').height();

        $('.scrolling-batches').css('height', (fixH) + 'px');


        $(window).resize(function () {
            wH = $(window).height(),
                headH = $('.card .header').outerHeight();
            $('.card').css('height', wH + 'px');
            $('.card .body').css('height', (wH - headH) + 'px');

            fixH = $('.fixed-height').height();

            $('.scrolling-batches').css('height', (fixH) + 'px');
        });
        $(".scrolling-batches").mCustomScrollbar({
            axis: "y",
            mouseWheelPixels: 200,
            moveDragger: true
        });
    },

    selectOnShift: function () {
        var ind = 1,
            shiftAction = false;
        $('.preview-thumb a').each(function () {
            $(this).attr('data-first', '0');
            $(this).attr('data-last', '0');
            $(this).attr('data-number', ind);
            ind += 1;
        });
        $(document).keydown(function (event) {

            if (event.keyCode == 16) {
                shiftAction = true;
            }
        }).keyup(function (event) {
            if (event.keyCode == 16) {
                shiftAction = false;
            }
        });

        $('.preview-thumb a').on('click', function () {
            if($('.preview-thumb a.check').length){
                $('#unselect').prop('disabled',false);
            }else{
                $('#unselect').prop('disabled',true);
            }
            if(!$(this).hasClass('triggered')) {
                $('.preview-thumb a').attr('data-last', '0');

                if (!$('.preview-thumb a[data-first="1"]').length) {

                    $(this).attr('data-first', '1');
                } else {
                    if (shiftAction) {
                        $(this).attr('data-last', '1');
                    } else {
                        $('.preview-thumb a').attr('data-first', '0');
                        $(this).attr('data-first', '1');
                    }
                }

                if ($(this).hasClass('check')) {
                    if ($('.preview-thumb a[data-last="1"]').length && $('.preview-thumb a[data-first="1"]').length) {
                        var start = $('.preview-thumb a[data-first="1"]').attr('data-number') * 1,
                            finish = $('.preview-thumb a[data-last="1"]').attr('data-number') * 1;
                        if (finish > start) {
                            $('.preview-thumb a:not(.check)').each(function () {

                                var num = $(this).attr('data-number') * 1;

                                if (num < finish && num > start) {
                                    $(this).addClass('triggered');
                                    $(this).trigger('click');
                                }
                            });
                        } else {
                            $('.preview-thumb a:not(.check)').each(function () {
                                var num = $(this).attr('data-number') * 1;
                                if (num < start && num > finish) {
                                    $(this).addClass('triggered');
                                    $(this).trigger('click');
                                }
                            });
                        }
                    }
                }
            }else{
                $(this).removeClass('triggered');
            }
        });
    },
    unselect: function () {
        $('#unselect').on('click',function(){
            $('.preview-thumb a.check').trigger('click');
            $(this).prop('disabled',true);
        });
    }


};


$(function () {
    $.adminBatchModeratinghModule.events.bindGroupBatch();
    $.adminBatchModeratinghModule.events.bindBatchedImage();
    $.adminBatchModeratinghModule.events.bindRandomBatching();

    $.adminBatchModeratinghModule.events.changeBatch();

    $.adminBatchModeratinghModule.events.filterFormat();
    $.adminBatchModeratinghModule.events.filmedFilter();

    $.adminBatchModeratinghModule.events.ungroup();
    $.adminBatchModeratinghModule.events.bindUngroupedImage();
    $.adminBatchModeratinghModule.events.ungroupSelected();
    $.adminBatchModeratinghModule.events.resizeHeights();
    $.adminBatchModeratinghModule.events.selectOnShift();
    $.adminBatchModeratinghModule.events.unselect();
});