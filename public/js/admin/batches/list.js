$.adminBatchListModule = {};

$.adminBatchListModule.events = {
    filter: function() {
        $('.filter-items').on('change', function() {
            var difficulty = $('#difficulty option:selected').val();
            var type = $('#work-type option:selected').val();
            var status = $('#batch-status option:selected').val();
            var executors = $('#executors-list option:selected').val();

            if (difficulty === '' && type === '' && status === '' && executors === '') {
                return window.location.href = '/admin/batches/list';
            }

            var url = '/admin/batches/list/filter/?';

            if ('' !== difficulty) {
                url += 'difficulty=' + difficulty + '&';
            }
            if ('' !== type) {
                url += 'type=' + type + '&';
            }
            if ('' !== status) {
                url += 'status=' + status + '&';
            }

            if ('' !== executors) {
                url += 'executors=' + executors + '&';
            }

            url = url.slice(0 , -1);
            window.location.href = url;
        });
    },

    ungroup: function() {
        $('.ungroup-batch').click(function(e) {
            var _element = this;
            e.preventDefault();

            if (confirm('Do you really want ungroup this batch?')) {
                var req = $.adminHelper.ajax('DELETE', '/admin/batches/list/destroy/' + $(this).attr('target-id'));
                req.success(function(response) {
                    $(_element).parent().parent().fadeOut();
                    $.adminHelper.notification('bg-green', response);
                    setTimeout(function() {
                        window.location.reload();
                    }, 2000);
                });
                req.error(function(response) {
                    $.adminHelper.notification('bg-red', response.responseText);
                });
            }
        });
    }
};

$(function () {
    $.adminBatchListModule.events.filter();
    $.adminBatchListModule.events.ungroup();
});
