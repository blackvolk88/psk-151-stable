if (typeof jQuery === "undefined") {
    throw new Error("jQuery plugins need to be before this file");
}

$.adminMain = {
    calcHeights: function() {
        var userH = $('aside .fixed-height').outerHeight();
        $('.filters-block').css('min-height', userH + 'px');
    },

    calcPanelScrollHeight: function() {
        var scrollH = $(window).height() - $('.work-panel .fixed-height').outerHeight();
        $('.work-panel .scroll-wrap').css('height', scrollH + 'px');
    },

    calcPhotosScrollHeight: function() {
        var scrollH = $(window).height() - $('.work-space .fixed-height').outerHeight();
        $('.work-space .scroll-wrap').css('height', scrollH + 'px');
    },

    initScrollBar: function() {
        $(".scroll-wrap").mCustomScrollbar({
            axis: "y",
            mouseWheelPixels: 200,
            moveDragger: true
        });
    },

    initDatePicker: function() {
        $(".datepicker").datepicker({
            showOn: "button",
            buttonText: "",
            dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
            showOtherMonths: true,
            dateFormat: "dd/mm/yy"
        });

        $('.picker-wrap').on('click', function () {
            $('.ui-datepicker-trigger').trigger('click');
        });
    }
};

$.adminMain.events = {
    logout: function() {
        $('#logout-button').click(function(e) {
            e.preventDefault();
            $('#logout-form').submit();
        });
    }
};

$(function () {
    $(window).load(function () {
        $.adminMain.calcHeights();
        $.adminMain.calcPanelScrollHeight();
        $.adminMain.calcPhotosScrollHeight();
    });

    $.adminMain.initScrollBar();
    $.adminMain.events.logout();

    $('select,input[type="file"]').styler();
    $('.user-data').find('.jq-file__name').text($('.user-data input[type="file"]').attr('placeholder'));

    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
});