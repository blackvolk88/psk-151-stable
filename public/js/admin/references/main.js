$.adminReferencesModule = {};

$.adminReferencesModule.events = {
    initStatusTable: function() {
        $('#statuses-table').editableTableWidget();

        $('#statuses-table td').on('change', function(event, value) {
            var req = $.adminHelper.ajax('PUT', '/admin/references/status/' + $(this).attr('status-id'), {name: value});
            req.success(function(response) {
                $.adminHelper.notification('bg-green', response);
            });
            req.error(function(response) {
                $.adminHelper.notification('bg-red', response.responseText);
                return false;
            });
        });
    },

    removeWorkType: function() {
        $('.remove-type-item').click(function(e) {
            var _element = this;
            e.preventDefault();

            var req = $.adminHelper.ajax('DELETE', '/admin/references/work-type/destroy/' + $(this).attr('target-id'));
            req.success(function(response) {
                $(_element).parent().parent().fadeOut();
                $.adminHelper.notification('bg-green', response);
            });
            req.error(function(response) {
                $.adminHelper.notification('bg-red', response.responseText);
            });
        });
    },

    removeDifficulty: function() {
        $('.remove-difficulty-item').click(function(e) {
            var _element = this;
            e.preventDefault();

            var req = $.adminHelper.ajax('DELETE', '/admin/references/difficulty/destroy/' + $(this).attr('target-id'));
            req.success(function(response) {
                $(_element).parent().parent().fadeOut();
                $.adminHelper.notification('bg-green', response);
            });
            req.error(function(response) {
                $.adminHelper.notification('bg-red', response.responseText);
            });
        });
    },

    removeRejectReason: function() {
        $('.delete-reject-item').click(function(e) {
            var _element = this;
            e.preventDefault();

            var req = $.adminHelper.ajax('DELETE', '/admin/references/reject-reasons/destroy/' + $(this).attr('target-id'));
            req.success(function(response) {
                $(_element).parent().parent().fadeOut();
                $.adminHelper.notification('bg-green', response);
                setTimeout(function() {
                    window.location.reload();
                }, 2000);
            });
            req.error(function(response) {
                $.adminHelper.notification('bg-red', response.responseText);
            });
        });
    }
};

$(document).ready(function() {
    $.adminReferencesModule.events.initStatusTable();
    $.adminReferencesModule.events.removeWorkType();
    $.adminReferencesModule.events.removeDifficulty();
    $.adminReferencesModule.events.removeRejectReason();
});