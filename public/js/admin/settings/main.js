$.adminSettingsModule = {};

$.adminSettingsModule.events = {
    initCoefficientSettings: function() {
        $('#difficulties-table').editableTableWidget({preventColumns: [0, 1]});

        $('#difficulties-table td').on('change', function(event, value) {
            var targetID = $(this).attr('target-id');
            if (targetID === undefined) {
                return false;
            }

            var req = $.adminHelper.ajax('PUT', '/admin/settings/batch-coefficients/edit/' + targetID, {coefficient: value});
            req.success(function(response) {
                $.adminHelper.notification('bg-green', response);
            });
            req.error(function(response) {
                $.each(response.responseJSON, function(key, message) {
                    $.adminHelper.notification('bg-red', message);
                });

                return false;
            });
        });
    },

    initFilePriceSettings: function() {
        $('#prices-table').editableTableWidget({preventColumns: [0, 1]});

        $('#prices-table td').on('change', function(event, value) {
            var targetID = $(this).attr('target-id');
            if (targetID === undefined) {
                return false;
            }

            var req = $.adminHelper.ajax('PUT', '/admin/settings/file-prices/edit/' + targetID, {price: value});
            req.success(function(response) {
                $.adminHelper.notification('bg-green', response);
            });
            req.error(function(response) {
                $.each(response.responseJSON, function(key, message) {
                    $.adminHelper.notification('bg-red', message);
                });

                return false;
            });
        });
    },

    initDesignerLevelCoefficient: function() {
        $('#designer-levels-table').editableTableWidget({preventColumns: [0, 1]});

        $('#designer-levels-table td').on('change', function(event, value) {
            var targetID = $(this).attr('target-id');
            if (targetID === undefined) {
                return false;
            }

            var req = $.adminHelper.ajax('PUT', '/admin/settings/designer-rates-coefficient/edit/' + targetID, {coefficient: value});
            req.success(function(response) {
                $.adminHelper.notification('bg-green', response);
            });
            req.error(function(response) {
                $.each(response.responseJSON, function(key, message) {
                    $.adminHelper.notification('bg-red', message);
                });

                return false;
            });
        });
    },

    initBatchLimit: function() {
        $('#designer-batch-limits-table').editableTableWidget({preventColumns: [0, 1]});

        $('#designer-batch-limits-table td').on('change', function(event, value) {
            var targetID = $(this).attr('target-id');
            if (targetID === undefined) {
                return false;
            }

            var req = $.adminHelper.ajax('PUT', '/admin/settings/designer-batch-limit/edit/' + targetID, {limit: value});
            req.success(function(response) {
                $.adminHelper.notification('bg-green', response);
            });
            req.error(function(response) {
                $.each(response.responseJSON, function(key, message) {
                    $.adminHelper.notification('bg-red', message);
                });

                return false;
            });
        });
    },

    initGlobalSettings: function(){
        $('#global-setting-table').editableTableWidget({preventColumns: [0, 1]});

        $('#global-setting-table td').on('change', function(event, value) {
            var targetID = $(this).attr('target-id');
            if (targetID === undefined) {
                return false;
            }

            var req = $.adminHelper.ajax('PUT', '/admin/settings/global-setting/edit/' + targetID, {value: value});
            req.success(function(response) {
                $.adminHelper.notification('bg-green', response);
            });
            req.error(function(response) {
                $.each(response.responseJSON, function(key, message) {
                    $.adminHelper.notification('bg-red', message);
                });

                return false;
            });
        });
    }
};

$(document).ready(function() {
    $.adminSettingsModule.events.initCoefficientSettings();
    $.adminSettingsModule.events.initFilePriceSettings();
    $.adminSettingsModule.events.initDesignerLevelCoefficient();
    $.adminSettingsModule.events.initBatchLimit();
    $.adminSettingsModule.events.initGlobalSettings();
});