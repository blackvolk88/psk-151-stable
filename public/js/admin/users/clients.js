$.adminClientsModule = {
    redirect: function() {
        var url = '/admin/users/clients/list?';

        var search = $('#search-client-name').val();

        if ('' !== search) {
            url += 'search=' + search + '&';
        }

        url = url.slice(0 , -1);
        window.location.href = url;
    }
};

$.adminClientsModule.events = {
    filter: function() {
        $('#search-button').click(function() {
            if ('' == $('#search-client-name').val()) {
                $.adminHelper.notification('bg-red', 'Please specify client name');
                return false;
            }

            $.adminClientsModule.redirect();
        });
    },

    initChangeUserStatus: function() {
        $('#user-status').click(function() {
            var target = $(this).attr('target-id');
            var status = ($(this).is(':checked')) ? 1 : 0;

            var req = $.adminHelper.ajax('POST', '/admin/users/clients/change-user-status/' + target, {status: status});
            req.success(function(response) {
                $.adminHelper.notification('bg-green', response);
            });
            req.error(function(response) {
                $.each(response.responseJSON, function(key, message) {
                    $.adminHelper.notification('bg-red', message);
                });
            });
        });
    }
};

$(document).ready(function() {
    $.adminClientsModule.events.filter();
    $.adminClientsModule.events.initChangeUserStatus();
});