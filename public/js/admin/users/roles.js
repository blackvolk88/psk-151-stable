$.adminRolesModule = {};

$.adminRolesModule.events = {
    delete: function() {
        $('.delete-role').click(function(e) {
            e.preventDefault();

            var _element = this;

            var req = $.adminHelper.ajax('DELETE', '/admin/users/roles/destroy/' + $(this).attr('target-id'));
            req.success(function(response) {
                $(_element).parent().parent().fadeOut();
                $.adminHelper.notification('bg-green', response);
                setTimeout(function() {
                    window.location.reload();
                }, 2000);
            });
            req.error(function(response) {
                $.adminHelper.notification('bg-red', response.responseText);
            });
        });
    }
};

$(document).ready(function() {
    $.adminRolesModule.events.delete();
});