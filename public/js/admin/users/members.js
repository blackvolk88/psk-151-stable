$.adminMembersModule = {
    redirect: function() {
        var url = '/admin/users/members/list?';

        var search = $('#search-member-name').val();

        if ('' !== search) {
            url += 'search=' + search + '&';
        }

        url = url.slice(0 , -1);
        window.location.href = url;
    }
};

$.adminMembersModule.events = {
    filter: function() {
        $('#search-button').click(function() {
            if ('' == $('#search-member-name').val()) {
                $.adminHelper.notification('bg-red', 'Please specify member name');
                return false;
            }

            $.adminMembersModule.redirect();
        });
    },

    initChangeUserStatus: function() {
        $('#user-status').click(function() {
            var target = $(this).attr('target-id');
            var status = ($(this).is(':checked')) ? 1 : 0;

            var req = $.adminHelper.ajax('POST', '/admin/users/members/change-user-status/' + target, {status: status});
            req.success(function(response) {
                $.adminHelper.notification('bg-green', response);
            });
            req.error(function(response) {
                $.each(response.responseJSON, function(key, message) {
                    $.adminHelper.notification('bg-red', message);
                });
            });
        });
    }
};

$(document).ready(function() {
    $.adminMembersModule.events.filter();
    $.adminMembersModule.events.initChangeUserStatus();
});